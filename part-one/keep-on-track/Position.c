/*
 * @title Position.c
 * @date 22 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "Position.h"

#define WRITE_INTERIM_IMAGES
#define LOGGER

Position getCarPosition(char* imagePath)
{
	//Defining the return variable
	Position carPosition = UNKNOWN;

	//Reading the data of the image file given
	ImageRGB* image = readBMPFile(imagePath);

	#ifdef LOGGER
	printf("The image was correctly opened\n");
	#endif

	//Getting the road area from the previous image
	ImageRGB* areaImage = getRoadArea(*image);

	//The first image is no longer necessary
	freeRGBImage(&image);

	#ifdef WRITE_INTERIM_IMAGES
	writeBMPFile(*areaImage,"area-image.bmp");
	#endif

	//Cropping the previous image
	ImageRGB* croppedAreaImage = cropImageRGB(*areaImage);

	//Defining the middle abscissa of the image
	int centerAbscissa = (int) croppedAreaImage->width / 2;

	//The area image is non longer necessary
	freeRGBImage(&areaImage);

	#ifdef WRITE_INTERIM_IMAGES
	writeBMPFile(*croppedAreaImage,"cropped-area-image.bmp");
	#endif

	//defining the number of regions in the cropped image
	int regionNumber = 0;

	//Growing regions from the cropped image
	ColorRegion** regions = growRegionFromImage(*croppedAreaImage,&regionNumber,60,60);

	#ifdef WRITE_INTERIM_IMAGES
	ImageRGB* regionsImage = getImageFromRegions(regions,regionNumber,croppedAreaImage->height,croppedAreaImage->width);
	writeBMPFile(*regionsImage,"region-cropped-area-image.bmp");
	freeRGBImage(&regionsImage);
	#endif

	#ifdef LOGGER
	printf("The growing region has been done\n");
	#endif

	int roadRegionIndex = getLargestRegionIndex(regions,regionNumber);

	Pixel* roadRegionBarycentre = getColorRegionBarycentre(*regions[roadRegionIndex]);

	if(isPixelVerticallyCentered(*roadRegionBarycentre,croppedAreaImage->height,croppedAreaImage->width))
	{
		#ifdef LOGGER
		printf("The road region barycentre is centered\n");
		#endif

		PixelCloud* significantBarycentres = getSignificantBarycentresFromRegions(regions,regionNumber,roadRegionIndex);

		//Freeing the region
		int regionIndex;

		for(regionIndex = 0 ; regionIndex < regionNumber ; regionIndex+=1)
		{
			freeColorRegion(&regions[regionIndex]);
		}

		free(regions);

		#ifdef LOGGER
		printf("The regions were freed\n");
		#endif

		if(significantBarycentres != NULL)
		{
			#ifdef WRITE_INTERIM_IMAGES
			ImageRGB* barycentresImage = newRGBImage(croppedAreaImage->height,croppedAreaImage->width);
			printf("First Hello\n");
			ColorRGB* red = newColorRGB(255,0,0);
			printf("Second Hello\n");
			drawPixelCloudInImageRGB(*significantBarycentres,barycentresImage,*red);
			printf("Third Hello\n");
			writeBMPFile(*barycentresImage,"barycentre-cropped-area-image.bmp");
			printf("Fourth Hello\n");
			freeColorRGB(&red);
			printf("Fifth Hello\n");
			freeRGBImage(&barycentresImage);
			#endif

			printf("Last Hello\n");

			PixelCloud* leftBarycentres = getLeftPixelCloud(*significantBarycentres,(unsigned int) centerAbscissa);

			PixelCloud* rightBarycentres = getRightPixelCloud(*significantBarycentres,(unsigned int) centerAbscissa);

			bool isThereEnoughPixels = false;

			if((leftBarycentres != NULL)&&(rightBarycentres != NULL))
			{
				if((leftBarycentres->size > 1)&&(rightBarycentres->size > 1))
				{
					isThereEnoughPixels = true;
				}
			}

			if(isThereEnoughPixels)
			{

				#ifdef LOGGER
				printf("There are enough pixels for two lines\n");
				#endif

				CartesianLineTable* leftLines = getLinesFromPixelCloud(*leftBarycentres);

				CartesianLineTable* rightLines = getLinesFromPixelCloud(*rightBarycentres);

				CartesianLineTable* leftSignificantLines = getSignificantLines(*leftLines,1);

				CartesianLineTable* rightSignificantLines = getSignificantLines(*rightLines,1);

				#ifdef WRITE_INTERIM_IMAGES
				ImageRGB* linesImage = newRGBImage(croppedAreaImage->height,croppedAreaImage->width);
				red = newColorRGB(255,0,0);
				drawCartesianLineInImage(*leftSignificantLines->lines[0],linesImage,*red);
				drawCartesianLineInImage(*rightSignificantLines->lines[0],linesImage,*red);
				writeBMPFile(*linesImage,"lines-cropped-area-image.bmp");
				freeColorRGB(&red);
				freeRGBImage(&linesImage);
				#endif

				Pixel* intersectionPixel = getIntersectionPixel(*leftSignificantLines->lines[0],*rightSignificantLines->lines[0]);

				if(intersectionPixel != NULL)
				{

					#ifdef LOGGER
					printf("The intersection pixel was found\n");
					#endif

					if(isPixelVerticallyCentered(*intersectionPixel,croppedAreaImage->height,croppedAreaImage->width))
					{
						carPosition = ON_TRACK;
					}
					else
					{
						if(intersectionPixel->abscissa < centerAbscissa)
						{
							carPosition = ON_THE_LEFT;
						}
						else
						{
							carPosition = ON_THE_RIGHT;
						}
					}
				}
				else
				{
					#ifdef LOGGER
					printf("No intersection pixel found\n");
					#endif
				}

				freePixel(&intersectionPixel);

				freeCartesianLineTable(&rightLines);

				freeCartesianLineTable(&leftLines);
			}
			else
			{
				#ifdef LOGGER
				printf("There are not enough pixels for two lines\n");
				#endif

				CartesianLineTable* lines = getLinesFromPixelCloud(*significantBarycentres);

				CartesianLineTable* significantLines = getSignificantLines(*lines,1);

				#ifdef WRITE_INTERIM_IMAGES
				ImageRGB* linesImage = newRGBImage(croppedAreaImage->height,croppedAreaImage->width);
				red = newColorRGB(255,0,0);
				drawCartesianLineInImage(*significantLines->lines[0],linesImage,*red);
				writeBMPFile(*linesImage,"lines-cropped-area-image.bmp");
				freeColorRGB(&red);
				freeRGBImage(&linesImage);
				#endif

				int originAbscissa = getOriginAbscissa(*significantLines->lines[0]);

				if(originAbscissa >= croppedAreaImage->width)
				{
					carPosition = ON_TRACK;
				}
				else
				{
					if(originAbscissa <= 0)
					{
						carPosition = ON_TRACK;
					}
					else
					{
						if(originAbscissa >= centerAbscissa)
						{
							carPosition = ON_THE_RIGHT;
						}
						else
						{
							carPosition = ON_THE_LEFT;
						}
					}
				}

				freeCartesianLineTable(&significantLines);

				freeCartesianLineTable(&lines);
			}

			freePixelCloud(&significantBarycentres);

			freePixelCloud(&leftBarycentres);

			freePixelCloud(&rightBarycentres);

			#ifdef LOGGER
			printf("The barycenters were freed\n");
			#endif
		}
		else
		{
			carPosition = ON_TRACK;
		}

	}
	else
	{
		if(roadRegionBarycentre->abscissa < centerAbscissa)
		{
			carPosition = ON_THE_RIGHT;
		}
		else
		{
			carPosition = ON_THE_LEFT;
		}
	}

	//The road region barycentre is no longer used
	freePixel(&roadRegionBarycentre);

	//The cropped region image is no longer used
	freeRGBImage(&croppedAreaImage);

	return carPosition;
}

bool isPixelVerticallyCentered(Pixel pixel, unsigned int height, unsigned int width)
{
	bool postulate = false;

	int centerAbscissa = (int) width / 2;

	float f_acuracy = 0.10 * ((float) width);
	int acuracy = (int) f_acuracy;

	int abscissaDifferencial = pixel.abscissa - centerAbscissa;

	if((abscissaDifferencial > -acuracy)&&(abscissaDifferencial < acuracy))
	{
		postulate = true;
	}

	return postulate;
}

PixelCloud* getSignificantBarycentresFromRegions(ColorRegion** colorRegions, int regionNumber, int largestRegionIndex)
{
	ColorRGB* black = newColorRGB(0,0,0);

	PixelCloud* significantBarycentres = NULL;

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < regionNumber ; regionIndex+=1)
	{
		if((regionIndex != largestRegionIndex)&&(colorRegions[regionIndex]->size > 1))
		{
			ColorRGB* currentAverageColor = getColorRegionAverage(*colorRegions[regionIndex]);

			if(getDistanceBetweenColor(*black,*currentAverageColor) != 0)
			{
				Pixel* currentBarycentre = getColorRegionBarycentre(*colorRegions[regionIndex]);

				if(significantBarycentres == NULL)
				{
					significantBarycentres = newPixelCloud(*currentBarycentre);
				}
				else
				{
					addPixelInPixelCloud(significantBarycentres,*currentBarycentre);
				}

				freePixel(&currentBarycentre);
			}

			freeColorRGB(&currentAverageColor);
		}
	}

	return significantBarycentres;
}

PixelCloud* getLeftPixelCloud(PixelCloud pixelCloud,unsigned int threshold)
{
	PixelCloud* leftPixelCloud = NULL;

	int pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < pixelCloud.size ; pixelIndex+=1)
	{
		if(pixelCloud.pixels[pixelIndex]->abscissa <= threshold)
		{
			if(leftPixelCloud == NULL)
			{
				leftPixelCloud = newPixelCloud(*pixelCloud.pixels[pixelIndex]);
			}
			else
			{
				addPixelInPixelCloud(leftPixelCloud,*pixelCloud.pixels[pixelIndex]);
			}
		}
	}

	return leftPixelCloud;
}

PixelCloud* getRightPixelCloud(PixelCloud pixelCloud, unsigned int threshold)
{
	PixelCloud* rightPixelCloud = NULL;

	int pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < pixelCloud.size ; pixelIndex+=1)
	{
		if(pixelCloud.pixels[pixelIndex]->abscissa > threshold)
		{
			if(rightPixelCloud == NULL)
			{
				rightPixelCloud = newPixelCloud(*pixelCloud.pixels[pixelIndex]);
			}
			else
			{
				addPixelInPixelCloud(rightPixelCloud,*pixelCloud.pixels[pixelIndex]);
			}
		}
	}

	return rightPixelCloud;
}

CartesianLineTable* getLinesFromPixelCloud(PixelCloud pixelCloud)
{

	CartesianLineTable* lines = NULL;

	int firstPixelIndex, secondPixelIndex;

	for(firstPixelIndex = 0 ; firstPixelIndex < pixelCloud.size ; firstPixelIndex+=1)
	{
		for(secondPixelIndex = 0 ; secondPixelIndex < pixelCloud.size ; secondPixelIndex+=1)
		{
			if(firstPixelIndex != secondPixelIndex)
			{
				CartesianLine* currentLine = newCartesianLineFromPixels(*pixelCloud.pixels[firstPixelIndex],*pixelCloud.pixels[secondPixelIndex]);

				if(lines == NULL)
				{
					lines = newCartesianLineTable(*currentLine);
				}
				else
				{
					int indexFound = isCartesianLineInTable(*lines,*currentLine);

					if(indexFound != -1)
					{
						lines->occurency[indexFound]+=1;
					}
					else
					{
						addCartesianLineToTable(lines,*currentLine);
					}
				}

				freeCartesianLine(&currentLine);
			}
		}
	}

	return lines;
}

int getOriginAbscissa(CartesianLine cartesianLine)
{
	int abscissa;

	if(cartesianLine.isVertical)
	{
		abscissa = cartesianLine.OriginIntercept;
	}
	else
	{
		if(cartesianLine.isHorizontal)
		{
			abscissa = 0;
		}
		else
		{
			abscissa = (int) - (cartesianLine.OriginIntercept / cartesianLine.slope);
		}
	}

	return abscissa;
}
