/*
 * @title Area.c
 * @date 20 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "Area.h"

ImageRGB* getRoadArea(ImageRGB imageRGB)
{
	ImageRGB* areaImage = newRGBImage(imageRGB.height,imageRGB.width);

	Pixel* areaCenter = newPixel(imageRGB.width/2,0);

	int referenceDistance = imageRGB.height/2;

	int columnIndex, rowIndex;

	for(columnIndex = 0 ; columnIndex < imageRGB.width ; columnIndex+=1)
	{
		for(rowIndex = 0 ; rowIndex < imageRGB.height ; rowIndex+=1)
		{
			Pixel* currentPixel = newPixel(columnIndex,rowIndex);

			if(distanceBetween(currentPixel,areaCenter) <= referenceDistance)
			{
				ColorRGB* currentColor = getPixelColorOfImageRGB(imageRGB,*currentPixel);

				setPixelColorOfImageRGB(areaImage,*currentColor,*currentPixel);

				freeColorRGB(&currentColor);
			}

			freePixel(&currentPixel);
		}
	}

	return areaImage;
}

int getAreaMinColumn(ImageRGB imageRGB)
{
	int minColumn = imageRGB.width;

	int columnIndex, rowIndex;

	ColorRGB* blackColor = newColorRGB(0,0,0);

	for(columnIndex = 0 ; columnIndex < imageRGB.width ; columnIndex+=1)
	{
		for(rowIndex = 0 ; rowIndex < imageRGB.height ; rowIndex+=1)
		{
			Pixel* currentPixel = newPixel(columnIndex,rowIndex);
			ColorRGB* currentColor = getPixelColorOfImageRGB(imageRGB,*currentPixel);

			if((getDistanceBetweenColor(*currentColor,*blackColor) != 0)&&(columnIndex < minColumn))
			{
				minColumn = columnIndex;
			}

			freeColorRGB(&currentColor);
			freePixel(&currentPixel);
		}
	}

	return minColumn;
}

int getAreaMaxColumn(ImageRGB imageRGB)
{
	int maxColumn = -1;

	int columnIndex, rowIndex;

	ColorRGB* blackColor = newColorRGB(0,0,0);

	for(columnIndex = 0 ; columnIndex < imageRGB.width ; columnIndex+=1)
	{
		for(rowIndex = 0 ; rowIndex < imageRGB.height ; rowIndex+=1)
		{
			Pixel* currentPixel = newPixel(columnIndex,rowIndex);
			ColorRGB* currentColor = getPixelColorOfImageRGB(imageRGB,*currentPixel);

			if((getDistanceBetweenColor(*currentColor,*blackColor) != 0)&&(columnIndex > maxColumn))
			{
				maxColumn = columnIndex;
			}

			freeColorRGB(&currentColor);
			freePixel(&currentPixel);
		}
	}

	return maxColumn;
}

int getAreaMinRow(ImageRGB imageRGB)
{
	int minRow = -1;

	int columnIndex, rowIndex;

	ColorRGB* blackColor = newColorRGB(0,0,0);

	for(rowIndex = 0 ; rowIndex < imageRGB.height ; rowIndex+=1)
	{
		for(columnIndex = 0 ; columnIndex < imageRGB.width ; columnIndex+=1)
		{
			Pixel* currentPixel = newPixel(columnIndex,rowIndex);
			ColorRGB* currentColor = getPixelColorOfImageRGB(imageRGB,*currentPixel);

			if(getDistanceBetweenColor(*currentColor,*blackColor) != 0)
			{
				minRow = rowIndex;
			}

			freeColorRGB(&currentColor);
			freePixel(&currentPixel);
		}

		if(minRow != -1)
		{
			break;
		}
	}

	return minRow;
}

int getAreaMaxRow(ImageRGB imageRGB)
{
	int maxRow = -1;

	int columnIndex, rowIndex;

	ColorRGB* blackColor = newColorRGB(0,0,0);

	for(rowIndex = imageRGB.height-1 ; rowIndex >= 0 ; rowIndex-=1)
	{
		for(columnIndex = 0 ; columnIndex < imageRGB.width ; columnIndex+=1)
		{
			Pixel* currentPixel = newPixel(columnIndex,rowIndex);
			ColorRGB* currentColor = getPixelColorOfImageRGB(imageRGB,*currentPixel);

			if(getDistanceBetweenColor(*currentColor,*blackColor) != 0)
			{
				maxRow = rowIndex;
			}

			freeColorRGB(&currentColor);
			freePixel(&currentPixel);
		}

		if(maxRow != -1)
		{
			break;
		}
	}

	return maxRow;
}

ImageRGB* cropImageRGB(ImageRGB imageRGB)
{
	int areaMinColumn = getAreaMinColumn(imageRGB);
	int areaMaxColumn = getAreaMaxColumn(imageRGB);
	int areaMinRow = getAreaMinRow(imageRGB);
	int areaMaxRow = getAreaMaxRow(imageRGB);

	unsigned int columnNumber = (unsigned int) (areaMaxColumn - areaMinColumn + 1);
	unsigned int rowNumber = (unsigned int) (areaMaxRow - areaMinRow + 1);

	ImageRGB* croppedImage = newRGBImage(rowNumber,columnNumber);

	int columnIndex, rowIndex;

	for(columnIndex = areaMinColumn ; columnIndex <= areaMaxColumn ; columnIndex+=1)
	{
		for(rowIndex = areaMinRow ; rowIndex <= areaMaxRow ; rowIndex+=1)
		{
			Pixel* currentPixel = newPixel(columnIndex,rowIndex);
			ColorRGB* currentColor = getPixelColorOfImageRGB(imageRGB,*currentPixel);

			Pixel* croppedImagePixel = newPixel(columnIndex-areaMinColumn,rowIndex-areaMinRow);

			setPixelColorOfImageRGB(croppedImage,*currentColor,*croppedImagePixel);
		}
	}

	return croppedImage;
}
