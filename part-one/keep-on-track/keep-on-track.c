/*
 * @title keep-on-track.c
 * @date 22 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "Position.h"

int main (int argc, char *argv[])
{
	if(argc != 2)
	{
		printf("There is not the right number of parameters. End of the program\n");
		exit(0);
	}
	else
	{
		Position carPosition = getCarPosition(argv[1]);

		switch(carPosition)
		{
		case ON_TRACK:
			printf("The car is centered on the track\n");
			break;
		case ON_THE_LEFT:
			printf("Turn right to re-center the car\n");
			break;
		case ON_THE_RIGHT:
			printf("Turn left to re-center the car\n");
			break;
		case BETWEEN_TRACKS:
			printf("Turn left or right to re-center the car\n");
			break;
		default:
			printf("The position of the car on the road could not be determined\n");
			break;
		}
	}

	return 0;
}
