/*
 * @title Area.h
 * @date 20 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef AREA_H_
#define AREA_H_

#include "../../elements/image/ImageRGB.h"

/**
 * @fn getRoadArea
 * @desc Give the area used to analyse the road from a given image
 * @param imageRGB (ImageRGB) : the image for which we want the area
 * @return (ImageRGB*) : the image containing only the area
 * @bcode
 */
ImageRGB* getRoadArea(ImageRGB imageRGB);
/**
 * @ecode
 */


int getAreaMinColumn(ImageRGB imageRGB);

int getAreaMaxColumn(ImageRGB imageRGB);

int getAreaMinRow(ImageRGB imageRGB);

int getAreaMaxRow(ImageRGB imageRGB);

/**
 * @fn cropImageRGB
 * @desc Crop an ImageRGB by removing the black parts
 * @param imageRGB (ImageRGB) : the image to crop
 * @return (ImageRGB*) : the image cropped
 * @bcode
 */
ImageRGB* cropImageRGB(ImageRGB imageRGB);
/**
 * @ecode
 */

#endif /* AREA_H_ */
