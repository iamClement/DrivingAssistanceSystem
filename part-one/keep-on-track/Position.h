/*
 * @title Position.h
 * @date 22 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef POSITION_H_
#define POSITION_H_

#include "Area.h"
#include "../../elements/region/ColorRegion.h"
#include "../../elements/pixel/PixelCloud.h"
#include "../../elements/line/cartesian/CartesianLineTable.h"

/**
 * @enum Position
 * @desc Define all the position a car can have on the road.
 */
typedef enum {ON_TRACK, ON_THE_LEFT, ON_THE_RIGHT, BETWEEN_TRACKS, UNKNOWN}Position;

/**
 * @fn getCarPosition
 * @desc Give the car position on the road from an image file
 * @param imagePath (char*) : the path of the image to analyse
 * @return (Position) : the position of the car on the road
 * @bcode
 */
Position getCarPosition(char* imagePath);
/**
 * @ecode
 */

/**
 * @fn isPixelCentered
 * @desc Check if a pixel is vertically centered on an image
 * @param pixel (Pixel) : the pixel to check
 * @param height (unsigned int) : the height of the image
 * @param width (unsigned int) : the width of the image
 * @return (bool) : true, the pixel is centered, false, the pixel is not centered
 * @bcode
 */
bool isPixelVerticallyCentered(Pixel pixel, unsigned int height, unsigned int width);
/**
 * @ecode
 */

/**
 * @fn getSignificantBarycentresFromRegions
 * @desc
 * @param
 * @param
 * @param
 * @return
 * @bcode
 */
PixelCloud* getSignificantBarycentresFromRegions(ColorRegion** colorRegions, int regionNumber, int largestRegionIndex);
/**
 * @ecode
 */

PixelCloud* getLeftPixelCloud(PixelCloud pixelCloud,unsigned int threshold);

PixelCloud* getRightPixelCloud(PixelCloud pixelCloud, unsigned int threshold);

/**
 * @fn
 * @bcode
 */
CartesianLineTable* getLinesFromPixelCloud(PixelCloud pixelCloud);
/**
 * @ecode
 */

int getOriginAbscissa(CartesianLine cartesianLine);

#endif /* POSITION_H_ */
