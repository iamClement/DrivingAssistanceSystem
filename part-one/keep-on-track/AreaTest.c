/*
 * @title AreaTest.c
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */


#include <stdio.h>
#include <stdlib.h>

#include "Area.h"
#include "../../elements/region/ColorRegion.h"

int main(void)
{
	ImageRGB* imageRGB = readBMPFile("../../image-files/route6.bmp");

	ImageRGB* areaImage = getRoadArea(*imageRGB);

	ImageRGB* areaCroppedImage = cropImageRGB(*areaImage);

	int regionNumber = 0;

	ColorRegion** regions = growRegionFromImage(*areaCroppedImage,&regionNumber,60,60);

	printf("Region number : %d\n",regionNumber);

	ImageRGB* areaCroppedRegionImage = getImageFromRegions(regions,regionNumber,areaCroppedImage->height,areaCroppedImage->width);

	int indexRegion;

	ColorRGB* red = newColorRGB(255,0,0);
	ColorRGB* black = newColorRGB(0,0,0);

	for(indexRegion = 0 ; indexRegion < regionNumber ; indexRegion+=1)
	{
		ColorRGB* currentColor = getColorRegionAverage(*regions[indexRegion]);

		//if(getDistanceBetweenColor(*currentColor,*black) != 0)
		//{
			Pixel* barycenter = getColorRegionBarycentre(*regions[indexRegion]);
			setPixelColorOfImageRGB(areaCroppedRegionImage,*red,*barycenter);
			freePixel(&barycenter);
		//}
	}

	freeColorRGB(&red);

	writeBMPFile(*areaImage,"../../image-files/route6-area.bmp");

	writeBMPFile(*areaCroppedImage,"../../image-files/route6-area-cropped.bmp");

	writeBMPFile(*areaCroppedRegionImage,"../../image-files/route6-area-cropped-regions.bmp");

	freeRGBImage(&areaCroppedRegionImage);

	freeRGBImage(&areaCroppedImage);

	freeRGBImage(&areaImage);

	freeRGBImage(&imageRGB);

	return 0;
}
