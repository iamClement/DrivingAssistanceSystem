#include "Radon.h"
#include <stdio.h>
#include <stdlib.h>


RadonSpace * newRadonSpace(unsigned int height,unsigned int width,unsigned int depth)
{
	int heightIndex,widthIndex,depthIndex;
	RadonSpace* radon = (RadonSpace* )malloc(sizeof(RadonSpace));
	
	radon->height = height;
	radon->width = width;
	radon->depth = depth;
		
	radon->data = (short int***)malloc(sizeof(short int**)*height);
	for(heightIndex=0;heightIndex<height; heightIndex++)
	{
		radon->data[heightIndex] = (short int**)malloc(sizeof(short int*) * width);
		for(widthIndex=0;widthIndex<width; widthIndex++)
		{
			radon->data[heightIndex][widthIndex] = (short int*)malloc(sizeof(short int) * depth);
			for(depthIndex=0;depthIndex<depth; depthIndex++)
			{
				radon->data[heightIndex][widthIndex][depthIndex] = 0;
			}			
		}
	}
	return radon;
}




void freeRadonSpace(RadonSpace ** radon)
{
	if(radon != NULL)
	{
		if(*radon != NULL)
		{			
			RadonSpace* interimRadon = *radon;

			int heightIndex,widthIndex;
			for(heightIndex = 0 ; heightIndex < interimRadon->height ; heightIndex++)
			{
				for(widthIndex= 0 ; widthIndex < interimRadon->width ; widthIndex++)
				{
					free(interimRadon->data[heightIndex][widthIndex]);
				}
				free(interimRadon->data[heightIndex]);
			}
			free(interimRadon->data);

			free(interimRadon);

			*radon = NULL;
		}
	}
}


RadonSpace * radonTransformation(ImageRGB * image)
{
	int yCenter, xCenter , rho ; // center coordinates and radius or the circle
	int heightIndex, widthIndex ;  
	
	int rhoMax =  sqrt( pow(image->height,2) + pow(image->width,2));

	RadonSpace * radon = newRadonSpace(image->height,image->width, rhoMax);

	//for each pixels ...
	for(heightIndex = 0 ; heightIndex < image->height ; heightIndex++) 
	{
		for(widthIndex = 0 ; widthIndex <image->width ; widthIndex++) 
		{
			// check if the pixel is white
			if (image->redMatrix->data[heightIndex][widthIndex] == 255)
			{ 
				//for each position of the center ...
				for (yCenter = 0 ; yCenter< radon->height; yCenter++) 
				{
					for (xCenter = 0 ; xCenter< radon->width; xCenter++) 
					{
						
					 //calculate rho
					 rho =(int)(sqrt( pow(widthIndex - xCenter , 2) + pow(heightIndex - yCenter, 2)));    
					
					 //increment the corresponding pixel
					radon->data[yCenter][xCenter][rho] ++;     
					}
				}
			}
		}
	}
	return radon;
}




ImageRGB * createRadonImage(RadonSpace * radon, int dimension , int value)
{
	
	int heightIndex,widthIndex, depthIndex;
	ImageRGB * image = NULL;
	Matrix * matrix = NULL;

	switch(dimension)
	{
		//cutting x
		case 1: 
			matrix = newMatrix(radon->depth , radon->height);
			for(heightIndex = 0 ; heightIndex < radon->height ; heightIndex++) 
			{
				for(depthIndex = 0 ; depthIndex < radon->depth ; depthIndex++) 
				{
					matrix->data[heightIndex][depthIndex] = radon->data[heightIndex][value][depthIndex];
				}
			}
		break;
		
		//cutting y
		case 2:
			matrix = newMatrix(radon->depth , radon->width);
			for(widthIndex = 0 ; widthIndex < radon->width ; widthIndex++) 
			{
				for(depthIndex = 0 ; depthIndex < radon->depth ; depthIndex++) 
				{
					matrix->data[widthIndex][depthIndex] = radon->data[value][widthIndex][depthIndex];
				}
			}
		break;
		
		//cutting rho
		case 3:
			matrix = newMatrix(radon->width , radon->height);
			for(heightIndex = 0 ; heightIndex < radon->height ; heightIndex++) 
			{
				for(widthIndex = 0 ; widthIndex < radon->width ; widthIndex++) 
				{
					matrix->data[heightIndex][widthIndex] = radon->data[heightIndex][widthIndex][value];
				}
			}
		break;
	}

	balance(matrix);
		
	image = newRGBImage(matrix->rowsNumber, matrix->columnsNumber);
	image->redMatrix = matrix;
	image->greenMatrix = matrix;
	image->blueMatrix = matrix;	


	return image;
}

void balance(Matrix * matrix)
{
	int rowIndex, columnIndex ;
	int maxValue=0;
	float multiplier;

	//research the maximum value of grey level on the matrix
	for(rowIndex = 0 ; rowIndex < matrix->rowsNumber ; rowIndex++)
	{
		for(columnIndex = 0 ; columnIndex <matrix->columnsNumber ; columnIndex++)
		{
			if (matrix->data[rowIndex][columnIndex] > maxValue)
			{
				 maxValue =  matrix->data[rowIndex][columnIndex];
			 }
		}
	}

	//calculate the multiplier
	if (maxValue != 0)
	{
		multiplier = 255 / (float)maxValue;
	}
	else multiplier = 1;

	//change the value of each pixel
	for(rowIndex = 0 ; rowIndex < matrix->rowsNumber ; rowIndex++)
	{
		for(columnIndex = 0 ; columnIndex <matrix->columnsNumber ; columnIndex++)
		{
			matrix->data[rowIndex][columnIndex]  = (int)( matrix->data[rowIndex][columnIndex] * multiplier);
		}
	}
}



