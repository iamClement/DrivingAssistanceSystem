/*
 * circular-panel-detection.c
 *
 *  Created on: 27 janv. 2019
 *      Author: anne
 */

#include "panelDetection.h"
int main(int argc, char *argv[]){

	if(argc != 2)
	{
		printf("There is not the right number of parameters. End of the program\n");
		exit(0);
	}
	else
	{
	char * name=argv[1];
	char * completeName = malloc((7+strlen(name))*sizeof(char));
	//strcpy(completeName,"Images/");
	strcpy(completeName,name);
	ImageRGB * file = readBMPFile(completeName);
	ImageHSV * hsvFile = imageRGBtoHSV(file);
	printf("Image loads correctly\n");
	int nbRed=0;
	int nbBlue=0;
	//int nbWhite = 0;
	ColorRegion ** redRegions = redDetect(file,hsvFile,&nbRed);
	printf("red areas detected \n");
	ColorRegion ** blueRegions = blueDetect(file,hsvFile,&nbBlue);
	printf("blue areas detected\n");
	//ColorRegion ** whiteRegions = whiteDetect(file,hsvFile,&nbWhite);
	//printf("white ok\n");
	int nbRedPanels = 0;
	int nbBluePanels = 0;
	//int nbWhitePanels = 0;
	ImageRGB ** redPanelsList = sortOutPanel(redRegions,nbRed,file->height,file->width,file,&nbRedPanels);
	printf("red areas sorted \n");
	ImageRGB ** bluePanelList = sortOutPanel(blueRegions,nbBlue,file->height,file->width,file,&nbBluePanels);
	printf("blue areas sorted\n");
	//ImageRGB ** whitePanelList = sortOutPanel(whiteRegions,nbWhite,file->height,file->width,file,&nbWhitePanels);
	//printf("sortout ok\n");

	int i;

	for(i=0;i<nbRedPanels;i++){
		int redRegions = 0;
		ColorRegion ** redRegionBis = growRegionFromImage(*(redPanelsList[i]),&redRegions,50,50);
		ImageRGB * uniformeRed = getImageFromRegions(redRegionBis,redRegions,redPanelsList[i]->height,redPanelsList[i]->width);
		PanelName name = namePanel(uniformeRed,INTERDICTION);


		if(name!=NOT_DEFINED){
			printf ("\033[31;01mPanel of interdiction detected !\033[00m\n");
			switch (name) {
				case MAX50KMH:
					printf ("\033[31;01m\t50 km/h max !\033[00m\n");
					break;
				case MAX80KMH:
					printf ("\033[31;01m\t80 km/h max !\033[00m\n");
					break;
				case NO_PARKING:
					printf ("\033[31;01m\tNo parking allowed !\033[00m\n");
					break;

				case NO_STOP:
					printf ("\033[31;01m\tNo stop allowed !\033[00m\n");
					break;
				default:
					printf ("\033[31;01m\tNot labelized panel detected ! Please have a look.\033[00m\n");
					break;
			}

		}



	}

	for(i=0;i<nbBluePanels;i++){
		int blueRegions = 0;
		ColorRegion ** blueRegionBis = growRegionFromImage(*(bluePanelList[i]),&blueRegions,50,50);
		ImageRGB * uniformeBlue = getImageFromRegions(blueRegionBis,blueRegions,bluePanelList[i]->height,bluePanelList[i]->width);
		PanelType pType =classifyBluePanel(uniformeBlue);
		PanelName name = namePanel(uniformeBlue,pType);



		if(name!=NOT_DEFINED){
			switch(pType){
				case OBLIGATION:
					printf ("\033[34;01mPanel of obligation detected !\033[00m\n");
					break;
				case END_OBLIGATION:
					printf ("\033[96;01mPanneau of obligation's end detected !\033[00m\n");
					break;

				default :
					break;
			}

			switch (name) {
				case TURN_RIGHT_NEXT_INTERSECTION:
					printf ("\033[34;01m\tTurn right at the next intersection !\033[00m\n");
					break;
				case END_HORSERIDERS_ONLY:
					printf ("\033[96;01m\tEnd of horseriders reserved way !\033[00m\n");
					break;
				default:
					break;
			}

		}

	}

	}

}
