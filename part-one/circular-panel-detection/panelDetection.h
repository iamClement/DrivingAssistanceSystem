/*
 * panelDetection.h
 *
 *  Created on: 15 janv. 2019
 *      Author: anne
 */

#ifndef PANELDETECTION_H_
#define PANELDETECTION_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../../elements/image/ImageRGB.h"
#include "../../elements/image/ImageHSV.h"
#include "../../elements/image/RGBtoHSV.h"
#include "../../elements/region/ColorRegion.h"
#include "CircleDetection.h"
#include "../../libs/matrixlib/extensions/image/Filter.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

typedef enum{
	INTERDICTION,
	OBLIGATION,
	END_INTERDICTION,
	END_OBLIGATION
}PanelType;

typedef enum{
	NOT_DEFINED,//0
	//interdiction
	WRONG_WAY,//1
	MAX130KMH,
	MAX110KMH,
	MAX90KMH,
	MAX80KMH,//5
	MAX70KMH,
	MAX50KMH,
	MAX30KMH,
	NO_VEHICULAR_TRAFFIC,
	ONCOMING_VEHICLES_PRIORITY,//10
	SOUNDS_SIGNALS_PROHIBITED,
	NO_OVERTAKING,
	NO_LEFT_TURN,
	NO_RIGHT_TURN,
	NO_UTURN,//15
	NO_PARKING,
	NO_STOP,
	NO_PEDESTRIAN,
	NO_MOTOR_ENGINE,
	NO_TRUCK,//20
	NO_BUS,
	//end interdiction
	END_MAX_SPEED,
	END_ALL_INTERDICTION,
	//Obligation
	TURN_LEFT,
	TURN_RIGHT,//25
	TURN_LEFT_NEXT_INTERSECTION,
	TURN_RIGHT_NEXT_INTERSECTION,
	TURN_LEFT_OR_GO_AHEAD_NEXT_INTERSECTION,
	TURN_RIGHT_OR_GO_AHEAD_NEXT_INTERSECTION,
	TURN_RIGHT_OR_LEFT_NEXT_INTERSECTION,//30
	GO_AHEAD,
	BYPASS_RIGHT,
	BYPASS_LEFT,
	BIKE_ONLY,
	PEDESTRIANS_ONLY,//35
	HORSERIDERS_ONLY,
	SNOWCHAINS_ON,
	BUSES_ONLY,
	TRAMWAYS_ONLY,
	//end obligation
	END_BIKE_ONLY,//40
	END_PEDESTRIANS_ONLY,
	END_HORSERIDERS_ONLY,
	END_SNOWCHAINS_ON,
	END_BUSES_ONLY,
	END_TRAMWAYS_ONLY

}PanelName;



ColorRegion ** redDetect(ImageRGB * rgb, ImageHSV * img,int * nbRegions);

ColorRegion ** blueDetect(ImageRGB * rgb,ImageHSV * img,int*nbRegions);

ColorRegion ** whiteDetect(ImageRGB * rgb,ImageHSV * img,int*nbRegions);

Circle * isRegionACircle(ImageRGB * imgRegion);

bool isPixelAnEdge(Pixel pixel, Matrix matrix);

Matrix* newOutlineMatrix(Matrix matrix);

ImageRGB * newOutlineImageRGB(ImageRGB * img);

void ManualThershold(ImageRGB * img, int thershold);

int getShapeMinColumn(Matrix matrix);

int getShapeMaxColumn(Matrix matrix);

int getShapeMinRow(Matrix matrix);

int getShapeMaxRow(Matrix matrix);

Matrix* getCroppedMatrixFromMatrix(Matrix matrix);

ImageRGB * cropImage(ImageRGB * src);

ImageRGB ** sortOutPanel(ColorRegion ** in, int nbRegions,int height, int width, ImageRGB * src, int * nbPanels);

ImageRGB * croppedCompleteImage(ImageRGB * src, ImageRGB * thersholded);

Matrix* applyLBPToMatrix(Matrix matrix);

ImageRGB * applyLBPToImageRGB(ImageRGB * src);

int * createHistogramFromGreyImage(ImageRGB * Grey);

double * balancedHistogram(ImageRGB * Grey);

double histogramAreTheSame(double * hist, double * compHist);

ImageRGB * CoIofImage(ImageRGB * src, Circle * sizeToCrop,ImageRGB * regionImage);

PanelType classifyBluePanel(ImageRGB * panel);

PanelName namePanel(ImageRGB * panel, PanelType type);

PanelName nameInterdictionPanel(ImageRGB * panel);

PanelName nameEndInterdictionPanel(ImageRGB * panel);

PanelName nameObligationPanel(ImageRGB * panel);

PanelName nameEndObligationPanel(ImageRGB * panel);

char ** listBMPdir(char * dirName, int * nbFiles);

bool isRed(int x, int y, ImageHSV * hsv);

bool isBlue(int x, int y, ImageHSV * hsv);


#endif /* PANELDETECTION_H_ */
