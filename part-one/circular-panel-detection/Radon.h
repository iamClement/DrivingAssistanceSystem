/**
 * @title Hough.h
 * @docauthor  Kevin Reynier
 * @date 6 dec. 2018
 * @make
 */


#ifndef RADON_H_
#define RADON_H_

//#include "Hough.h"
#include "../../elements/image/ImageRGB.h"
#include <math.h>

/**
 * @struct RadonSpace
 * @desc A structure that represents the 3D space of radon
 * @field height (unsigned int) : the height of the 3D space
 * @field width (unsigned int) : the width of the 3D space
 * @field depth (unsigned int) : the depth of the 3D space
 * @field data (short int ***) : the 3D space containing the values 
 */
typedef struct RadonSpace 
{
	unsigned int height;
	unsigned int width;
	unsigned int depth;	
	short int *** data;
} RadonSpace;



/**
 * @fn newRadonSpace
 * @desc Allocate the memory for the Radon space
 * @param height (unsigned int) : the height size of the 3D space (represents y center)
 * @param width (unsigned int) : the width size of the 3D space (represents x center)
 * @param depth (unsigned int) : the depth size of the 3D space (represents radius)
 * @return RadonSpace* : the Radon space allocated
 * @bcode
 */
RadonSpace * newRadonSpace(unsigned int height,unsigned int width, unsigned int depth);
/**
 * @ecode
 */
 
 
 /**
 * @fn freeRadonSpace
 * @desc Free the memory space used by the matrix
 * @param radon (RadonSpace**) : the Radon space to be freed
 * @bcode
 */
void freeRadonSpace(RadonSpace ** radon);
/**
 * @ecode
 */
 
 
/**
 * @fn radonTransformation
 * @desc Create the radon transformation of an image
 * @param image (ImageRGB *) : the image to process
 * @return (RadonSpace*) : the radon space corresponding to the image
 * @bcode
 */
RadonSpace * radonTransformation(ImageRGB * image);
/**
 * @ecode
 */


/**
 * @fn createRadonImage
 * @desc Create the image which represents a cut of a Radon transformation
 * @param radon (RadonSpace *) : the Radon space 
 * @param dimension (int ) : the dimension chosen for the cut (1: x coordinate of the center, 2: y coordinate of the center , 3: radius )
 * @param value (int) : the value chosen for the cut
 * @return (ImageRGB *) : the image that represents a cut of the Radon space
 * @bcode
 */
ImageRGB * createRadonImage(RadonSpace * radon, int dimension , int value);
/**
 * @ecode
 */
 
void balance(Matrix * matrix);


#endif
