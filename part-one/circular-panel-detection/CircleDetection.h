/**
 * @title Hough.h
 * @docauthor  Kevin Reynier
 * @date 6 dec. 2018
 * @make
 */


#ifndef CIRCLE_H
#define CIRCLE_H

#include "Radon.h"
#include "../../elements/image/ImageRGB.h"


/**
 * @struct Circle
 * @desc A structure that represents a circle on a picture
 * @field abscissaCenter (int) : the abscissa of the center of the circle
 * @field ordinateCenter (int) : the ordinate of the center of the circle
 * @field radius (int) : the radius of the circle
 */
typedef struct Circle
{
	int abscissaCenter;
	int ordinateCenter;
	int radius;
} Circle;


/**
 * @fn newCircle
 * @desc Allocate the memory for the Circle
 * @param abscissaCenter (int) : the abscissa of the center of the circle
 * @param ordinateCenter (int) : the ordinate of the center of the circle
 * @param radius (int) : the radius of the circle
 * @return Circle* : the Circle allocated and filled
 * @bcode
 */
Circle * newCircle(int abscissaCenter , int ordinateCenter , int radius);
/**
 * @ecode
 */


/**
 * @fn freeCircle
 * @desc free the memory space used by the circle
 * @param circle (Circle *) :  the circle to free
 * @bcode
 */
void freeCircle(Circle * circle);
/**
 * @ecode
 */

/**
 * @fn circleDetection
 * @desc return the list of the circles detected on a picture
 * @param image (ImageRGB *) :  the image to process
 * @param radon (RadonSpace *) :  the Radon space associated to the image
 * @return Circle** : the list of the circles detected on the image
 * @bcode
 */
Circle ** circleDetection(ImageRGB * image , RadonSpace * radon);
/**
 * @ecode
 */


/**
 * @fn newCircleImage
 * @desc create de the image with the circled detected on a given image 
 * @param image (ImageRGB *) : the image to process 
 * @param name (char *) : name of the new .bmp picture 
 * @bcode
 */
//void newCircleImage(ImageRGB * image,char *name);
/**
 * @ecode
 */


/**
 * @fn drawCircle
 * @desc draw a circle on a picture
 * @param image (ImageRGB *) : the image where we draw the circle 
 * @param circle (Circle *) :  the circle to draw on the picture
 * @bcode
 */
//void drawCircle(Circle * circle, ImageRGB * image);
/**
 * @ecode
 */


/**
 * @fn minimum
 * @desc return the minimum integer between 2 integers
 * @param a (int) : first integer
 * @param b (int) :  second integer
 * @return int : the minimum value
 * @bcode
 */
int minimumInt(int a, int b );
/**
 * @ecode
 */

/**
 * @fn maximum
 * @desc return the maximum integer between 2 integers
 * @param a (int) : first integer
 * @param b (int) :  second integer
 * @return int : the maximum value
 * @bcode
 */
int maximumInt(int a, int b );
/**
 * @ecode
 */

#endif
