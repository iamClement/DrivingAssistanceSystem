#include "CircleDetection.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

Circle * newCircle(int abscissaCenter , int ordinateCenter , int radius)
{
	Circle * circle = (Circle *)malloc(sizeof(circle));

	circle->abscissaCenter = abscissaCenter;
	circle->ordinateCenter = ordinateCenter;
	circle->radius = radius;

	return circle;
}

void freeCircle(Circle * circle)
{
	free(circle);
}


Circle ** circleDetection(ImageRGB * image , RadonSpace * radon)
{

	Circle ** circles = (Circle **)malloc(sizeof(Circle*)*50);
	int i;
	for(i=0;i<50;i+=1){
		circles[i]=NULL;
	}
	int maxValue=0;
	int threshold;
	int circleIndex = 0;

	int xmin, xmax , ymin , ymax , zmin , zmax;
	int x , y , rho; //coordinates of the current circle (the max value)
	int n = 5; //neighbors



	//search the maximum value of the radon space
	for(int i = 0 ; i<radon->height ; i+=1)
	{
		for(int j = 0 ; j<radon->width ; j+=1)
		{
			for(int k = 0 ; k<radon->depth ; k+=1)
			{
				if(radon->data[i][j][k] > maxValue) 
				{
					y = i;
					x = j;
					rho = k;
					maxValue = radon->data[i][j][k] ; 
				}
			}
		}
	}

	threshold = maxValue * 0.25;

	//consider only the circles over the threshold
	while(maxValue > threshold)
	{

		//printf("in while : radondata = %d, a=%d et b=%d\n",radon->data[y][x][rho],(int)(2.0 * M_PI * (float)rho *0.50),(int)(2.0 * M_PI * (float)rho *1.50));
		//printf("rho = %d\n",rho);
		if (radon->data[y][x][rho] > (int)(2.0 * M_PI * (float)rho *0.30) && radon->data[y][x][rho] < (int)(2.0 * M_PI * (float)rho *1.50))
		{
			//printf("CERCLE !");
			circles[circleIndex] = newCircle( y, x, rho);  
			circleIndex+=1;

			xmin = maximumInt(0,x-n);
			xmax = minimumInt(radon->width , x+n);
			ymin = maximumInt(0,y-n );
			ymax = minimumInt(radon->height , y+n);
			zmin = maximumInt(0,rho -n);
			zmax = minimumInt(radon->depth, rho+n);

			for(int i = ymin ; i<ymax ; i+=1)
			{
				for(int j = xmin ; j<xmax ; j+=1)
				{
					for(int k = zmin ; k<zmax ; k+=1)
					{
						radon->data[i][j][k] = 0;
					}
				}
			}

		}
		else radon->data[y][x][rho] = 0;

		maxValue = 0;

		for(int i = 0 ; i<radon->height ; i+=1)
		{
			for(int j = 0 ; j<radon->width ; j+=1)
			{
				for(int k = 0 ; k<radon->depth ; k+=1)
				{
					if(radon->data[i][j][k] > maxValue) 
					{
						y = i;
						x = j;
						rho = k;
						maxValue = radon->data[i][j][k] ; 
					}
				}
			}
		}
	}

	return circles;
}




/*
void newCircleImage(ImageRGB * image, char *name)
{
	RadonSpace * radon = radonTransformation(image);
	Circle ** circles = circleDetection(image, radon);

	ImageRGB * imageCircles = newRGBImage(image->height , image->width);
	fillRGBImage(imageCircles);

	int  index = 0;

	while(circles[index] != NULL)
	{
		drawCircle(circles[index] , imageCircles);
		index ++;
	}

	DonneesImageRGB* donnee = convertToDIRGB(imageCircles);
	ecrisBMPRGB_Dans(donnee, name);
}


void drawCircle(Circle * circle, ImageRGB * image)
{
	int xmin = maximumInt(0, circle->abscissaCenter - circle->radius);
	int xmax = minimumInt(image->width, circle->abscissaCenter + circle->radius);
	int ymin = maximumInt(0,circle->ordinateCenter - circle->radius);
	int ymax = minimumInt(image->height,circle->ordinateCenter + circle->radius);

	for(int i=xmin; i<=xmax ; i++)
	{
		for(int j=ymin; j<=ymax ; j++)
		{
			if (circle->radius ==(int)(sqrt( pow(i - circle->abscissaCenter , 2) + pow(j - circle->ordinateCenter , 2) ) ) )
			{
				image->redMatrix->data[i][j] = 200 ;    //doute sur l'ordre i et j
			} 			
		}
	}

}
*/

int minimumInt(int a, int b )
{
	if (a>b) return b ;
	else return a;
}

int maximumInt(int a, int b )
{
	if (a<b) return b ;
	else return a;
}
