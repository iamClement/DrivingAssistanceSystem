/*
 * createLBP.c
 *
 *  Created on: 26 janv. 2019
 *      Author: anne
 */


#include "../panelDetection.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int nbInter=0;
	char ** inter = listBMPdir("Interdiction",&nbInter);

	int nbOb=0;
	char ** oblig = listBMPdir("Obligation",&nbOb);

	int nbEInt=0;
	char ** EndInter = listBMPdir("EndInterdiction",&nbEInt);

	int nbEOb=0;
	char ** EndOb = listBMPdir("EndObligation",&nbEOb);

	printf("listes ok\n");

	int i;

	for(i=0;i<nbInter;i++){
		printf("i=%d\n",i);
		char * file = malloc((13+strlen(inter[i]))*sizeof(char));
		strcpy(file,"Interdiction/");
		strcat(file,inter[i]);
		printf("file = %s\n",file);
		ImageRGB * rgb = readBMPFile(file);
		ImageHSV * hsv = imageRGBtoHSV(rgb);
		int nb = 0;
		ColorRegion ** region = redDetect(rgb,hsv,&nb);
		printf("redDetectOK \n");
		int nb2=0;
		ImageRGB ** List = sortOutPanel(region,nb,rgb->height,rgb->width,rgb,&nb2);
		printf("nb2=%d\n",nb2);
		printf("sortOutOK \n");
		int a;
			for(a=0;a<nb2;a++){
				int regionNb = 0;
				ColorRegion ** coloredregions = growRegionFromImage(*(List[a]),&regionNb,50,50);
				ImageRGB * uniforme = getImageFromRegions(coloredregions,regionNb,List[a]->height,List[a]->width);
				char * name = malloc((17+strlen(inter[i]))*sizeof(char));
				strcpy(name,"Interdiction/LBP/");
				strcat(name,inter[i]);
				ImageRGB * greyLevel = convertToGreyLevel(*uniforme);
				ImageRGB * LBP = applyLBPToImageRGB(greyLevel);
				writeBMPFile(*LBP,name);
				printf("written ok \n");
				freeRGBImage(&(List[a]));
				freeColorRegion(coloredregions);
				freeRGBImage(&uniforme);
				free(name);
				freeRGBImage(&greyLevel);
				freeRGBImage(&LBP);
			}
		free(file);
		freeRGBImage(&rgb);
		freeHSVImage(&hsv);
		freeColorRegion(region);
		free(List);

	}



	printf("Interd OK\n");

	for(i=0;i<nbOb;i++){
		printf("i=%d\n",i);
		//int len = strlen(oblig[i]);
		while(oblig[i][strlen(oblig[i])-1]!='p'){
			printf("error et len = %d\n",strlen(oblig[i]));
			oblig[i][strlen(oblig[i])-1]='\0';
			printf("last = %s\n",oblig[i]);
		}
		char * file = malloc((11+strlen(oblig[i]))*sizeof(char));
		strcpy(file,"Obligation/");
		strcat(file,oblig[i]);
		printf("oblig[%d] = %s et file =%s \n",i,oblig[i],file);
		ImageRGB * rgb = readBMPFile(file);
		printf("Image chargée \n");
		ImageHSV * hsv = imageRGBtoHSV(rgb);
		int nb = 0;
		ColorRegion ** region = blueDetect(rgb,hsv,&nb);
		printf("blueDetectOK\n");
		int nb2=0;
		ImageRGB ** List = sortOutPanel(region,nb,rgb->height,rgb->width,rgb,&nb2);
		printf("sortOut ok et nb2=%d\n",nb2);
		int a;
		int minWidth = List[0]->width;
		int minHeight = List[0]->height;
			for(a=0;a<nb2;a++){
				int regionNb = 0;
				ColorRegion ** coloredregions = growRegionFromImage(*(List[a]),&regionNb,50,50);
				ImageRGB * uniforme = getImageFromRegions(coloredregions,regionNb,List[a]->height,List[a]->width);
				char * name = malloc((15+strlen(oblig[i]))*sizeof(char));
				strcpy(name,"Obligation/LBP/");
				strcat(name,oblig[i]);
				ImageRGB * greyLevel = convertToGreyLevel(*uniforme);
				ImageRGB * LBP = applyLBPToImageRGB(greyLevel);
				if(List[a]->width>=minWidth && List[a]->height>=minHeight){
					writeBMPFile(*LBP,name);
					printf("iamge écrite\n");
				}
			}
	}

	printf("Obli OK\n");


	for(i=0;i<nbEInt;i++){
		while(EndInter[i][strlen(EndInter[i])-1]!='p'){
			printf("error et len = %d\n",strlen(EndInter[i]));
			EndInter[i][strlen(EndInter[i])-1]='\0';
			printf("last = %s\n",EndInter[i]);
		}
		char * file = malloc((16+strlen(EndInter[i]))*sizeof(char));
		strcpy(file,"EndInterdiction/");
		strcat(file,EndInter[i]);
		printf("file = %s\n",file);
		ImageRGB * rgb = readBMPFile(file);
		ImageHSV * hsv = imageRGBtoHSV(rgb);
		int nb = 0;
		ColorRegion ** region = whiteDetect(rgb,hsv,&nb);
		int nb2=0;
		ImageRGB ** List = sortOutPanel(region,nb,rgb->height,rgb->width,rgb,&nb2);
		int a;
		int minWidth = List[0]->width;
		int minHeight = List[0]->height;
			for(a=0;a<nb2;a++){
				int regionNb = 0;
				ColorRegion ** coloredregions = growRegionFromImage(*(List[a]),&regionNb,50,50);
				ImageRGB * uniforme = getImageFromRegions(coloredregions,regionNb,List[a]->height,List[a]->width);
				char * name = malloc((20+strlen(EndInter[i]))*sizeof(char));
				strcpy(name,"EndInterdiction/LBP/");
				strcat(name,EndInter[i]);
				ImageRGB * greyLevel = convertToGreyLevel(*uniforme);
				ImageRGB * LBP = applyLBPToImageRGB(greyLevel);
				if(List[a]->width>=minWidth && List[a]->height>=minHeight){
					writeBMPFile(*LBP,name);
					printf("iamge écrite\n");
				}
			}
	}

	printf("endinter ok \n");

	for(i=0;i<nbEOb;i++){
		while(EndOb[i][strlen(EndOb[i])-1]!='p'){
			printf("error et len = %d\n",strlen(EndOb[i]));
			EndOb[i][strlen(EndOb[i])-1]='\0';
			printf("last = %s\n",EndOb[i]);
		}
		char * file = malloc((14+strlen(EndOb[i]))*sizeof(char));
		strcpy(file,"EndObligation/");
		strcat(file,EndOb[i]);
		printf("file = %s\n",file);
		ImageRGB * rgb = readBMPFile(file);
		ImageHSV * hsv = imageRGBtoHSV(rgb);
		int nb = 0;
		ColorRegion ** region = blueDetect(rgb,hsv,&nb);
		int nb2=0;
		ImageRGB ** List = sortOutPanel(region,nb,rgb->height,rgb->width,rgb,&nb2);
		int a;
		int minWidth = List[0]->width;
		int minHeight = List[0]->height;
			for(a=0;a<nb2;a++){
				int regionNb = 0;
				ColorRegion ** coloredregions = growRegionFromImage(*(List[a]),&regionNb,50,50);
				ImageRGB * uniforme = getImageFromRegions(coloredregions,regionNb,List[a]->height,List[a]->width);
				char * name = malloc((18+strlen(EndOb[i]))*sizeof(char));
				strcpy(name,"EndObligation/LBP/");
				strcat(name,EndOb[i]);
				ImageRGB * greyLevel = convertToGreyLevel(*uniforme);
				ImageRGB * LBP = applyLBPToImageRGB(greyLevel);
				if(List[a]->width>=minWidth && List[a]->height>=minHeight){
					writeBMPFile(*LBP,name);
					printf("iamge écrite\n");
				}
			}
	}

}
