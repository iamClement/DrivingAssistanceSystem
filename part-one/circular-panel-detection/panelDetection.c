/*
 * paneldetection.c
 *
 *  Created on: 15 janv. 2019
 *      Author: anne
 */

#include "panelDetection.h"



ColorRegion ** redDetect(ImageRGB * rgb, ImageHSV * img,int * nbRegions){
	ColorRegion ** regions = (ColorRegion**)malloc(sizeof(ColorRegion*));


	Pixel * redPix = NULL;

	int i,j;

	for (i = 0; i < img->height; i++) {
		for(j = 0; j < img->width; j++){
			if((img->hueMatrix->data[i][j]>=350)||(img->hueMatrix->data[i][j]<=20)){
				if((img->saturationMatrix->data[i][j]>=70)&&(img->valueMatrix->data[i][j]>=70)){
					redPix = newPixel(j,i);
					int k;
					bool isInARegion = false;
					for(k=0;k<*nbRegions;k++){
						if(isPixelInColorRegion(*(regions[k]),*redPix)==true){
							isInARegion=true;
						}
					}
					if(isInARegion==false){
						*nbRegions+=1;
						regions = realloc(regions,*nbRegions*sizeof(ColorRegion*));
						ColorRGB * seedColor = getPixelColorOfImageRGB(*rgb,*redPix);
						regions[(*nbRegions)-1]=newColorRegion(*redPix,*seedColor);
						float localRate = 100;
						float globalRate = 100;
						growRegionFromPixel(regions,*nbRegions,*rgb,*redPix,localRate,globalRate);
					}

				}
			}
		}

	}

	return regions;
}

ColorRegion ** blueDetect(ImageRGB * rgb,ImageHSV * img,int*nbRegions){
	ColorRegion ** regions = (ColorRegion**)malloc(sizeof(ColorRegion*));
	Pixel * bluePix = NULL;

	int i,j;

	for (i = 0; i < img->height; i++) {
		for(j = 0; j < img->width; j++){
			if((img->hueMatrix->data[i][j]>=200)&&(img->hueMatrix->data[i][j]<=240)){
				if((img->saturationMatrix->data[i][j]>=70)&&(img->valueMatrix->data[i][j]>=70)){


					bool isInARegion = false;
						bluePix = newPixel(j,i);

					int k;
					for(k=0;k<*nbRegions;k++){
						if(isPixelInColorRegion(*(regions[k]),*bluePix)==true){
							isInARegion=true;
						}
					}
					if(isInARegion==false){
						*nbRegions+=1;
						regions = realloc(regions,*nbRegions*sizeof(ColorRegion*));
						ColorRGB * seedColor = getPixelColorOfImageRGB(*rgb,*bluePix);
						regions[(*nbRegions)-1]=newColorRegion(*bluePix,*seedColor);
						float localRate = 85;
						float globalRate = 85;
						growRegionFromPixel(regions,*nbRegions,*rgb,*bluePix,localRate,globalRate);
					}
				}
			}
		}
	}

	return regions;
}

ColorRegion ** whiteDetect(ImageRGB * rgb,ImageHSV * img,int*nbRegions){

	ColorRegion ** regions = (ColorRegion**)malloc(sizeof(ColorRegion*));
	Pixel * whitePix = NULL;

	int i,j;

	for(i=0;i<img->height;i++){
		for(j=0;j<img->width;j++){
			if((img->saturationMatrix->data[i][j]<=5)&&(img->valueMatrix->data[i][j]>=95)){
				freePixel(&whitePix);
				whitePix = newPixel(j,i);
				int k;
				bool isInARegion = false;
				for(k=0;k<*nbRegions;k++){
					if(isPixelInColorRegion(*(regions[k]),*whitePix)==true){
						isInARegion=true;
					}
				}
				if(isInARegion==false){

					*nbRegions+=1;
					regions = realloc(regions,*nbRegions*sizeof(ColorRegion*));

					ColorRGB * seedColor = getPixelColorOfImageRGB(*rgb,*whitePix);

					regions[(*nbRegions)-1]=newColorRegion(*whitePix,*seedColor);

					float localRate = 85;
					float globalRate = 85;
					growRegionFromPixel(regions,*nbRegions,*rgb,*whitePix,localRate,globalRate);

				}
			}
		}
	}

	return regions;
}

Circle * isRegionACircle(ImageRGB * imgRegion){
	RadonSpace * radon = radonTransformation(imgRegion);
	ImageRGB * radonImg = createRadonImage(radon,3,10);
	writeBMPFile(*radonImg,"Images/radon.bmp");
	Circle ** circles = circleDetection(imgRegion,radon);
	Circle ** newList = malloc(0);
	int nbCircle =0;
	int i;
	for(i=0;i<50;i++){
		if(circles[i]!=NULL){
			nbCircle+=1;
			newList = (Circle**)realloc(newList,nbCircle*sizeof(Circle*));
			newList[nbCircle-1]=circles[i];
		}
	}

	if(nbCircle==0){
		return NULL;
	}

	int maxradius = newList[0]->radius;
	Circle * maxCircle = newList[0];

	for (i = 1; i < nbCircle; i++) {
		if(newList[i]->radius<maxradius){
			maxradius = newList[i]->radius;
			maxCircle = newList[i];
		}

	}
	return maxCircle;


}


bool isPixelAnEdge(Pixel pixel, Matrix matrix)
{
	bool postulate = false;

	if(getValueOfMatrixAt(matrix,pixel.abscissa,pixel.ordinate) == 255)
	{
		if((getValueOfMatrixAt(matrix,pixel.abscissa,pixel.ordinate+1) == 0)||(getValueOfMatrixAt(matrix,pixel.abscissa,pixel.ordinate-1) == 0)||(getValueOfMatrixAt(matrix,pixel.abscissa+1,pixel.ordinate) == 0)||(getValueOfMatrixAt(matrix,pixel.abscissa-1,pixel.ordinate) == 0))
		{
			postulate = true;
		}
	}

	return postulate;
}

Matrix* newOutlineMatrix(Matrix matrix)
{
	Matrix* outlineMatrix = newMatrix(matrix.columnsNumber,matrix.rowsNumber);

	fillMatrix(outlineMatrix);

	int columnIndex, rowIndex;

	for(columnIndex = 0 ; columnIndex < matrix.columnsNumber ; columnIndex+=1)
	{
		for(rowIndex = 0 ; rowIndex < matrix.rowsNumber ; rowIndex+=1)
		{
			Pixel* pixel = newPixel(columnIndex,rowIndex);

			if(isPixelAnEdge(*pixel,matrix))
			{
				addValueToMatrixAt(outlineMatrix,255,columnIndex,rowIndex);
			}

			freePixel(&pixel);
		}
	}

	return outlineMatrix;
}

ImageRGB * newOutlineImageRGB(ImageRGB * img){
	ImageRGB * ret = newRGBImage(img->height,img->width);
	ret->blueMatrix = newOutlineMatrix(*(img->blueMatrix));
	ret->redMatrix = newOutlineMatrix(*(img->redMatrix));
	ret->greenMatrix = newOutlineMatrix(*(img->greenMatrix));

	return ret;

}

void ManualThershold(ImageRGB * img, int thershold)
{

	int i,j;

	for(i=0;i<img->height;i++){
		for(j=0;j<img->width;j++){
			if(img->blueMatrix->data[i][j]>thershold){
				img->blueMatrix->data[i][j]=255;
			}
			else{
				img->blueMatrix->data[i][j]=0;
			}
			if(img->redMatrix->data[i][j]>thershold){
				img->redMatrix->data[i][j]=255;
			}
			else{
				img->redMatrix->data[i][j]=0;
			}
			if(img->greenMatrix->data[i][j]>thershold){
				img->greenMatrix->data[i][j]=255;
			}
			else{
				img->greenMatrix->data[i][j]=0;
			}
		}

	}

}

ImageRGB * cropImage(ImageRGB * src){
	Matrix * newMat = getCroppedMatrixFromMatrix(*(src->blueMatrix));

	ImageRGB * dest = newRGBImage(newMat->rowsNumber,newMat->columnsNumber);
	dest->blueMatrix = newMat;
	dest->redMatrix = getCroppedMatrixFromMatrix(*(src->redMatrix));
	dest->greenMatrix = getCroppedMatrixFromMatrix(*(src->greenMatrix));

	return dest;

}

int getShapeMinColumn(Matrix matrix)
{
	int minColumn = -1;

	int columnIndex, rowIndex;

	for(columnIndex = 0 ; columnIndex < matrix.columnsNumber ; columnIndex+=1)
	{
		for(rowIndex = 0 ; rowIndex < matrix.rowsNumber ; rowIndex+=1)
		{
			if(getValueOfMatrixAt(matrix,columnIndex,rowIndex) == 255)
			{
				minColumn = columnIndex;
			}
		}

		if(minColumn != -1)
		{
			break;
		}
	}

	return minColumn;
}

int getShapeMaxColumn(Matrix matrix)
{
	int maxColumn = -1;

	int columnIndex, rowIndex;

	for(columnIndex = matrix.columnsNumber-1 ; columnIndex >= 0 ; columnIndex-=1)
	{
		for(rowIndex = 0 ; rowIndex < matrix.rowsNumber ; rowIndex+=1)
		{
			if(getValueOfMatrixAt(matrix,columnIndex,rowIndex) == 255)
			{
				maxColumn = columnIndex;
			}
		}

		if(maxColumn != -1)
		{
			break;
		}
	}

	return maxColumn;
}

int getShapeMinRow(Matrix matrix)
{
	int minRow = -1;

	int columnIndex, rowIndex;

	for(rowIndex = 0 ; rowIndex < matrix.rowsNumber ; rowIndex+=1)
	{
		for(columnIndex = 0 ; columnIndex < matrix.columnsNumber ; columnIndex+=1)
		{
			if(getValueOfMatrixAt(matrix,columnIndex,rowIndex) == 255)
			{
				minRow = rowIndex;
			}
		}

		if(minRow != -1)
		{
			break;
		}
	}

	return minRow;
}

int getShapeMaxRow(Matrix matrix)
{
	int maxRow = -1;

	int columnIndex, rowIndex;

	for(rowIndex = matrix.rowsNumber-1 ; rowIndex >= 0 ; rowIndex-=1)
	{
		for(columnIndex = 0 ; columnIndex < matrix.columnsNumber ; columnIndex+=1)
		{
			if(getValueOfMatrixAt(matrix,columnIndex,rowIndex) == 255)
			{
				maxRow = rowIndex;
			}
		}

		if(maxRow != -1)
		{
			break;
		}
	}

	return maxRow;
}


Matrix* getCroppedMatrixFromMatrix(Matrix matrix)
{
	int shapeMinColumn = getShapeMinColumn(matrix);
	int shapeMaxColumn = getShapeMaxColumn(matrix);
	int shapeMinRow = getShapeMinRow(matrix);
	int shapeMaxRow = getShapeMaxRow(matrix);

	unsigned int columnNumber = (unsigned int) (shapeMaxColumn - shapeMinColumn + 1);
	unsigned int rowNumber = (unsigned int) (shapeMaxRow - shapeMinRow + 1);

	Matrix* croppedMatrix = newMatrix(columnNumber,rowNumber);

	fillMatrix(croppedMatrix);

	int columnIndex, rowIndex;

	for(columnIndex = shapeMinColumn ; columnIndex <= shapeMaxColumn ; columnIndex+=1)
	{
		for(rowIndex = shapeMinRow ; rowIndex <= shapeMaxRow ; rowIndex+=1)
		{
			addValueToMatrixAt(croppedMatrix,getValueOfMatrixAt(matrix,columnIndex,rowIndex),columnIndex-shapeMinColumn,rowIndex-shapeMinRow);
		}
	}

	return croppedMatrix;
}

ImageRGB ** sortOutPanel(ColorRegion ** in, int nbRegions,int height, int width, ImageRGB * src,int * nbPanels){
	int i;
	ImageRGB ** panelList = malloc(0);
	for(i=0;i<nbRegions;i++){
		ImageRGB * regionImage = getImageFromRegion(*(in[i]),height,width);
		regionImage = convertToGreyLevel(*regionImage);
		ManualThershold(regionImage,10);
		ImageRGB * regionImage2 = newOutlineImageRGB(regionImage);
		ImageRGB * cropped = cropImage(regionImage2);
		if(cropped->height>10 && cropped->width>10){
			Circle * isACircle = isRegionACircle(cropped);
			if(isACircle!=NULL){

				if(isACircle->radius<=10){
					in[i]=NULL;
				}
				else{
					ImageRGB * currentImage = CoIofImage(src,isACircle,regionImage2);
					*nbPanels+=1;
					panelList = realloc(panelList,(*nbPanels)*sizeof(ImageRGB*));
					panelList[(*nbPanels)-1]=currentImage;

				}

			}
			else{
				in[i]=NULL;
			}
		}
		else{
			in[i]=NULL;
		}

	}

	return panelList;

}

ImageRGB * CoIofImage(ImageRGB * src, Circle * sizeToCrop,ImageRGB * regionImage){

	int i,j;
	int minX = getShapeMinColumn(*(regionImage->blueMatrix));
	int minY = getShapeMinRow(*(regionImage->blueMatrix));

	double y = sizeToCrop->radius * (sqrt(2)/2);
	int Iy = (int)y;
	double x = sizeToCrop->radius *sqrt(2)/2;
	int Ix = (int)x;

	int debX = minX-Ix+sizeToCrop->abscissaCenter;
	int finX = minX+Ix+sizeToCrop->abscissaCenter;

	if(debX<0){
		debX=0;
	}
	if(debX>=src->width){
			debX=src->width-1;
		}

	int debY = minY-Iy+sizeToCrop->ordinateCenter;
	int finY = minY+Iy+sizeToCrop->ordinateCenter;

	if(debY<0){
		debY=0;
	}
	if(debY>=src->height){
		debY=src->height-1;
	}

	ImageRGB * ret = newRGBImage(Iy*2,Ix*2);

	int k=0,l=0;

	for (i = 0; i < ret->height; i++) {
		for(j = 0; j< ret->width; j++){
			int newX = j+debX;
			int newY = i+debY;

			if(newX>=src->width){
				newX=src->width-1;
			}
			if(newY>=src->height){
				newY = src->height-1;
			}

			ret->blueMatrix->data[i][j]=src->blueMatrix->data[newY][newX];
			ret->redMatrix->data[i][j]=src->redMatrix->data[newY][newX];
			ret->greenMatrix->data[i][j]=src->greenMatrix->data[newY][newX];
			k++;

		}
		l++;
		k=0;

	}

	writeBMPFile(*ret,"Images/newCropped.bmp");
	return ret;
}

ImageRGB * croppedCompleteImage(ImageRGB * src, ImageRGB * thersholded){
	Matrix * matrix = thersholded->blueMatrix;
	int shapeMinColumn = getShapeMinColumn(*matrix);
	int shapeMaxColumn = getShapeMaxColumn(*matrix);
	int shapeMinRow = getShapeMinRow(*matrix);
	int shapeMaxRow = getShapeMaxRow(*matrix);

	if(shapeMinColumn>=3){
		shapeMinColumn-=3;
	}
	if(shapeMaxColumn<=src->width-3){
		shapeMaxColumn+=3;
	}
	if(shapeMinRow>=3){
		shapeMinRow-=3;
	}
	if(shapeMaxRow<=src->height-3){
		shapeMaxRow+=3;
	}

	unsigned int columnNumber = (unsigned int) (shapeMaxColumn - shapeMinColumn + 1);
	unsigned int rowNumber = (unsigned int) (shapeMaxRow - shapeMinRow + 1);

	ImageRGB* croppedImage = newRGBImage(rowNumber,columnNumber);

	int columnIndex, rowIndex;

	for(columnIndex = shapeMinColumn ; columnIndex <= shapeMaxColumn ; columnIndex+=1)
	{
		for(rowIndex = shapeMinRow ; rowIndex <= shapeMaxRow ; rowIndex+=1)
		{
			croppedImage->blueMatrix->data[rowIndex-shapeMinRow][columnIndex-shapeMinColumn]=src->blueMatrix->data[rowIndex][columnIndex];
			croppedImage->greenMatrix->data[rowIndex-shapeMinRow][columnIndex-shapeMinColumn]=src->greenMatrix->data[rowIndex][columnIndex];
			croppedImage->redMatrix->data[rowIndex-shapeMinRow][columnIndex-shapeMinColumn]=src->redMatrix->data[rowIndex][columnIndex];

		}
	}

	return croppedImage;


}


Matrix* applyLBPToMatrix(Matrix matrix)
{
	Matrix* modifiedMatrix = newMatrix(matrix.columnsNumber,matrix.rowsNumber);

	Filter* binaryFilter = newFilter(3,3,1,1,1);

	fillMatrix(binaryFilter->filterMatrix);

	addValueToMatrixAt(binaryFilter->filterMatrix,1,0,0);
	addValueToMatrixAt(binaryFilter->filterMatrix,2,1,0);
	addValueToMatrixAt(binaryFilter->filterMatrix,4,2,0);
	addValueToMatrixAt(binaryFilter->filterMatrix,8,2,1);
	addValueToMatrixAt(binaryFilter->filterMatrix,16,2,2);
	addValueToMatrixAt(binaryFilter->filterMatrix,32,1,2);
	addValueToMatrixAt(binaryFilter->filterMatrix,64,0,2);
	addValueToMatrixAt(binaryFilter->filterMatrix,128,1,0);

	int columnIndex, rowIndex;

	for(columnIndex = 0 ; columnIndex < modifiedMatrix->columnsNumber ; columnIndex+=1)
	{
		for(rowIndex = 0 ; rowIndex < modifiedMatrix->rowsNumber ; rowIndex+=1)
		{
			int filterColumnIndex, filterRowIndex;

			Filter* binaryValueFilter = newFilter(3,3,1,1,1);

			fillMatrix(binaryValueFilter->filterMatrix);

			int currentPixelValue = getValueOfMatrixAt(matrix,columnIndex,rowIndex);

			for(filterColumnIndex = 0 ; filterColumnIndex < 3 ; filterColumnIndex+=1)
			{
				for(filterRowIndex = 0 ; filterRowIndex < 3 ; filterRowIndex+=1)
				{
					if((filterColumnIndex != 1)&&(filterRowIndex != 1))
					{
						int neighbourValue = getValueOfMatrixAt(matrix,columnIndex-1-filterColumnIndex,rowIndex-1-filterRowIndex);

						int neighbourCoefficient = getValueOfMatrixAt(*binaryFilter->filterMatrix,filterColumnIndex,filterRowIndex);

						if(currentPixelValue <= neighbourValue)
						{
							addValueToMatrixAt(binaryValueFilter->filterMatrix,neighbourCoefficient,filterColumnIndex,filterRowIndex);
						}
					}
				}
			}

			int additionColumnIndex, additionRowIndex;

			int additionValue = 0;

			for(additionColumnIndex = 0 ; additionColumnIndex < 3 ; additionColumnIndex+=1)
			{
				for(additionRowIndex = 0 ; additionRowIndex < 3 ; additionRowIndex+=1)
				{
					additionValue+=getValueOfMatrixAt(*binaryValueFilter->filterMatrix,additionColumnIndex,additionRowIndex);
				}
			}

			addValueToMatrixAt(modifiedMatrix,additionValue,columnIndex,rowIndex);

			freeFilter(&binaryValueFilter);

		}
	}

	return modifiedMatrix;
}

ImageRGB * applyLBPToImageRGB(ImageRGB * src){
	ImageRGB * dest = newRGBImage(src->height,src->width);
	dest->blueMatrix = applyLBPToMatrix(*(src->blueMatrix));
	dest->redMatrix = applyLBPToMatrix(*(src->redMatrix));
	dest->greenMatrix = applyLBPToMatrix(*(src->greenMatrix));

	return dest;
}

int * createHistogramFromGreyImage(ImageRGB * Grey){
	int * hist = (int *)malloc(256*sizeof(int));
	int h;
	for(h=0;h<256;h+=1){
		hist[h]=0;
	}
	int i,j;
	for(i=0;i<Grey->height;i+=1){
		for(j=0;j<Grey->width;j+=1){
			hist[Grey->blueMatrix->data[i][j]]+=1;
		}
	}

	return hist;
}

double * balancedHistogram(ImageRGB * Grey){
	int * hist = createHistogramFromGreyImage(Grey);
	int nbPix = Grey->height*Grey->height;
	double * balancedHist = (double *)malloc(256*sizeof(double));
	int i;
	for(i=0;i<256;i++){
		double h = (double)hist[i];
		double pix = (double)nbPix;
		double r = h/pix;
		balancedHist[i]=r;
	}

	return balancedHist;
}

double histogramAreTheSame(double * hist, double * compHist){
	int i;
	double result;
	double sum=0;
	for(i=0;i<256;i++){

		result = hist[i]-compHist[i];
		if(result<0){
			result=-result;
		}
		sum += result;
	}

	return sum;
}

PanelType classifyBluePanel(ImageRGB * panel)
{
	PanelType panelType=OBLIGATION;

	ImageHSV * HSVpanel  = imageRGBtoHSV(panel);

	int i,j;

	for(i=0;i<HSVpanel->height;i++){
		for(j=0;j<HSVpanel->width;j++){
			if(isRed(j,i,HSVpanel)==1){
				return END_OBLIGATION;
			}
		}
	}

	return panelType;
}

PanelName namePanel(ImageRGB * panel, PanelType type){
	PanelName name = NOT_DEFINED;
	if(type==OBLIGATION){
		name = nameObligationPanel(panel);
	}
	else if(type==INTERDICTION){
		name = nameInterdictionPanel(panel);
	}
	else if(type==END_INTERDICTION){
		name = nameEndInterdictionPanel(panel);
	}
	else if(type==END_OBLIGATION){
		name = nameEndObligationPanel(panel);
	}

	return name;
}

PanelName nameObligationPanel(ImageRGB * panel){
	PanelName namePanel = NOT_DEFINED;

	int nbFiles = 0;
	char ** list = listBMPdir("part-one/circular-panel-detection/Images/Obligation/LBP",&nbFiles);
	ImageRGB * grey = convertToGreyLevel(*panel);
	ImageRGB * LBP = applyLBPToImageRGB(grey);
	writeBMPFile(*LBP,"part-one/circular-panel-detection/Images/imgLBP.bmp");
	double * histo = balancedHistogram(LBP);
	int i;
	double ** hists = malloc(nbFiles*sizeof(float*));
	while(list[0][strlen(list[0])-1]!='p'){
		list[0][strlen(list[0])-1]='\0';
	}
	char * name = malloc((24+strlen(list[0]))*sizeof(char));
	strcpy(name,"part-one/circular-panel-detection/Images/Obligation/LBP/");
	strcat(name,list[0]);
	ImageRGB * cmp = readBMPFile(name);
	hists[0]=balancedHistogram(cmp);
	double min = histogramAreTheSame(hists[0],histo);



	char * minChar = list[0];
	for(i=1;i<nbFiles;i++){
		while(list[i][strlen(list[i])-1]!='p'){
			list[i][strlen(list[i])-1]='\0';
		}
		char * nameI = malloc((24+strlen(list[i]))*sizeof(char));
		strcpy(nameI,"part-one/circular-panel-detection/Images/Obligation/LBP/");
		strcat(nameI,list[i]);
		ImageRGB * cmpI = readBMPFile(nameI);
		hists[i]=balancedHistogram(cmpI);
		double result = histogramAreTheSame(hists[i],histo);

		if(result<min){
			min = result;
			minChar = list[i];
		}

	}

	if(min>=0.2){
		return NOT_DEFINED;
	}

	char Right[] = "droite.bmp";
	if(strcmp(minChar,Right)==0){
		return TURN_RIGHT_NEXT_INTERSECTION;
	}

	return namePanel;
}

PanelName nameInterdictionPanel(ImageRGB * panel){
	PanelName namePanel = NOT_DEFINED;

	ImageHSV * hsv = imageRGBtoHSV(panel);

	//is it a no stop/ no car panel (top center pixel blue)
	if(isBlue(panel->width/2,0,hsv)==true){
		//no stop (top left corner red
		if(isRed(0,0,hsv)==true){
			return NO_STOP;
		}

		return NO_PARKING;
	}

	//wrong way
	else if(isRed(panel->width/2,0,hsv)==true){
		return WRONG_WAY;
	}


	//icon recognition
	else{
		int nbFiles = 0;
		char ** list = listBMPdir("part-one/circular-panel-detection/Images/Interdiction/LBP",&nbFiles);
		ImageRGB * grey = convertToGreyLevel(*panel);
		ImageRGB * LBP = applyLBPToImageRGB(grey);
		writeBMPFile(*LBP,"part-one/circular-panel-detection/Images/imgLBP.bmp");
		double * histo = balancedHistogram(LBP);
		int i;
		double ** hists = malloc(nbFiles*sizeof(float*));
		while(list[0][strlen(list[0])-1]!='p'){
			list[0][strlen(list[0])-1]='\0';
		}
		char * name = malloc((58+strlen(list[0]))*sizeof(char));
		strcpy(name,"part-one/circular-panel-detection/Images/Interdiction/LBP/");
		strcat(name,list[0]);
		ImageRGB * cmp = readBMPFile(name);
		hists[0]=balancedHistogram(cmp);
		double min = histogramAreTheSame(hists[0],histo);



		char * minChar = list[0];
		for(i=1;i<nbFiles;i++){
			while(list[i][strlen(list[i])-1]!='p'){
				list[i][strlen(list[i])-1]='\0';
			}
			char * nameI = malloc((58+strlen(list[i]))*sizeof(char));
			strcpy(nameI,"part-one/circular-panel-detection/Images/Interdiction/LBP/");
			strcat(nameI,list[i]);
			ImageRGB * cmpI = readBMPFile(nameI);
			hists[i]=balancedHistogram(cmpI);
			double result = histogramAreTheSame(hists[i],histo);

			if(result<min){
				min = result;
				minChar = list[i];
			}
		}

		//we find the closest histogram
		char LBP50[] = "50LBP.bmp";
		char LBP80[] = "80LBP.bmp";
		if(min>=0.2){
			return NOT_DEFINED;
		}
		if(strcmp(minChar,LBP50)==0){
			return MAX50KMH;
		}

		if(strcmp(minChar,LBP80)==0){
			return MAX80KMH;
		}
	}


	return namePanel;

}

PanelName nameEndInterdictionPanel(ImageRGB * panel){
	PanelName name = NOT_DEFINED;



	return name;

}

PanelName nameEndObligationPanel(ImageRGB * panel){
	PanelName namePanel = NOT_DEFINED;

		int nbFiles = 0;
		char ** list = listBMPdir("part-one/circular-panel-detection/Images/EndObligation/LBP",&nbFiles);
		ImageRGB * grey = convertToGreyLevel(*panel);
		ImageRGB * LBP = applyLBPToImageRGB(grey);
		writeBMPFile(*LBP,"part-one/circular-panel-detection/Images/imgLBP.bmp");
		double * histo = balancedHistogram(LBP);
		int i;
		double ** hists = malloc(nbFiles*sizeof(float*));
		while(list[0][strlen(list[0])-1]!='p'){
			list[0][strlen(list[0])-1]='\0';
		}
		char * name = malloc((24+strlen(list[0]))*sizeof(char));
		strcpy(name,"part-one/circular-panel-detection/Images/EndObligation/LBP/");
		strcat(name,list[0]);
		ImageRGB * cmp = readBMPFile(name);
		hists[0]=balancedHistogram(cmp);
		double min = histogramAreTheSame(hists[0],histo);

		char * minChar = list[0];
		for(i=1;i<nbFiles;i++){
			while(list[i][strlen(list[i])-1]!='p'){
				list[i][strlen(list[i])-1]='\0';
			}
			char * nameI = malloc((24+strlen(list[i]))*sizeof(char));
			strcpy(nameI,"part-one/circular-panel-detection/Images/EndObligation/LBP/");
			strcat(nameI,list[i]);
			ImageRGB * cmpI = readBMPFile(nameI);
			hists[i]=balancedHistogram(cmpI);
			double result = histogramAreTheSame(hists[i],histo);

			if(result<min){
				min = result;
				minChar = list[i];
			}

		}
		if(min>=0.2){
			return NOT_DEFINED;
		}

		char RiderEnd[] = "chevalend.bmp";
		if(strcmp(minChar,RiderEnd)==0){
			return END_HORSERIDERS_ONLY;
		}


	return namePanel;

}

char ** listBMPdir(char * dirName,int * nbFiles){
	char ** listFiles = malloc(0);

	DIR * rep = opendir(dirName);
	if (rep != NULL){
		struct dirent * ent;
		while ((ent = readdir(rep)) != NULL){
			//name more than 4 chars
			if(strlen(ent->d_name)>4){
				char * endName = (char *)malloc(5*sizeof(char));
				int i;
				for(i=3;i>=0;i--){
					int ind = strlen(ent->d_name)-4+i;
					endName[i]=ent->d_name[ind];
				}
				endName[4]='\0';

				if(endName[0]=='.'&&endName[1]=='b'&&endName[2]=='m'&&endName[3]=='p'){
					*nbFiles+=1;
					listFiles = (char **)realloc(listFiles,*nbFiles*sizeof(char *));
					//char * tmp = (char *)malloc(strlen(ent->d_name));
					listFiles[(*nbFiles)-1]=(char *)malloc(strlen(ent->d_name)*sizeof(char));
					int a;
					for(a=0;a<strlen(ent->d_name);a++){
						listFiles[(*nbFiles)-1][a]=ent->d_name[a];
					}
				}


			}

		}
	closedir(rep);
	}

	return listFiles;

}


bool isRed(int x, int y, ImageHSV * hsv){
	if((hsv->hueMatrix->data[y][x]>=350)||(hsv->hueMatrix->data[y][x]<=20)){
		if((hsv->saturationMatrix->data[y][x]>=70)&&(hsv->valueMatrix->data[y][x]>=70)){
			return true;
		}
	}

	return false;

}

bool isBlue(int x, int y, ImageHSV * hsv){
	if((hsv->hueMatrix->data[y][x]>=200)&&(hsv->hueMatrix->data[y][x]<=240)){
		if((hsv->saturationMatrix->data[y][x]>=70)&&(hsv->valueMatrix->data[y][x]>=70)){
			return true;
		}
	}
	return false;
}
