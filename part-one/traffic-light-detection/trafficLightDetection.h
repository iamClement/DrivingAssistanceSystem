#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../../elements/image/ImageRGB.h"
#include "../../elements/image/ImageHSV.h"
#include "../../elements/image/RGBtoHSV.h"
#include "../../elements/region/ColorRegion.h"
#include "../circular-panel-detection/CircleDetection.h"

ColorRegion ** redDetection(ImageRGB * rgb, int* nbRegions);
ColorRegion ** orangeDetection(ImageRGB * rgb, int* nbRegions);
ColorRegion ** greenDetection(ImageRGB * rgb, int* nbRegions);
ColorRegion ** blackDetection(ImageRGB * rgb, int* nbRegions);
void printTrafficLight(ImageRGB * RGB, Circle* feu);