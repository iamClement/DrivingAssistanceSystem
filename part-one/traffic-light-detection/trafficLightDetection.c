/*
 * TrafficLightDetection.c
 *
 *  Created on: 18 janv. 2019
 *      Author: Clément
 */

#include "../circular-panel-detection/panelDetection.h"
#include "trafficLightDetection.h"
#include "../../libs/matrixlib/extensions/image/Filter.h"
#include <stdio.h>
#include <stdlib.h>


int main(void){


	ImageRGB * RGB = readBMPFile("traffic-light-1.jpg");
	printf("Image chargée \n");

	int nb=0;
	printf("First, detection of the traffic light black box");
	ColorRegion ** blackRegions = blackDetection(RGB,&nb);
	int i;
	Circle * feuRouge;
	Circle * feuOrange;
	Circle * feuVert;

	//Detection des régions noirs
	for(i=0;i<nb;i++){
		printf("region = %p \n",blackRegions[i]);

		//si il y a des régions noirs
		if(blackRegions[i]!=NULL){

			//création d'images de type ImageRGB à partir des régions noirs
			ImageRGB * regionImage = getImageFromRegion(*(blackRegions[i]),RGB->height,RGB->width);
			ImageRGB * croppedRGB = croppedCompleteImage(RGB,regionImage);

			//Détections de régions rouges/orange/verte à partir de ces images
			ColorRegion ** redRegions = redDetection(croppedRGB,&nb);
			if (redRegions[i]!=NULL){
			//Création d'une image de type
			ImageRGB * imgRouge = getImageFromRegion(*(redRegions[i]),RGB->height,RGB->width);
			//Vérification que cette régions est un cercle
			feuRouge = isRegionACircle(imgRouge);
			printf("feu rouge détectée\n");
			printTrafficLight(RGB, feuRouge);
			}


			ColorRegion ** orangeRegions = orangeDetection(croppedRGB,&nb);
			if (orangeRegions[i]!=NULL){
			//Création d'une image de type
			ImageRGB * imgOrange = getImageFromRegion(*(orangeRegions[i]),RGB->height,RGB->width);
			//Vérification que cette régions est un cercle
			feuOrange = isRegionACircle(imgOrange);
			printf("feu orange détectée\n");
			printTrafficLight(RGB, feuOrange);
			}


			ColorRegion ** greenRegions = greenDetection(croppedRGB,&nb);
			if (greenRegions[i]!=NULL){
			//Création d'une image de type
			ImageRGB * imgGreen = getImageFromRegion(*(greenRegions[i]),RGB->height,RGB->width);
			//Vérification que cette régions est un cercle
			feuVert = isRegionACircle(imgGreen);
			printf("feu vert détectée\n");
			printTrafficLight(RGB, feuVert);

			}
		}
	}
}

//red light color detect
ColorRegion ** redDetection(ImageRGB * rgb ,int * nbRegions){
	ColorRegion ** regions = (ColorRegion**)malloc(sizeof(ColorRegion*));
	Pixel * redPix = NULL;

	int i,j;

	for (i = 0; i < rgb->height; i++) {
		for(j = 0; j < rgb->width; j++){
			if(rgb->redMatrix->data[i][j]<200){
				if((rgb->blueMatrix->data[i][j]<20)&&(rgb->greenMatrix->data[i][j]<20)){
					redPix = newPixel(j,i);
					int k;
					bool isInARegion = false;
					for(k=0;k<*nbRegions;k++){
						if(isPixelInColorRegion(*(regions[k]),*redPix)==true){
							isInARegion=true;
						}
					}
					if(isInARegion==false){
						*nbRegions+=1;
						regions = realloc(regions,*nbRegions*sizeof(ColorRegion*));
						ColorRGB * seedColor = getPixelColorOfImageRGB(*rgb,*redPix);
						regions[(*nbRegions)-1]=newColorRegion(*redPix,*seedColor);
						float localRate = 100;
						float globalRate = 100;
						growRegionFromPixel(regions,*nbRegions,*rgb,*redPix,localRate,globalRate);
						printf("newregion\n");
					}
				}
			}
		}
	}

	return regions;
}
//orange light color detect
ColorRegion ** orangeDetection(ImageRGB * rgb, int * nbRegions){
	ColorRegion ** regions = (ColorRegion**)malloc(sizeof(ColorRegion*));
	Pixel * orangePix = NULL;

	int i,j;

	for (i = 0; i < rgb->height; i++) {
		for(j = 0; j < rgb->width; j++){
			if(rgb->redMatrix->data[i][j]<235){
				if((rgb->blueMatrix->data[i][j]<42)&&(rgb->greenMatrix->data[i][j]<155)){
					orangePix = newPixel(j,i);
					int k;
					bool isInARegion = false;
					for(k=0;k<*nbRegions;k++){
						if(isPixelInColorRegion(*(regions[k]),*orangePix)==true){
							isInARegion=true;
						}
					}
					if(isInARegion==false){
						*nbRegions+=1;
						regions = realloc(regions,*nbRegions*sizeof(ColorRegion*));
						ColorRGB * seedColor = getPixelColorOfImageRGB(*rgb,*orangePix);
						regions[(*nbRegions)-1]=newColorRegion(*orangePix,*seedColor);
						float localRate = 100;
						float globalRate = 100;
						growRegionFromPixel(regions,*nbRegions,*rgb,*orangePix,localRate,globalRate);
						printf("newregion\n");
					}
				}
			}
		}
	}

	return regions;
}

//green light color detect
ColorRegion ** greenDetection(ImageRGB * rgb,int*nbRegions){
	ColorRegion ** regions = (ColorRegion**)malloc(sizeof(ColorRegion*));
	Pixel * greenPix = NULL;

	int i,j;

	for (i = 0; i < rgb->height; i++) {
		for(j = 0; j < rgb->width; j++){
			if(rgb->greenMatrix->data[i][j]<235){
				if((rgb->blueMatrix->data[i][j]<60)&&(rgb->greenMatrix->data[i][j]<20)){
					greenPix = newPixel(j,i);
					int k;
					bool isInARegion = false;
					for(k=0;k<*nbRegions;k++){
						if(isPixelInColorRegion(*(regions[k]),*greenPix)==true){
							isInARegion=true;
						}
					}
					if(isInARegion==false){
						*nbRegions+=1;
						regions = realloc(regions,*nbRegions*sizeof(ColorRegion*));
						ColorRGB * seedColor = getPixelColorOfImageRGB(*rgb,*greenPix);
						regions[(*nbRegions)-1]=newColorRegion(*greenPix,*seedColor);
						float localRate = 85;
						float globalRate = 85;
						growRegionFromPixel(regions,*nbRegions,*rgb,*greenPix,localRate,globalRate);
						printf("newregion\n");
					}
				}
			}
		}
	}

	return regions;
}

//black box traffic light detection
ColorRegion ** blackDetection(ImageRGB * rgb,int*nbRegions){

	ColorRegion ** regions = (ColorRegion**)malloc(sizeof(ColorRegion*));
	Pixel * blackPix = NULL;

	int i,j;

	for (i = 0; i < rgb->height; i++) {
		for(j = 0; j < rgb->width; j++){
			if(rgb->redMatrix->data[i][j]<10){
				if((rgb->blueMatrix->data[i][j]<10)&&(rgb->greenMatrix->data[i][j]<10)){
				blackPix = newPixel(j,i);
				int k;
				bool isInARegion = false;
				for(k=0;k<*nbRegions;k++){
					if(isPixelInColorRegion(*(regions[k]),*blackPix)==true){
						isInARegion=true;
					}
				}
				if(isInARegion==false){

					*nbRegions+=1;
					regions = realloc(regions,*nbRegions*sizeof(ColorRegion*));

					ColorRGB * seedColor = getPixelColorOfImageRGB(*rgb,*blackPix);

					regions[(*nbRegions)-1]=newColorRegion(*blackPix,*seedColor);

					float localRate = 85;
					float globalRate = 85;
					growRegionFromPixel(regions,*nbRegions,*rgb,*blackPix,localRate,globalRate);

				}
			}
		}
	}
}
	return regions;
}




//affiche le feu sur l'image
void  printTrafficLight(ImageRGB * RGB, Circle* feu){

	int xmin = maximumInt(0, feu->abscissaCenter - feu->radius);
	int xmax = minimumInt(RGB->width, feu->abscissaCenter + feu->radius);
	int ymin = maximumInt(0,feu->ordinateCenter - feu->radius);
	int ymax = minimumInt(RGB->height,feu->ordinateCenter + feu->radius);

	for(int i=xmin; i<=xmax ; i++)
	{
		for(int j=ymin; j<=ymax ; j++)
		{
			if (feu->radius ==(int)(sqrt(pow(i-feu->abscissaCenter,2) + pow(j-feu->ordinateCenter,2))))
				RGB->redMatrix->data[i][j] = 255 ;
		}
	}

	writeBMPFile(*RGB,"feu.bmp");
	freeRGBImage(&RGB);

}
