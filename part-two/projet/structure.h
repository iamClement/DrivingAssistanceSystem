#ifndef STRUCTURE_H
#define STRUCTURE_H

typedef struct MAP{
	int **matrice_principale;
	int hauteurmatrice;
	int largeurmatrice;
	int jardin;
	int route;
	int routeD;
	int routeG;
	int voiture;
	int taille;
	int central;
	int routebis;
}MAP;

typedef struct COORD{
	int h;
	int l;
	int stop;
	double angle;
	int choix;
}COORD;

#endif