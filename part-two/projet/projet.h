#ifndef PROJET_H
#define PROJET_H
#include "structure.h"

MAP *allocation();
void affichermatrice(int **matrice, int h, int l);
void finitionmap(MAP *map);
void tracage(MAP *map, int h, int l, int adj, int opp);
COORD *dg(MAP *map, COORD *coord, int hyp, double alpha, int choix);
MAP *principale(MAP *map);
void movecar(MAP *map);

#endif