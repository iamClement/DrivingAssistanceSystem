#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "ESLib.h" // Pour utiliser valeurAleatoire()

#include "projet.h"
#include "structure.h"


MAP *allocation(){
	printf("allocation\n");
	int **matrice = NULL;
	int hauteur = 200, largeur= 51;

	MAP *map = malloc(sizeof(MAP));

	matrice = calloc(hauteur,sizeof(int*));
	for(int i=0;i<hauteur;i++){
		matrice[i] = calloc(largeur,sizeof(int));
	}

	map->matrice_principale = matrice;
	map->hauteurmatrice = hauteur;
	map->largeurmatrice = largeur;
	map->jardin = 0;
	map->route = 1;
	map->limitation = 2;
	map->voiture = 555;
	map->taille = 10;

	return map;
}

void affichermatrice(int **matrice, int h, int l){
	
	printf("\t**********\n");
	for(int i=0;i<h;i++){
		for(int j=0;j<l;j++){
			printf("%d ",matrice[i][j]);
		}
		printf("\n");
	}
	printf("\t**********\n");
}

void finitionmap(MAP *map){

	for(int i=0;i<map->hauteurmatrice;i++){
		for(int j=0;j<map->largeurmatrice;j++){
			if(map->matrice_principale[i][j] == 0){
				map->matrice_principale[i][j] = map->jardin;
			}
		}
	}
}

void tracage(MAP *map, int h, int l, int adj, int opp){
	printf("tracage\n");

	for(int i=h;i>h-adj;i--){
		if(i > adj/2){
			map->matrice_principale[i][l] = map->route;
			map->matrice_principale[i][l-1] = map->limitation;
			map->matrice_principale[i][l+1] = map->limitation;
		}else{
			map->matrice_principale[i][l+opp] = map->route;
			map->matrice_principale[i][l+opp+1] = map->limitation;
			map->matrice_principale[i][l+opp-1] = map->limitation;
		}
	}
}

COORD *dg(MAP *map, COORD *coord, int hyp, double alpha, int choix){
	int newh=0, newl=0;
	int adj=0, opp=0;

	adj = abs(cos(alpha)*hyp);

	if(adj == 10){
		//ligne
		opp = (sin(alpha)*hyp);
		coord->choix = 2;
	}else{
		if(choix == 0){
			//gauche
			opp = (sin(alpha)*hyp);
			coord->choix = 0;
		}else if(choix == 1){
			//droite
			opp = -(sin(alpha)*hyp);
			coord->choix = 1;
		}
	}

	printf("adj=%d | opp=%d | alpha=%f \n",adj,opp,alpha);

	newh = coord->h - adj;
	newl = coord->l - opp;

	if(newh > 0){
		if(newl > 0 && newl < map->largeurmatrice - 1){
			tracage(map,coord->h,coord->l,adj,opp);
			coord->h = newh;
			coord->l = newl;
			coord->angle = alpha;
		}
	}else{
		coord->stop = 1;
	}

	return coord;
}

MAP *principale(MAP *map){
	int h = map->hauteurmatrice;
	int l = map->largeurmatrice;

	double ale=-1;
	double angle=(10*M_PI)/180;  

	double cg = 0;
	
	COORD *coord = malloc(sizeof(COORD));
	coord->h=h-1;
	coord->l=l/2;
	coord->choix=2;
	coord->angle=0;

	coord = dg(map,coord,map->taille,0,coord->choix);	
	
	while(coord->stop != 1){
		cg = valeurAleatoire()*1;
		if(cg < 0.5){
			printf("\tchangement\n");
			ale = valeurAleatoire()*1;
			if(coord->angle == 0){
				coord->angle = angle;
			}
			if(ale >= 0.3){
				printf("droite\n");
				coord = dg(map,coord,map->taille,(coord->angle),0);		//droite 
			}else{
				printf("gauche\n");
				coord = dg(map,coord,map->taille,(coord->angle),1);	//gauche
			}
			
		}else{
			printf("\tpas Changement\n");
			coord = dg(map,coord,map->taille,0,coord->choix);	
		}
	}

	finitionmap(map);

	return map;
}

void movecar(MAP *map){
	int i = map->hauteurmatrice - 1;
	int j = map->largeurmatrice/2;
	int nb = 0;

	map->matrice_principale[i][j] = map->voiture;

	while(nb != 1){
		map->matrice_principale[i][j] = map->route;
		if(map->matrice_principale[i-1][j-1] == map->route){
			i--;
			j--;
		}else if(map->matrice_principale[i-1][j] == map->route){
			i--;
		}else if(map->matrice_principale[i-1][j+1] == map->route){
			i--;
			j++;
		}else{
			nb = 1;
		}

		if(nb == 0){
			map->matrice_principale[i][j] = map->voiture;
		}
	}
}