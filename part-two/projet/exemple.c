#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "ESLib.h" // Pour utiliser valeurAleatoire()
#include "structure.h"

// Largeur et hauteur par defaut d'une image correspondant a nos criteres
#define LargeurFenetre 1856
#define HauteurFenetre 600

#include "structure.h"


MAP *allocation(){
	int **matrice = NULL;
	int hauteur = 185, largeur= 21;

	MAP *map = malloc(sizeof(MAP));

	matrice = calloc(hauteur,sizeof(int*));
	for(int i=0;i<hauteur;i++){
		matrice[i] = calloc(largeur,sizeof(int));
	}

	map->matrice_principale = matrice;
	map->hauteurmatrice = hauteur;
	map->largeurmatrice = largeur;
	map->jardin = 0;
	map->route = 1;
	map->routebis = 2;
	map->routeD = 3;
	map->routeG = 4;
	map->voiture = 555;
	map->taille = 10;
	map->central = 6;

	return map;
}

void affichermatrice(int **matrice, int h, int l){
	
	printf("\t**********\n");
	for(int i=0;i<h;i++){
		for(int j=0;j<l;j++){
			printf("%d ",matrice[i][j]);
		}
		printf("\n");
	}
	printf("\t**********\n");
}

void finitionmap(MAP *map){
	printf("finitionmap\n");
	for(int i=0;i<map->hauteurmatrice;i++){
		for(int j=0;j<map->largeurmatrice;j++){
			if(map->matrice_principale[i][j] == 0){
				map->matrice_principale[i][j] = map->jardin;
			}
		}
	}
}

void tracage(MAP *map, int h, int l, int adj, int opp){
	printf("tracage\n");

	for(int i=h;i>h-adj;i--){
		if(i > adj/2){
			map->matrice_principale[i][l-2] = map->routeG;
			map->matrice_principale[i][l-1] = map->routebis;
			map->matrice_principale[i][l] = map->central;
			map->matrice_principale[i][l+1] = map->route;
			map->matrice_principale[i][l+2] = map->routeD;
		}else{
			map->matrice_principale[i][l+opp-2] = map->routeG;
			map->matrice_principale[i][l+opp-1] = map->routebis;
			map->matrice_principale[i][l+opp] = map->central;
			map->matrice_principale[i][l+opp+1] = map->route;
			map->matrice_principale[i][l+opp+2] = map->routeD;
		}
	}
}

COORD *dg(MAP *map, COORD *coord, int hyp, double alpha, int choix){
	int newh=0, newl=0;
	int adj=0, opp=0;

	adj = abs(cos(alpha)*hyp);

	if(adj == 10){
		//ligne
		opp = (sin(alpha)*hyp);
		coord->choix = 2;
	}else{
		if(choix == 0){
			//gauche
			opp = (sin(alpha)*hyp);
			coord->choix = 0;
		}else if(choix == 1){
			//droite
			opp = -(sin(alpha)*hyp);
			coord->choix = 1;
		}
	}
	newh = coord->h - adj;
	newl = coord->l - opp;

	if(newh > 0){
		if(newl > 1 && newl < map->largeurmatrice-2){
			tracage(map,coord->h,coord->l,adj,opp);
			coord->h = newh;
			coord->l = newl;
			coord->angle = alpha;
		}
	}else{
		coord->stop = 1;
	}

	return coord;
}

MAP *inicar(MAP *map, COORD *coord){

	int h = map->hauteurmatrice;
	int l = map->largeurmatrice;

	coord->h=h-1;
	coord->l=l/2+1;
	coord->stop=0;
	map->matrice_principale[coord->h][coord->l] = map->voiture;

	return map;
}

MAP *principale(MAP *map,COORD *coord){
	int h = map->hauteurmatrice;
	int l = map->largeurmatrice;
	printf("principale\n");

	double ale=-1;
	double angle=(10*M_PI)/180;  

	double cg = 0;
	
	coord->h=h-1;
	coord->l=l/2;
	coord->choix=2;
	coord->angle=0;

	coord = dg(map,coord,map->taille,0,coord->choix);	
	
	while(coord->stop != 1){
		cg = valeurAleatoire()*1;
		if(cg < 0.5){
			printf("\tchangement\n");
			ale = valeurAleatoire()*1;
			if(coord->angle == 0){
				coord->angle = angle;
			}
			if(ale >= 0.3){
				printf("droite\n");
				coord = dg(map,coord,map->taille,(coord->angle),0);	
			}else{
				printf("gauche\n");
				coord = dg(map,coord,map->taille,(coord->angle),1);	
			}
			
		}else{
			printf("\tpas Changement\n");
			coord = dg(map,coord,map->taille,0,coord->choix);	
		}
	}

	map = inicar(map,coord);

	finitionmap(map);
	return map;
}

COORD *movecar(MAP *map, COORD *coord){
	int i = coord->h;
	int j = coord->l;

	map->matrice_principale[i][j] = map->route;
	if(map->matrice_principale[i-1][j-1] == map->route){
		map->matrice_principale[i-1][j-1] = map->voiture;
		coord->h=i-1;
		coord->l=j-1;
	}else if(map->matrice_principale[i-1][j] == map->route){
		map->matrice_principale[i-1][j] = map->voiture;
		coord->h=i-1;
		coord->l=j;
	}else if(map->matrice_principale[i-1][j+1] == map->route){
		map->matrice_principale[i-1][j+1] = map->voiture;
		coord->h=i-1;
		coord->l=j+1;
	}else{
		coord->stop = 1;
	}

	return coord;
}

int main(int argc, char **argv)
{
	initialiseGfx(argc, argv);
	
	prepareFenetreGraphique("GfxLib", LargeurFenetre, HauteurFenetre);

	/* Lance la boucle qui aiguille les evenements sur la fonction gestionEvenement ci-apres,
		qui elle-meme utilise fonctionAffichage ci-dessous */
	lanceBoucleEvenements();
	
	return 0;
}

/* La fonction de gestion des evenements, appelee automatiquement par le systeme
des qu'une evenement survient */
void gestionEvenement(EvenementGfx evenement)
{
	static bool pleinEcran = false; // Pour savoir si on est en mode plein ecran ou pas
	static DonneesImageRGB *route = NULL; 
	static DonneesImageRGB *routeD = NULL; 
	static DonneesImageRGB *routeG = NULL; 
	static DonneesImageRGB *routeC = NULL;
	static DonneesImageRGB *herbe = NULL; 
	static DonneesImageRGB *titine = NULL; 

	static MAP *map = NULL;
	static COORD *coord = NULL;
	static int ok = 0;

	switch (evenement)
	{
		case Initialisation:
						
			/* Le message "Initialisation" est envoye une seule fois, au debut du
			programme : il permet de fixer "image" a la valeur qu'il devra conserver
			jusqu'a la fin du programme : soit "image" reste a NULL si l'image n'a
			pas pu etre lue, soit "image" pointera sur une structure contenant
			les caracteristiques de l'image "imageNB.bmp" */
			route = lisBMPRGB("route.bmp"); 
			routeD = lisBMPRGB("routeD.bmp"); 
			routeG = lisBMPRGB("routeG.bmp");
			routeC = lisBMPRGB("routeC.bmp");
			herbe = lisBMPRGB("herbe.bmp");
			titine = lisBMPRGB("voiture.bmp");

			//fonction
			map = malloc(sizeof(MAP));
			coord = malloc(sizeof(COORD));
			map = allocation();
			map = principale(map,coord);
			affichermatrice(map->matrice_principale,map->hauteurmatrice,map->largeurmatrice);

			// Configure le systeme pour generer un message Temporisation
			// toutes les 20 millisecondes
			demandeTemporisation(20);
			
			break;
		
		case Temporisation:
			
			rafraichisFenetre();
			break;
			
		case Affichage:
			// On part d'un fond d'ecran noir
			effaceFenetre(0,0,0);
			afficheChaine("Appuyez sur: R ou r", 50, largeurFenetre()/3, 4*hauteurFenetre()/5);

			if(coord->stop == 0){
				if(ok == 1){
					afficheChaine("Lancement ...", 50, largeurFenetre()/2.5, 3*hauteurFenetre()/5);
					coord = movecar(map,coord);
				}
				for(int i=map->hauteurmatrice-1;i>=0;i--){
					for(int j=0;j<map->largeurmatrice;j++){
						if(map->matrice_principale[i][j] == map->route || map->matrice_principale[i][j] == map->routebis){
							ecrisImage(i*10,j*10, route->largeurImage, route->hauteurImage, route->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->routeD){
							ecrisImage(i*10,j*10, routeD->largeurImage, routeD->hauteurImage, routeD->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->routeG){
							ecrisImage(i*10,j*10, routeG->largeurImage, routeG->hauteurImage, routeG->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->central){
							ecrisImage(i*10,j*10, routeC->largeurImage, routeC->hauteurImage, routeC->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->jardin){
							ecrisImage(i*10,j*10, herbe->largeurImage, herbe->hauteurImage, herbe->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->voiture){
							ecrisImage(i*10,j*10, titine->largeurImage, titine->hauteurImage, titine->donneesRGB);
						}
					}
				}
			}else{
				effaceFenetre(0,0,0);

				afficheChaine("FIN !!!", 50, largeurFenetre()/2, 3*hauteurFenetre()/5);

				for(int i=map->hauteurmatrice-1;i>=0;i--){
					for(int j=0;j<map->largeurmatrice;j++){
						if(map->matrice_principale[i][j] == map->route || map->matrice_principale[i][j] == map->routebis){
							ecrisImage(i*10,j*10, route->largeurImage, route->hauteurImage, route->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->routeD){
							ecrisImage(i*10,j*10, routeD->largeurImage, routeD->hauteurImage, routeD->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->routeG){
							ecrisImage(i*10,j*10, routeG->largeurImage, routeG->hauteurImage, routeG->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->central){
							ecrisImage(i*10,j*10, routeC->largeurImage, routeC->hauteurImage, routeC->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->jardin){
							ecrisImage(i*10,j*10, herbe->largeurImage, herbe->hauteurImage, herbe->donneesRGB);
						}
						if(map->matrice_principale[i][j] == map->voiture){
							ecrisImage(i*10,j*10, titine->largeurImage, titine->hauteurImage, titine->donneesRGB);
						}
					}
				}
			}
			break;
			
		case Clavier:
			printf("%c : ASCII %d\n", caractereClavier(), caractereClavier());

			switch (caractereClavier())
			{
				case 'Q': /* Pour sortir quelque peu proprement du programme */
				case 'q':
					/* On libere la structure image,
					c'est plus propre, meme si on va sortir du programme juste apres */
					termineBoucleEvenements();
					break;

				case 'F':
				case 'f':
					pleinEcran = !pleinEcran; // Changement de mode plein ecran
					if (pleinEcran)
						modePleinEcran();
					else
						redimensionneFenetre(LargeurFenetre, HauteurFenetre);
					break;

				case 'R':
				case 'r':
					// Configure le systeme pour generer un message Temporisation
					// toutes les 20 millisecondes (rapide)
					ok = 1;
					break;

				case 'L':
				case 'l':
					// Configure le systeme pour generer un message Temporisation
					// toutes les 100 millisecondes (lent)
					demandeTemporisation(100);
					break;

				case 'S':
				case 's':
					// Configure le systeme pour ne plus generer de message Temporisation
					demandeTemporisation(-1);
					break;
			}
			break;
			
		case ClavierSpecial:
			printf("ASCII %d\n", toucheClavier());
			break;

		case BoutonSouris:
			if (etatBoutonSouris() == GaucheAppuye){
				printf("Bouton gauche (largeur:%d, hateur:%d)\n\n", abscisseSouris(), ordonneeSouris());
			}
			else if (etatBoutonSouris() == GaucheRelache)
			{
				printf("Bouton gauche relache en : (%d, %d)\n", abscisseSouris(), ordonneeSouris());
			}
			break;
		
		case Souris: // Si la souris est deplacee
			break;
		
		case Inactivite: // Quand aucun message n'est disponible
			break;
		
		case Redimensionnement: // La taille de la fenetre a ete modifie ou on est passe en plein ecran
			
			printf("Largeur : %d\t", largeurFenetre());
			printf("Hauteur : %d\n", hauteurFenetre());
			break;
	}
}
