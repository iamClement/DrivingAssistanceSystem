#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../elements/line/cartesian/CartesianLine.h"
#include "../../elements/pixel/PixelCloud.h"
#include "../../elements/region/ColorRegion.h"
#include "direction.h"

#define WARNING "../../ressources/warning.bmp"
#define STRAIGHT "../../ressources/straight.bmp"
#define LEFT "../../ressources/left.bmp"
#define RIGHT "../../ressources/right.bmp"

void turn_detection(char* inputfile, char* outputfile);

Direction getDirectionFromImage(ImageRGB* image);

void display_direction(ImageRGB* image, Direction direction);

int main(int argc, char* argv[])
{
	//if(argc != 2)
	//{
	//	printf("Wrong number of argument\n");
	//	return 1;
	//}

	//turn_detection(argv[1]);

	char* in = "../../image-files/virages-input/virages414.bmp";
	char* out = "virages414.bmp";

	char* input = malloc(strlen(in));
	char* output = malloc(strlen(out));

	strcpy(input,in);
	strcpy(output,out);

	//39-40-41
	while((input[39] != '7')||(input[40] != '9')||(input[41] != '1'))
	{
		printf("img n°%c%c%c\n",input[39],input[40],input[41]);
		turn_detection(input,output);

		if(input[41] == '9')
		{
			input[41] = '0';
			output[42] = '0';

			if(input[40] == '9')
			{
				input[40] = '0';
				output[41] = '0';

				if(input[39] == '9')
				{
					input[39] = '0';
					output[40] = '0';
				}
				else
				{
					input[39]+=1;
					output[40]+=1;
				}
			}
			else
			{
				input[40]+=1;
				output[41]+=1;
			}
		}
		else
		{
			input[41] = input[41]+1;
			output[42] = output[42]+1;
		}
	}


	return 0;
}

void turn_detection(char* inputfile, char* outputfile)
{
	ImageRGB* image = readBMPFile(inputfile);

	Direction direction = getDirectionFromImage(image);

	printDirection(direction);

	display_direction(image,direction);

	writeBMPFile(*image,outputfile);

	freeRGBImage(&image);
}

Direction getDirectionFromImage(ImageRGB* image)
{
	//Growing region
	int regionNumber = 0;
	ColorRegion** regions = growRegionFromImage(*image,&regionNumber,20,20);

	//Getting the road region index
	int roadIndex = getLargestRegionIndex(regions,regionNumber);

	//Getting the top pixel of the road region
	Pixel* topRoadRegion = getColorRegionTopPixel(*regions[roadIndex]);

	int halfEdge = topRoadRegion->ordinate / 2;
	int threeQuarterEdge = halfEdge + (halfEdge / 2);
	int topEdge = threeQuarterEdge + (halfEdge / 4);

	PixelCloud* downHalf = NULL;
	PixelCloud* upHalf = NULL;
	PixelCloud* downTopHalf = NULL;
	PixelCloud* upTopHalf = NULL;

	int pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < regions[roadIndex]->size ; pixelIndex+=1)
	{
		if(regions[roadIndex]->pixels[pixelIndex]->ordinate > topEdge)
		{
			if(upTopHalf)
			{
				addPixelInPixelCloud(upTopHalf,*regions[roadIndex]->pixels[pixelIndex]);
			}
			else
			{
				upTopHalf = newPixelCloud(*regions[roadIndex]->pixels[pixelIndex]);
			}
		}
		else
		{
			if(regions[roadIndex]->pixels[pixelIndex]->ordinate > threeQuarterEdge)
			{
				if(downTopHalf)
				{
					addPixelInPixelCloud(downTopHalf,*regions[roadIndex]->pixels[pixelIndex]);
				}
				else
				{
					downTopHalf = newPixelCloud(*regions[roadIndex]->pixels[pixelIndex]);
				}
			}
			else
			{
				if(regions[roadIndex]->pixels[pixelIndex]->ordinate > halfEdge)
				{
					if(upHalf)
					{
						addPixelInPixelCloud(upHalf,*regions[roadIndex]->pixels[pixelIndex]);
					}
					else
					{
						upHalf = newPixelCloud(*regions[roadIndex]->pixels[pixelIndex]);
					}
				}
				else
				{
					if(downHalf)
					{
						addPixelInPixelCloud(downHalf,*regions[roadIndex]->pixels[pixelIndex]);
					}
					else
					{
						downHalf = newPixelCloud(*regions[roadIndex]->pixels[pixelIndex]);
					}
				}
			}
		}
	}

	Pixel* upBary = getPixelCloudBarycentre(*upTopHalf);
	Pixel* middleUpBary = getPixelCloudBarycentre(*downTopHalf);
	Pixel* middleDownBary = getPixelCloudBarycentre(*upHalf);
	Pixel* downBary = getPixelCloudBarycentre(*downHalf);

	ColorRGB* virageColor = newColorRGB(0,255,0);

	setPixelColorOfImageRGB(image,*virageColor,*upBary);
	setPixelColorOfImageRGB(image,*virageColor,*middleUpBary);
	setPixelColorOfImageRGB(image,*virageColor,*middleDownBary);
	setPixelColorOfImageRGB(image,*virageColor,*downBary);

	Direction direction = getNextDirection(*upBary,*middleUpBary,*middleDownBary,*downBary);

	//Liberation of the memory

	freePixel(&upBary);
	freePixel(&middleUpBary);
	freePixel(&middleDownBary);
	freePixel(&downBary);

	freePixelCloud(&upTopHalf);
	freePixelCloud(&downTopHalf);
	freePixelCloud(&upHalf);
	freePixelCloud(&downHalf);

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < regionNumber ; regionIndex+=1)
	{
		freeColorRegion(&regions[regionIndex]);
	}

	free(regions);

	return direction;
}

void display_direction(ImageRGB* image, Direction direction)
{
	char* filename;

	switch(direction)
	{
	case UNKNOWN_DIR:
		filename = WARNING;
		break;
	case STRAIGHT_DIR:
		filename = STRAIGHT;
		break;
	case LEFT_DIR:
		filename = LEFT;
		break;
	case RIGHT_DIR:
		filename = RIGHT;
		break;
	}

	ImageRGB* icon = readBMPFile(filename);

	int rowIndex, columnIndex;

	ColorRGB* black = newColorRGB(0,0,0);

	for(rowIndex = 10 ; rowIndex < 10+icon->height ; rowIndex+=1)
	{
		for(columnIndex = 10 ; columnIndex < 10+icon->width ; columnIndex+=1)
		{
			Pixel* currentPixel = newPixel(columnIndex-10,rowIndex-10);
			Pixel* curPixel = newPixel(columnIndex,rowIndex);
			ColorRGB* currentColor = getPixelColorOfImageRGB(*icon,*currentPixel);

			if(getDistanceBetweenColor(*currentColor,*black) != 0)
			{
				setPixelColorOfImageRGB(image,*currentColor,*curPixel);
			}

			freeColorRGB(&currentColor);
			freePixel(&curPixel);
			freePixel(&currentPixel);
		}
	}
}

