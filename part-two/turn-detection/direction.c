/*
 * @title direction.c
 * @date 8 avr. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "direction.h"

void printDirection(Direction direction)
{
	switch(direction)
	{
	case UNKNOWN_DIR:
		printf("UNKNOWN DIRECTION\n");
		break;
	case STRAIGHT_DIR:
		printf("STRAIGHT ON\n");
		break;
	case LEFT_DIR:
		printf("LEFT TURN\n");
		break;
	case RIGHT_DIR:
		printf("RIGHT TURN\n");
		break;
	}
}

Direction getNextDirection(Pixel top, Pixel middleTop, Pixel middleDown, Pixel down)
{
	Direction direction = UNKNOWN_DIR;

	int maxAbscissa = getMaxAbscissa(top,middleTop,middleDown,down);
	int minAbscissa = getMinAbscissa(top,middleTop,middleDown,down);
	int width = maxAbscissa - minAbscissa;

	if(width < 25)
	{
		direction = STRAIGHT_DIR;
	}
	else
	{
		int topDownDiff = getAbscissaDiff(top,down);

		if((topDownDiff > -12)&&(topDownDiff < 12))
		{
			int topMiddleTopDiff = getAbscissaDiff(top,middleTop);

			if((topMiddleTopDiff > -12)&&(topMiddleTopDiff < 12))
			{
				direction = STRAIGHT_DIR;
			}
			else
			{
				int downMiddleDownDiff = getAbscissaDiff(middleDown,down);

				if((downMiddleDownDiff > -12)&&(downMiddleDownDiff < 12))
				{
					direction = STRAIGHT_DIR;
				}
				else
				{
					int middleDiff = getAbscissaDiff(middleTop,middleDown);

					if((middleDiff > -12)&&(middleDiff < 12))
					{
						direction = STRAIGHT_DIR;
					}
				}
			}
		}
		else
		{
			if(topDownDiff > 0)
			{
				direction = RIGHT_DIR;
			}
			else
			{
				direction = LEFT_DIR;
			}
		}
	}

	return direction;
}

int getAbscissaDiff(Pixel pixelOne, Pixel pixelTwo)
{
	return (pixelOne.abscissa - pixelTwo.abscissa);
}

int getMaxAbscissa(Pixel top, Pixel middleTop, Pixel middleDown, Pixel down)
{
	return max_int(max_int(top.abscissa,middleTop.abscissa),max_int(middleDown.abscissa,down.abscissa));
}

int getMinAbscissa(Pixel top, Pixel middleTop, Pixel middleDown, Pixel down)
{
	return min_int(min_int(top.abscissa,middleTop.abscissa),min_int(middleDown.abscissa,down.abscissa));
}

int max_int(int a, int b)
{
	return a>b?a:b;
}

int min_int(int a, int b)
{
	return a>b?b:a;
}
