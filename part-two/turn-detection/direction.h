/*
 * @title direction.h
 * @date 8 avr. 2019
 * @docauthor luca
 * @make
 */

#ifndef DIRECTION_H_
#define DIRECTION_H_

#include "../../elements/pixel/Pixel.h"

typedef enum Direction {UNKNOWN_DIR, STRAIGHT_DIR, LEFT_DIR, RIGHT_DIR}Direction;

void printDirection(Direction direction);

Direction getNextDirection(Pixel top, Pixel middleTop, Pixel middleDown, Pixel down);

int getAbscissaDiff(Pixel pixelOne, Pixel pixelTwo);

int getMaxAbscissa(Pixel top, Pixel middleTop, Pixel middleDown, Pixel down);

int getMinAbscissa(Pixel top, Pixel middleTop, Pixel middleDown, Pixel down);

int max_int(int a, int b);

int min_int(int a, int b);

#endif /* DIRECTION_H_ */
