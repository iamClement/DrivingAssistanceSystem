# Driving Assistance System (Système d'Aide à la Conduite)

Le but du 'Driving Assistance System' (DAS, ou SAC en français) est de créer une version simplifié d'un système d'aide à la conduite.

## Sommaire

1. [Informations utiles](#info)
    1. [Classes conseillées](#classes)
    2. [Concepts requis](#concepts)
2. [À propos](#apropos)
    1. [Comment ça marche ?](#comment)
    2. [Pour qui est-ce que cela a été conçu ?](#qui)
3. [Utilisation dans vos projets](#utilisation)
    1. [Pré-requis](#prerequis)
    2. [Installation](#installation)
    3. [Utilisation](#utilisation)
4. [Crédits](#credits)
5. [Alternatives](#alternatives)
6. [Mainteneurs](#mainteneurs)

## Informations utiles <a name=info></a>

### Classes conseillées <a name=classes></a>

+ Année minimum : M1
+ Domaine Professionnel : Développement Logiciel Bio Inspiré
+ Cycle fait : Tous

### Concepts requis <a name=concepts></a>

+ Traitement d'image
+ Vision par ordinateur
+ Machine learning

## À propos du SAC <a name=apropos></a>

### Comment ça marche ? <a name=comment></a>

Le programme utilise different algorithmes de traitement d'image et d'analyse d'image.

### Pour qui est-ce que cela a été conçu ? <a name=qui></a>

Le SAC a été concus pour ceux qui veulent se reposer au volant, ceux qui en on marre de conduire ou encore ceux qui ne savent pas conduire.

## Utilisation du SAC dans vos projets <a name=utilisation></a>

### Pré-requis <a name=prerequis></a>

Pas de prérequis pour le moment.

### Installation <a name=installation></a>

Il n'y a pas encore de version stable du projet.

### Utilisation <a name=utilisation></a>

Le manuel est en cours d'ecriture.

## Crédits <a name=credits></a>

+ libisentlib, librairie de l'ISEN Toulon écrite par Ghislain Oudinet
+ matrixlib, librairie de gestion de matrice.

## Alternatives <a name=alternatives></a>

+ [Tesla Autopilot](https://www.tesla.com/fr_FR/autopilot)

## Mainteneurs <a name=mainteneurs></a>

[@iamClement](https://framagit.org/iamClement)  
[@LucaMayerDalverny](https://framagit.org/LucaMayerDalverny)  
[@AnneS](https://framagit.org/AnneS)