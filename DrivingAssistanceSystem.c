/*
 * @title DrivingAssistanceSystem.c
 * @date 27 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[])
{
	if(argc > 1)
	{
		int part = atoi(argv[1]);

		if((part == 1)&&(argc != 3))
		{
			printf("A minimum of two arguments is required to run the first part of the project. End of the program\n");
			exit(0);
		}

		switch(part)
		{
		case 1:

			printf("Part-one selected\n");

			int size = strlen(argv[2]);

			printf("\nkeep-on-track : \n");

			int keepOnTrackSize = size + 37;
			char* commandKeepOnTrack = (char*) malloc(keepOnTrackSize*sizeof(char));
			strcpy(commandKeepOnTrack,"part-one/keep-on-track/keep-on-track ");
			strcat(commandKeepOnTrack,argv[2]);

			system(commandKeepOnTrack);

			printf("\npanel-detection : \n");

			int panelDetectionSize = size + 46;
			char* commandPanelDetection = (char*) malloc(panelDetectionSize*sizeof(char));
			strcpy(commandPanelDetection,"part-one/circular-panel-detection/panelDetect ");
			strcat(commandPanelDetection,argv[2]);

			system(commandPanelDetection);

			printf("\ntraffic-light-detection : \n");

			break;
		case 2:

			printf("Part-two selected\n");

			char* roadCommand = (char*) malloc(23*sizeof(char));
			strcpy(roadCommand,"part-two/projet/exemple");

			system(roadCommand);
			break;
		default:
			break;
		}
	}
	else
	{
		printf("A minimum of one argument is required. End of the program\n");
		exit(0);
	}

	return 0;
}
