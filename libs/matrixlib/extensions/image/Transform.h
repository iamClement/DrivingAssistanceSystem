/*
 * @title Transform.h
 * @date 19 nov. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef MATRIXLIB_EXTENSIONS_IMAGE_TRANSFORM_H_
#define MATRIXLIB_EXTENSIONS_IMAGE_TRANSFORM_H_

#include "../../Matrix.h"

/**
 * @fn houghTransform
 * @desc Apply the Hough transform to a given Matrix structure. The result will be stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the transform will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* houghTransform(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn radonTransform
 * @desc Apply the Hough transform to a given Matrix structure. The result will be stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the transform will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* radonTransform(Matrix matrix);
/**
 * @ecode
 */

#endif /* MATRIXLIB_EXTENSIONS_IMAGE_TRANSFORM_H_ */
