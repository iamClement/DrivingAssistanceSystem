/*
 * @title Filter.h
 * @date 30 oct. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef FILTER_H_
#define FILTER_H_

#include "../../Matrix.h"
#include "../math/FunctionalOperations.h"

typedef int Filter3x3[3][3];

/**
 * @struct Filter
 * @desc A structure representing a filter that can be applied to an Image
 * @field filterMatrix (Matrix*) : the matrix containing the values of the filter
 * @field divider (unsigned int) : the number by which the sum of the image values multiplied by the corresponding weights of the filter is divided
 * @field currentPixelColumn (unsigned int) : the column where the current pixel to modify is in the filter
 * @field currentPixelRow (unsigned int) : the row where the current pixel to modify is in the filter
 */
typedef struct Filter
{
	Matrix* filterMatrix;
	unsigned int divider;
	unsigned int currentPixelColumn;
	unsigned int currentPixelRow;
}Filter;

/**
 * @fn newFilter
 * @desc Allocate the memory for a filter structure
 * @param columnsNumber (unsigned int) : the number of column of the filter
 * @param rowNumber (unsigned int) : the number of row of the filter
 * @param divider (unsigned int) : the number by which the sum of the image values multiplied by the corresponding weights of the filter is divided
 * @param currentPixelColumn (unsigned int) : the column where the current pixel to modify is in the filter
 * @param currentPixelRow (unsigned int) : the row where the current pixel to modify is in the filter
 * @return (Filter*) : the filter allocated
 * @bcode
 */
Filter* newFilter(unsigned int columnsNumber, unsigned int rowNumber, unsigned int divider, unsigned int currentPixelColumn, unsigned int currentPixelRow);
/**
 * @ecode
 */

/**
 * @fn newRobertsXFilter
 * @desc Allocate and fill a filter with the values of the X Roberts filter
 * @return (Filter*) : the X Roberts filter
 * @bcode
 */
Filter* newRobertsXFilter(void);
/**
 * @ecode
 */

/**
 * @fn newRobertsYFilter
 * @desc Allocate and fill a filter with the values of the Y Roberts filter
 * @return (Filter*) : the Y Roberts filter
 * @bcode
 */
Filter* newRobertsYFilter(void);
/**
 * @ecode
 */

/**
 * @fn newSobelXFilter
 * @desc Allocate and fill a filter with the values of the X Sobel filter
 * @return (Filter*) : the X Sobel filter
 * @bcode
 */
Filter* newSobelXFilter(void);
/**
 * @ecode
 */

/**
 * @fn newSobelYFilter
 * @desc Allocate and fill a filter with the values of the Y Sobel filter
 * @return (Filter*) : the Y Sobel filter
 * @bcode
 */
Filter* newSobelYFilter(void);
/**
 * @ecode
 */

/**
 * @fn newPrewittXFilter
 * @desc Allocate and fill a filter with the values of the X Prewitt filter
 * @return (Filter*) : the X Prewitt filter
 * @bcode
 */
Filter* newPrewittXFilter(void);
/**
 * @ecode
 */

/**
 * @fn newPrewittYFilter
 * @desc Allocate and fill a filter with the values of the Y Prewitt filter
 * @return (Filter*) : the Y Prewitt filter
 * @bcode
 */
Filter* newPrewittYFilter(void);
/**
 * @ecode
 */

/**
 * @fn applyFilterToMatrix
 * @desc Apply a filter of type 'Filter' to a matrix. The result is stored in a new Matrix structure.
 * @param matrix (Matrix*) : the matrix on which the filter will be applied
 * @param filter (Filter) : the filter to apply on the Matrix
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyFilterToMatrix(Matrix matrix, Filter filter);
/**
 * @ecode
 */

/**
 * @fn applyMedianFilterToMatrix
 * @desc Apply the median filter to a matrix. The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyMedianFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn applyRobertsFilterToMatrix
 * @desc Apply the Prewitt filter to a given Matrix. The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyRobertsFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn applySobelFilterToMatrix
 * @desc Apply the Sobel filter to a given Matrix. The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applySobelFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn applyPrewittFilterToMatrix
 * @desc Apply the Prewitt filter to a given Matrix. The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyPrewittFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn freeFilter
 * @desc Free the memory space used by the filter
 * @param filter (Filter**) : the filter to be freed
 * @bcode
 */
void freeFilter(Filter** filter);
/**
 * @ecode
 */

#endif /* FILTER_H_ */
