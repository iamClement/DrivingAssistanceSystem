/*
 * @title File.h
 * @date 26 nov. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef MATRIXLIB_EXTENSIONS_IMAGE_FILE_H_
#define MATRIXLIB_EXTENSIONS_IMAGE_FILE_H_


#include "../../Matrix.h"
#include "../../../libisentlib/BmpLib.h"

/**
 * @chap Introduction
 */

/**
 * The goal of the functions in this file is to fill Matrix structures with the data of an 24 bits bmp image file. All these functions require the bmplib writen by Mr Ghislain Oudinet.
 */

/**
 * @chap Grey Images functions
 */

/**
 * @fn newGreyMatrixFromImage
 * @desc Create a new matrix structure with the data of a 24 bits BMP image.
 * @param fileName (char*) : the name of the image
 * @return (Matrix*) : the matrix created from the image file
 * @bcode
 */
Matrix* newGreyMatrixFromImage(char* fileName);
/**
 * @ecode
 */

/**
 * @fn newGreyImageFromMatrix
 * @desc Create a 24 bits BMP image file (in grey levels) from a Matrix structure.
 * @param matrix (Matrix*) : the matrix used to create the image
 * @param fileName (char*) : the name of the file to create
 * @bcode
 */
void newGreyImageFromMatrix(Matrix matrix, char* fileName);
/**
 * @ecode
 */

/**
 * @chap Color images functions
 */

/**
 * @fn newMatrixFromRGBImageData
 * @desc Fill three Matrix structures with the red, green and blue values of a given 24 bits BMP image file.
 * @param redMatrix (Matrix**) : the Matrix receiving the red value of the image
 * @param greenMatrix (Matrix**) : the Matrix receiving the green values of the image
 * @param blueMatrix (Matrix**) : the Matrix receiving the blue values of the image
 * @param fileName (char*) : the name of the image to use
 * @bcode
 */
void newMatrixFromRGBImageData(Matrix** redMatrix, Matrix** greenMatrix, Matrix** blueMatrix, char* fileName);
/**
 * @ecode
 */

/**
 * @fn createRGBImageFromMatrix
 * @desc Create a 24 bits BMP image file with the data of three Matrix structures containing the red, green and blue values of the image.
 * @param redMatrix (Matrix) : the Matrix containing the red value of the image
 * @param greenMatrix (Matrix) : the Matrix containing the green values of the image
 * @param blueMatrix (Matrix) : the Matrix containing the blue values of the image
 * @param fileName (char*) : the name of the image to create
 * @bcode
 */
void createRGBImageFromMatrix(Matrix redMatrix, Matrix greenMatrix, Matrix blueMatrix, char* fileName);

#endif /* MATRIXLIB_EXTENSIONS_IMAGE_FILE_H_ */
