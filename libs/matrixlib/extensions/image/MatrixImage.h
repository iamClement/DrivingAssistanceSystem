/*
 * @title MatrixImage.h
 * @date 7 nov. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef _MATRIXIMAGE_H_
#define _MATRIXIMAGE_H_

#include "../../Matrix.h"
#include "../../../libisentlib/BmpLib.h"
#include "Filter.h"

/**
 * @fn newGreyMatrixFromImage
 * @desc Create a new matrix structure with the data of a 24 bits BMP image.
 * @param fileName (char*) : the name of the image
 * @return (Matrix*) : the matrix created from the image file
 * @bcode
 */
Matrix* newGreyMatrixFromImage(char* fileName);
/**
 * @ecode
 */

/**
 * @fn applyFilterToMatrix
 * @desc Apply a 3x3 pixel filter to a matrix. The result is stored in a new Matrix structure.
 * @param matrix (Matrix*) : the matrix on which the filter will be applied
 * @param filter (Filter3x3) : the filter to apply on the Matrix
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyFilterToMatrix(Matrix matrix, Filter3x3 filter);
/**
 * @ecode
 */

/**
 * @fn applyMedianFilterToMatrix
 * @desc Apply the median filter to a matrix. The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyMedianFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn applySobelFilterToMatrix
 * @desc Apply the Sobel filter to a given Matrix.The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applySobelFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn applyPrewittFilterToMatrix
 * @desc Apply the Prewitt filter to a given Matrix.The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyPrewittFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn applyRobertsFilterToMatrix
 * @desc Apply the Prewitt filter to a given Matrix.The result is stored in a new Matrix structure.
 * @param matrix (Matrix) : the matrix on which the filter will be applied
 * @return (Matrix*) : the matrix containing the result of the process
 * @bcode
 */
Matrix* applyRobertsFilterToMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn mergeTwoMatrixWithGradient
 * @desc Merge two matrices using the following gradient formula : g(x,y) = sqrt(x^2 + y^2). The result is stored in a new Matrix structure.
 * @param matrix_x (Matrix) : The first matrix to merge
 * @param matrix_y (Matrix) : The second matrix to merge
 * @return (Matrix*) : The fusion of the two matrices
 * @bcode
 */
Matrix* mergeTwoMatrixWithGradient(Matrix matrix_x, Matrix matrix_y);
/**
 * @ecode
 */

/**
 * @fn newGreyImageFromMatrix
 * @desc Create a 24 bits BMP image file (in grey levels) from a Matrix structure
 * @param matrix (Matrix*) : the matrix used to create the image
 * @param fileName (char*) : the name of the file to create
 * @bcode
 */
void newGreyImageFromMatrix(Matrix matrix, char* fileName);
/**
 * @ecode
 */

#endif /* _MATRIXIMAGE_H_ */
