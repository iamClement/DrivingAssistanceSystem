/*
 * @title ElementaryOperations.h
 * @date 13 nov. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef MATRIXLIB_EXTENSIONS_MATH_ELEMENTARYOPERATIONS_H_
#define MATRIXLIB_EXTENSIONS_MATH_ELEMENTARYOPERATIONS_H_

#include "../../Matrix.h"

/**
 * @chap Introduction
 */

/**
 * @chap Functions returning Matrix structures
 */

/**
 * @fn newMatrixFromTwoMatrixAddition
 * @desc
 * @param
 * @param
 * @return
 * @bcode
 */
Matrix* newMatrixFromTwoMatrixAddition(Matrix matrixOne, Matrix matrixTwo);
/**
 * @ecode
 */

/**
 * @fn newMatrixFromTwoMatrixSubtraction
 * @desc
 * @param
 * @param
 * @return
 * @bcode
 */
Matrix* newMatrixFromTwoMatrixSubtraction(Matrix matrixOne, Matrix matrixTwo);
/**
 * @ecode
 */

/**
 * @fn newMatrixFromTwoMatrixMultiplication
 * @desc
 * @param
 * @param
 * @return
 * @bcode
 */
Matrix* newMatrixFromTwoMatrixMultiplication(Matrix matrixOne, Matrix matrixTwo);
/**
 * @ecode
 */

/**
 * @chap Function returning nothing
 */

/**
 * @fn multiplyCoefficientsMatrixToMatrix
 * @desc
 * @param
 * @param
 * @bcode
 */
void multiplyCoefficientsMatrixToMatrix(Matrix* matrix, Matrix coefficientsMatrix);
/**
 * @ecode
 */

#endif /* MATRIXLIB_EXTENSIONS_MATH_ELEMENTARYOPERATIONS_H_ */
