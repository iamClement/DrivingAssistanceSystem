/*
 * @title TotalOrderOperations.h
 * @date 13 nov. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef MATRIXLIB_EXTENSIONS_MATH_TOTALORDEROPERATIONS_H_
#define MATRIXLIB_EXTENSIONS_MATH_TOTALORDEROPERATIONS_H_

#include "../../Matrix.h"

/**
 * @fn getMatrixMaxima
 * @desc Give the maxima value of a given value
 * @param matrix (Matrix) : the matrix for which we want the maxima value
 * @return (short int) : the maxima value of the matrix
 * @bcode
 */
short int getMatrixMaxima(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn getMatrixMinima
 * @desc Give the minima value of a given matrix
 * @param matrix (Matrix) : the matrix for which we want the minima
 * @return (short int) : the minima value of the matrix
 * @bcode
 */
short int getMatrixMinima(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn sortMatrix
 * @desc Sort a given Matrix structure. The sorted value are stored in a new one dimension Matrix structure
 * @param matrix (Matrix) : the matrix to be sorted
 * @return (Matrix*) : the matrix containing the result of the process
 */
Matrix* sortMatrix(Matrix matrix);

#endif /* MATRIXLIB_EXTENSIONS_MATH_TOTALORDEROPERATIONS_H_ */
