/*
 * @title FunctionalOperations.h
 * @date 26 nov. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef MATRIXLIB_EXTENSIONS_MATH_FUNCTIONALOPERATIONS_H_
#define MATRIXLIB_EXTENSIONS_MATH_FUNCTIONALOPERATIONS_H_

#include "../../Matrix.h"

/**
 * @chap Introduction
 */

/**
 * The goal the functions contained in this file is to modify Matrix structures by applying on them functions such as the norm ( f(x,y) = sqrt(x^2 + y^2)).
 */

/**
 * @chap Functions in the file
 */

/**
 * @fn mergeTwoMatrixWithNorm
 * @desc Merge two matrices using the following formula : g(x,y) = sqrt(x^2 + y^2). The result is stored in a new Matrix structure.
 * @param matrix_x (Matrix) : The first matrix to merge
 * @param matrix_y (Matrix) : The second matrix to merge
 * @return (Matrix*) : The fusion of the two matrices
 * @bcode
 */
Matrix* mergeTwoMatrixWithNorm(Matrix matrix_x, Matrix matrix_y);
/**
 * @ecode
 */

#endif /* MATRIXLIB_EXTENSIONS_MATH_FUNCTIONALOPERATIONS_H_ */
