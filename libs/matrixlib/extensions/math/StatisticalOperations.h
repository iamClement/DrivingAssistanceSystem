/*
 * @title StatisticalOperations.h
 * @date 13 nov. 2018
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef MATRIXLIB_EXTENSIONS_MATH_STATISTICALOPERATIONS_H_
#define MATRIXLIB_EXTENSIONS_MATH_STATISTICALOPERATIONS_H_

#include "../../Matrix.h"

/**
 * @fn getMatrixMedian
 * @desc Give the median value of a given value
 * @param matrix (Matrix) : the matrix for which we want the median value
 * @return (short int) : the median value of the matrix
 * @bcode
 */
short int getMatrixMedian(Matrix matrix);
/**
 * @ecode
 */

#endif /* MATRIXLIB_EXTENSIONS_MATH_STATISTICALOPERATIONS_H_ */
