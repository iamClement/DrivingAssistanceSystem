/**
 * @title Matrix.h
 * @docauthor Luca Mayer-Dalverny
 * @date 14 sept. 2018
 * @make
 */

#ifndef MATRIX_H_
#define MATRIX_H_

/**
 * @struct Matrix
 * @desc A structure that represent a matrix
 * @field columnsNumber (unsigned int) : the number of columns of the matrix
 * @field rowsNumber (unsigned int) : the number of rows of the matrix
 * @field data (short int **) : the data of the matrix
 */
typedef struct Matrix
{
	unsigned int columnsNumber;
	unsigned int rowsNumber;
	short int** data;
}Matrix;

/**
 * @fn newMatrix
 * @desc Allocate the memory for the matrix
 * @param columnsNumber (unsigned int) : the number of columns of the matrix
 * @param rowsNumber (unsigned int) : the number of rows of the matrix
 * @return Matrix* : the matrix allocated
 * @bcode
 */
Matrix* newMatrix(unsigned int columnsNumber, unsigned int rowsNumber);
/**
 * @ecode
 */

/**
 * @fn fillMatrix
 * @desc Fill a matrix with 0
 * @param matrix (Matrix*) : the matrix to fill
 * @bcode
 */
void fillMatrix(Matrix* matrix);
/**
 * @ecode
 */

/**
 * @fn addValueToMatrixAt
 * @desc Add a value to given coordinates in the matrix
 * @param matrix (Matrix*) : The matrix to be modified
 * @param value (short int) : the value to add
 * @param column (unsigned int) : The column of the data to be modified
 * @param row (unsigned int) : The line of the data to be modified
 * @bcode
 */
void addValueToMatrixAt(Matrix* matrix, short int value, unsigned int column, unsigned int row);
/**
 * @ecode
 */

/**
 * @fn getValueFromMAtrixAt
 * @desc Give the value at coordinates of a matrix
 * @param matrix (Matrix) : The matrix to get the value
 * @param column (unsigned int) : The column of the value to get
 * @param row (unsigned int) : The line of the value to be get
 * @return (short int) : The value wanted
 * @bcode
 */
short int getValueOfMatrixAt(Matrix matrix, int column, int row);
/**
 * @ecode
 */

/**
 * @fn displayMatrix
 * @desc Display the matrix in the command prompt
 * @param matrix (Matrix) : the matrix to display
 * @bcode
 */
void displayMatrix(Matrix matrix);
/**
 * @ecode
 */

/**
 * @fn freeMatrix
 * @desc Free the memory space used by the matrix
 * @param matrix (Matrix**) : the matrix to be freed
 * @bcode
 */
void freeMatrix(Matrix** matrix);
/**
 * @ecode
 */

#endif /* MATRIX_H_ */
