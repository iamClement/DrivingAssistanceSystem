/*
 * @title Pixel.c
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Pixel.h"

Pixel* newPixel(unsigned int abscissa, unsigned int ordinate)
{
	//Allocation of the memory for the structure
	Pixel* pixel = (Pixel*) malloc(sizeof(Pixel));

	//Filling the fields of the structure
	pixel->abscissa = abscissa;
	pixel->ordinate = ordinate;

	return pixel;
}

void freePixel(Pixel** pixel)
{
	if(pixel != NULL)
	{
		if(*pixel != NULL)
		{
			Pixel* interimPixel = *pixel;

			free(interimPixel);

			*pixel = NULL;
		}
	}
}

bool isPixelEqualTo(Pixel pixel, Pixel pixelToCompare)
{
	bool postulate = false;

	if((pixel.abscissa == pixelToCompare.abscissa)&&(pixel.ordinate == pixelToCompare.ordinate))
	{
		postulate = true;
	}

	return postulate;
}

int distanceBetween(Pixel * pixel1, Pixel * pixel2)
{
	int x2 = (pixel1->abscissa-pixel2->abscissa)*(pixel1->abscissa-pixel2->abscissa);
	int y2 = (pixel1->ordinate-pixel2->ordinate)*(pixel1->ordinate-pixel2->ordinate);

	return ((int)sqrt(x2+y2));
}
