/*
 * @title PixelCloud.c
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "PixelCloud.h"

PixelCloud* newPixelCloud(Pixel pixel)
{
	PixelCloud* pixelCloud = (PixelCloud*) malloc(sizeof(PixelCloud));

	pixelCloud->size = 1;

	pixelCloud->pixels = (Pixel**) malloc(sizeof(Pixel*));

	pixelCloud->pixels[0] = newPixel(pixel.abscissa,pixel.ordinate);

	return pixelCloud;
}

void freePixelCloud(PixelCloud** pixelCloud)
{
	if(pixelCloud != NULL)
	{
		if(*pixelCloud != NULL)
		{
			PixelCloud* interimPixelCloud = *pixelCloud;

			int pixelIndex;

			for(pixelIndex = 0 ; pixelIndex < interimPixelCloud->size ; pixelIndex+=1)
			{
				freePixel(&interimPixelCloud->pixels[pixelIndex]);
			}

			free(interimPixelCloud->pixels);
			free(interimPixelCloud);

			*pixelCloud = NULL;
		}
	}

}

void addPixelInPixelCloud(PixelCloud* pixelCloud,Pixel pixel)
{
	pixelCloud->size+=1;

	pixelCloud->pixels = (Pixel**) realloc(pixelCloud->pixels,pixelCloud->size * sizeof(Pixel*));

	pixelCloud->pixels[pixelCloud->size-1] = newPixel(pixel.abscissa,pixel.ordinate);
}

bool isPixelInPixelCloud(PixelCloud pixelCloud, Pixel pixel)
{
	bool postulate = false;

	int pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < pixelCloud.size ; pixelIndex+=1)
	{
		if(isPixelEqualTo(pixel,*pixelCloud.pixels[pixelIndex]))
		{
			postulate = true;
		}
	}

	return postulate;
}

void drawPixelCloudInImageRGB(PixelCloud pixelCloud, ImageRGB* imageRGB, ColorRGB colorRGB)
{
	int pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < pixelCloud.size ; pixelIndex+=1)
	{
		setPixelColorOfImageRGB(imageRGB,colorRGB,*pixelCloud.pixels[pixelIndex]);
	}
}

Pixel* getPixelCloudBarycentre(PixelCloud pixelCloud)
{
	unsigned int abscissa = 0;
	unsigned int ordinate = 0;

	int pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < pixelCloud.size ; pixelIndex+=1)
	{
		abscissa+=pixelCloud.pixels[pixelIndex]->abscissa;
		ordinate+=pixelCloud.pixels[pixelIndex]->ordinate;
	}

	abscissa = (unsigned int) abscissa / pixelCloud.size;
	ordinate = (unsigned int) ordinate / pixelCloud.size;

	return newPixel(abscissa,ordinate);
}
