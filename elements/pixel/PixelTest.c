/*
 * @title PixelTest.c
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>
#include "Pixel.h"

int main(void)
{
	Pixel* pixel = newPixel(10,10);

	Pixel* pixelbis = newPixel(5,10);

	printf("pixel = pixelbis ? %d\n",isPixelEqualTo(*pixel,*pixelbis));
	printf("pixel = pixel ? %d\n",isPixelEqualTo(*pixel,*pixel));

	freePixel(&pixelbis);
	freePixel(&pixel);
}
