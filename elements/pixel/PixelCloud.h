/*
 * @title PixelCloud.h
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef PIXELCLOUD_H_
#define PIXELCLOUD_H_

#include "Pixel.h"
#include "../image/ImageRGB.h"

/**
 * @chap The 'PixelCloud' data structure and its associated functions
 */

/**
 * @sec The 'PixelCloud' data structure
 */

/**
 * @struct PixelCloud
 * @desc Represent a pixel table
 * @field size (int) : the size of the cloud
 * @field pixels (Pixel**) : the pixels of the cloud
 */
typedef struct PixelCloud
{
	int size;
	Pixel** pixels;
}PixelCloud;

/**
 * @sec The memory handling functions
 */

/**
 * @fn newPixelCloud
 * @desc Create a new pixel cloud from a pixel
 * @param pixel (Pixel) : the first pixel added to the cloud
 * @return (PixelCloud*) : the new pixel cloud
 * @bcode
 */
PixelCloud* newPixelCloud(Pixel pixel);
/**
 * @ecode
 */

/**
 * @fn freePixelCloud
 * @desc Free the memory used by a PixelCloud data structure
 * @param pixelCloud (PixelCloud**) : the address of the structure's pointer
 * @bcode
 */
void freePixelCloud(PixelCloud** pixelCloud);
/**
 * @ecode
 */

/**
 * @sec The getter and setter
 */

/**
 * @fn addPixelInPixelCloud
 * @desc Add a pixel to a given pixel cloud
 * @param pixelCloud (PixelCloud*) : The pixel cloud in which the pixel is added
 * @param pixel (Pixel) : the pixel to add
 * @bcode
 */
void addPixelInPixelCloud(PixelCloud* pixelCloud,Pixel pixel);
/**
 * @ecode
 */

/**
 * @sec The boolean returning functions
 */

/**
 * @fn isPixelInPixelCloud
 * @desc Check if a pixel is inside a pixel cloud
 * @param pixelCloud (PixelCloud) : the pixel cloud in which the pixel might be
 * @param pixel (Pixel) : the pixel to check
 * @return (bool) : true, the pixel is in the pixel cloud, false, the pixel is not in the pixel cloud
 * @bcode
 */
bool isPixelInPixelCloud(PixelCloud pixelCloud, Pixel pixel);
/**
 * @ecode
 */

/**
 * @sec The processing functions
 */

/**
 * @fn drawPixelCloudInImageRGB
 * @desc Draw the pixels of the pixel cloud in a given image
 * @param pixelCloud (PixelCloud) : the pixel cloud to draw
 * @param imageRGB (ImageRGB*) : the image to be modified
 * @param colorRGB (ColorRGB) : the color of the pixel to drawn
 */
void drawPixelCloudInImageRGB(PixelCloud pixelCloud, ImageRGB* imageRGB, ColorRGB colorRGB);
/**
 * @ecode
 */

Pixel* getPixelCloudBarycentre(PixelCloud pixelCloud);

#endif /* PIXELCLOUD_H_ */
