/*
 * @title Pixel.h
 * @date 14 janv. 2019
 * @docauthor Luca Mayer-Dalverny, Anne Sarrazin
 * @make
 */

#ifndef PIXEL_H_
#define PIXEL_H_

#include <stdbool.h>

/**
 * @chap The 'Pixel' data structure and its associated functions
 */

/**
 * @sec The 'Pixel' data structure
 */

/**
 * @struct Pixel
 * @author Luca Mayer-Dalverny
 * @desc Represent a pixel of an image
 * @field abscissa (unsigned int) : the abscisa of the pixel
 * @field ordinate (unsigned int) : the ordinate of the pixel
 */
typedef struct Pixel
{
	unsigned int abscissa;
	unsigned int ordinate;
}Pixel;

/**
 * @sec The memory handling functions
 */

/**
 * @fn newPixel
 * @author Luca Mayer-Dalverny
 * @desc Create a new pixel with a given abscissa and a given ordinate
 * @param abscissa (unsigned int) : the abscisa of the pixel
 * @param ordinate (unsigned int) : the ordinate of the pixel
 * @return (Pixel*) : the allocated pixel
 * @bcode
 */
Pixel* newPixel(unsigned int abscissa, unsigned int ordinate);
/**
 * @ecode
 */

/**
 * @fn freePixel
 * @author Luca Mayer-Dalverny
 * @desc free the memory used by a Pixel structure
 * @param pixel (Pixel**) : the address of the structure pointer
 * @bcode
 */
void freePixel(Pixel** pixel);
/**
 * @ecode
 */

/**
 * @sec The boolean returning functions
 */

/**
 * @fn isPixelEqualTo
 * @author Luca Mayer-Dalverny
 * @desc Check if two pixels are equal
 * @param pixel (Pixel) : the first pixel to compare
 * @param pixel (Pixel) : the second pixel to compare
 * @return (bool) : true, the pixels are equal, false, the pixels are not equal
 * @bcode
 */
bool isPixelEqualTo(Pixel pixel, Pixel pixelToCompare);
/**
 * @ecode
 */

/**
 * @sec The processing functions
 */

/**
 * @fn distanceBetween
 * @author Anne Sarrazin
 * @desc Give the euclidian distance (converted in integer) between two pixels
 * @param pixel1 (Pixel) : the first pixel
 * @param pixel2 (Pixel) : the second pixel
 * @return (int) : the euclidian distance between the two pixels
 * @bcode
 */
int distanceBetween(Pixel * pixel1, Pixel * pixel2);
/**
 * @ecode
 */

#endif /* PIXEL_H_ */
