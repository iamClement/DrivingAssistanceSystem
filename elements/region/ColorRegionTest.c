/*
 * @title ColorRegionTest.c
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "ColorRegion.h"
#include "../pixel/PixelCloud.h"

int main(int argc, char* argv[])
{

	Pixel* firstPixel = newPixel(0,0);
	ColorRGB* firstColor = newColorRGB(255,0,0);

	ColorRegion* region = newColorRegion(*firstPixel,*firstColor);

	ColorRGB* firstAvg = getColorRegionAverage(*region);

	printf("average : %hu %hu %hu\n",firstAvg->red,firstAvg->green,firstAvg->blue);

	ColorRGB* secondColor = newColorRGB(0,0,255);
	Pixel* secondPixel = newPixel(0,1);

	addColorAndPixelToRegion(region,*secondColor,*secondPixel);

	ColorRGB* secondAvg = getColorRegionAverage(*region);

	printf("average : %hu %hu %hu\n",secondAvg->red,secondAvg->green,secondAvg->blue);

	printf("is Pixel in : %d\n",isPixelInColorRegion(*region,*firstPixel));

	Pixel* thirdPixel = newPixel(1,1);

	printf("is Pixel in : %d\n",isPixelInColorRegion(*region,*thirdPixel));

	freeColorRegion(&region);

	freePixel(&firstPixel);
	freePixel(&secondPixel);

	freeColorRGB(&firstColor);
	freeColorRGB(&secondColor);
	freeColorRGB(&firstAvg);
	freeColorRGB(&secondAvg);

	ImageRGB* image = readBMPFile(argv[1]);

	printf("Largeur %d hauteur %d\n",image->width,image->height);

	int regionNumber = 0;

	ColorRegion** regions = growRegionFromImage(*image,&regionNumber,20,20);

	int roadRegionIndex = getLargestRegionIndex(regions,regionNumber);

	Pixel* roadBary = getColorRegionBarycentre(*regions[roadRegionIndex]);

	printf("Après region growing\n");

	Pixel* roadTopPixel = getColorRegionTopPixel(*regions[roadRegionIndex]);

	printf("Top ordinate : %hu\n",roadTopPixel->ordinate);

	int horizon = roadTopPixel->ordinate / 2;
	int upHorizon = horizon + (horizon / 2);

	PixelCloud* skyCloud = NULL;

	PixelCloud* upCloud = NULL;

	PixelCloud* downCloud = NULL;

	int pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < regions[roadRegionIndex]->size ; pixelIndex+=1)
	{
		if(regions[roadRegionIndex]->pixels[pixelIndex]->ordinate > upHorizon)
		{
			if(skyCloud)
			{
				addPixelInPixelCloud(skyCloud,*regions[roadRegionIndex]->pixels[pixelIndex]);
			}
			else
			{
				skyCloud = newPixelCloud(*regions[roadRegionIndex]->pixels[pixelIndex]);
			}
		}
		else
		{
			if(regions[roadRegionIndex]->pixels[pixelIndex]->ordinate > horizon)
			{
				if(upCloud)
				{
					addPixelInPixelCloud(upCloud,*regions[roadRegionIndex]->pixels[pixelIndex]);
				}
				else
				{
					upCloud = newPixelCloud(*regions[roadRegionIndex]->pixels[pixelIndex]);
				}
			}
			else
			{
				if(downCloud)
				{
					addPixelInPixelCloud(downCloud,*regions[roadRegionIndex]->pixels[pixelIndex]);
				}
				else
				{
					downCloud = newPixelCloud(*regions[roadRegionIndex]->pixels[pixelIndex]);
				}
			}
		}
	}

	printf("Après création pixel cloud");

	Pixel* skyBary = getPixelCloudBarycentre(*skyCloud);

	Pixel* upBary = getPixelCloudBarycentre(*upCloud);

	Pixel* downBary = getPixelCloudBarycentre(*downCloud);

	printf("Après obtention barycentre\n");

	ImageRGB* regionImage = getImageFromRegions(regions,regionNumber,image->height,image->width);

	ImageRGB* roadImage = getImageFromRegion(*regions[roadRegionIndex],image->height,image->width);

	regionImage->redMatrix->data[roadBary->ordinate][roadBary->abscissa] = 255;
	regionImage->greenMatrix->data[roadBary->ordinate][roadBary->abscissa] = 0;
	regionImage->blueMatrix->data[roadBary->ordinate][roadBary->abscissa] = 0;

	regionImage->redMatrix->data[upBary->ordinate][upBary->abscissa] = 0;
	regionImage->greenMatrix->data[upBary->ordinate][upBary->abscissa] = 0;
	regionImage->blueMatrix->data[upBary->ordinate][upBary->abscissa] = 255;

	regionImage->redMatrix->data[downBary->ordinate][downBary->abscissa] = 0;
	regionImage->greenMatrix->data[downBary->ordinate][downBary->abscissa] = 0;
	regionImage->blueMatrix->data[downBary->ordinate][downBary->abscissa] = 255;

	regionImage->redMatrix->data[skyBary->ordinate][skyBary->abscissa] = 0;
	regionImage->greenMatrix->data[skyBary->ordinate][skyBary->abscissa] = 0;
	regionImage->blueMatrix->data[skyBary->ordinate][skyBary->abscissa] = 255;

	writeBMPFile(*regionImage,"../../image-files/virages/regions.bmp");

	writeBMPFile(*roadImage,"../../image-files/virages/road.bmp");

	freeRGBImage(&regionImage);

	freeRGBImage(&image);

	return 0;
}
