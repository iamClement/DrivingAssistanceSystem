/*
 * @title ColorRegion.c
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "ColorRegion.h"

ColorRegion* newColorRegion(Pixel pixel, ColorRGB colorRGB)
{
	ColorRegion* colorRegion = (ColorRegion*) malloc(sizeof(ColorRegion));

	colorRegion->size = 1;

	colorRegion->colors = (ColorRGB**) malloc(sizeof(ColorRGB*));
	colorRegion->pixels = (Pixel**) malloc(sizeof(Pixel*));

	colorRegion->colors[0] = newColorRGB(colorRGB.red,colorRGB.green,colorRGB.blue);
	colorRegion->pixels[0] = newPixel(pixel.abscissa,pixel.ordinate);

	return colorRegion;
}

void freeColorRegion(ColorRegion** colorRegion)
{
	if(colorRegion != NULL)
	{
		if(*colorRegion != NULL)
		{
			ColorRegion* interimColorRegion = *colorRegion;

			int indexRegion;

			for(indexRegion = 0 ; indexRegion < interimColorRegion->size ; indexRegion+=1)
			{
				freePixel(&interimColorRegion->pixels[indexRegion]);
				freeColorRGB(&interimColorRegion->colors[indexRegion]);
			}

			free(interimColorRegion->pixels);
			free(interimColorRegion->colors);

			free(interimColorRegion);

			*colorRegion = NULL;
		}
	}
}

void addColorAndPixelToRegion(ColorRegion* colorRegion, ColorRGB colorRGB, Pixel pixel)
{
	if(!isPixelInColorRegion(*colorRegion,pixel))
	{
		colorRegion->size+=1;

		colorRegion->pixels = (Pixel**) realloc(colorRegion->pixels,colorRegion->size * sizeof(Pixel*));
		colorRegion->colors = (ColorRGB**) realloc(colorRegion->colors,colorRegion->size * sizeof(ColorRGB*));

		colorRegion->pixels[colorRegion->size-1] = (Pixel*) malloc(sizeof(Pixel));
		colorRegion->colors[colorRegion->size-1] = (ColorRGB*) malloc(sizeof(ColorRGB));

		colorRegion->pixels[colorRegion->size-1]->abscissa = pixel.abscissa;
		colorRegion->pixels[colorRegion->size-1]->ordinate = pixel.ordinate;

		colorRegion->colors[colorRegion->size-1]->red = colorRGB.red;
		colorRegion->colors[colorRegion->size-1]->green = colorRGB.green;
		colorRegion->colors[colorRegion->size-1]->blue = colorRGB.blue;

	}
}

bool isPixelInColorRegion(ColorRegion colorRegion, Pixel pixel)
{
	bool postulate = false;

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < colorRegion.size ; regionIndex+=1)
	{
		if(isPixelEqualTo(pixel,*colorRegion.pixels[regionIndex]))
		{
			postulate = true;
			break;
		}
	}

	return postulate;
}

bool isPixelInColorRegionTable(ColorRegion** colorRegions, int regionNumber, Pixel pixel)
{

	bool postulate = false;

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < regionNumber ; regionIndex+=1)
	{
		if(isPixelInColorRegion(*colorRegions[regionIndex],pixel))
		{
			postulate = true;
			break;
		}
	}

	return postulate;
}

ColorRGB* getColorRegionAverage(ColorRegion colorRegion)
{
	unsigned int red = 0;
	unsigned int green = 0;
	unsigned int blue = 0;

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < colorRegion.size ; regionIndex+=1)
	{
		red+=colorRegion.colors[regionIndex]->red;
		green+=colorRegion.colors[regionIndex]->green;
		blue+=colorRegion.colors[regionIndex]->blue;
	}

	red = (unsigned int) red / colorRegion.size;
	green = (unsigned int) green / colorRegion.size;
	blue = (unsigned int) blue / colorRegion.size;

	return newColorRGB(red,green,blue);
}

Pixel* getColorRegionBarycentre(ColorRegion colorRegion)
{
	unsigned int abscissa = 0;
	unsigned int ordinate = 0;

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < colorRegion.size ; regionIndex+=1)
	{
		abscissa+=colorRegion.pixels[regionIndex]->abscissa;
		ordinate+=colorRegion.pixels[regionIndex]->ordinate;
	}

	abscissa = (unsigned int) abscissa / colorRegion.size;
	ordinate = (unsigned int) ordinate / colorRegion.size;

	return newPixel(abscissa,ordinate);
}

Pixel* getColorRegionTopPixel(ColorRegion colorRegion)
{
	unsigned int ordinateMax = 0;

	int indexFound, pixelIndex;

	for(pixelIndex = 0 ; pixelIndex < colorRegion.size ; pixelIndex+=1)
	{
		if(colorRegion.pixels[pixelIndex]->ordinate > ordinateMax)
		{
			ordinateMax = colorRegion.pixels[pixelIndex]->ordinate;
			indexFound = pixelIndex;
		}
	}

	return newPixel(colorRegion.pixels[indexFound]->abscissa,colorRegion.pixels[indexFound]->ordinate);
}

int getLargestRegionIndex(ColorRegion** colorRegions, int regionNumber)
{
	int indexFound = 0;

	int regionIndex;

	for(regionIndex = 1 ; regionIndex < regionNumber ; regionIndex+=1)
	{
		if(colorRegions[indexFound]->size < colorRegions[regionIndex]->size)
		{
			indexFound = regionIndex;
		}
	}

	return indexFound;
}

void writeRegionInImage(ColorRegion colorRegion, ImageRGB* imageRGB)
{
	ColorRGB* averageColor = getColorRegionAverage(colorRegion);

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < colorRegion.size ; regionIndex+=1)
	{
		setPixelColorOfImageRGB(imageRGB,*averageColor,*colorRegion.pixels[regionIndex]);
	}

	freeColorRGB(&averageColor);
}

ImageRGB* getImageFromRegion(ColorRegion colorRegion, int height, int width)
{
	ImageRGB* imageRGB = newRGBImage(height,width);

	writeRegionInImage(colorRegion,imageRGB);

	return imageRGB;
}

ImageRGB* getImageFromRegions(ColorRegion** colorRegions, int regionNumber, int height, int width)
{
	ImageRGB* imageRGB = newRGBImage(height,width);

	int regionIndex;

	for(regionIndex = 0 ; regionIndex < regionNumber ; regionIndex+=1)
	{
		writeRegionInImage(*colorRegions[regionIndex],imageRGB);
	}

	return imageRGB;
}

void growRegionFromPixel(ColorRegion** colorRegions, int regionNumber, ImageRGB image, Pixel pixel, float localRate, float globalRate)
{
	ColorRGB* currentColor = getPixelColorOfImageRGB(image,pixel);

	ColorRGB* averageColor = getColorRegionAverage(*colorRegions[regionNumber-1]);

	if( (((int) pixel.abscissa ) -1 ) >= 0 )
	{
		Pixel* leftPixel = newPixel(pixel.abscissa-1,pixel.ordinate);

		ColorRGB* leftColor = getPixelColorOfImageRGB(image,*leftPixel);

		float distanceWithCurrent = getDistanceBetweenColor(*currentColor,*leftColor);
		float distanceWithAverage = getDistanceBetweenColor(*averageColor,*leftColor);

		if( (distanceWithCurrent <= localRate) && (distanceWithAverage <= globalRate) && (!isPixelInColorRegionTable(colorRegions,regionNumber,*leftPixel)))
		{
			addColorAndPixelToRegion(colorRegions[regionNumber-1],*leftColor,*leftPixel);
			growRegionFromPixel(colorRegions,regionNumber,image,*leftPixel,localRate,globalRate);
		}

		freePixel(&leftPixel);
		freeColorRGB(&leftColor);
	}

	if( ( ((int) pixel.abscissa) +1  ) < image.width )
	{
		Pixel* rightPixel = newPixel(pixel.abscissa+1,pixel.ordinate);

		ColorRGB* rightColor = getPixelColorOfImageRGB(image,*rightPixel);

		float distanceWithCurrent = getDistanceBetweenColor(*currentColor,*rightColor);
		float distanceWithAverage = getDistanceBetweenColor(*averageColor,*rightColor);

		if( (distanceWithCurrent <= localRate) && (distanceWithAverage <= globalRate) && (!isPixelInColorRegionTable(colorRegions,regionNumber,*rightPixel)) )
		{
			addColorAndPixelToRegion(colorRegions[regionNumber-1],*rightColor,*rightPixel);
			growRegionFromPixel(colorRegions,regionNumber,image,*rightPixel,localRate,globalRate);
		}

		freePixel(&rightPixel);
		freeColorRGB(&rightColor);
	}

	if( ( ((int) pixel.ordinate) -1 ) >= 0 )
	{
		Pixel* upPixel = newPixel(pixel.abscissa,pixel.ordinate-1);

		ColorRGB* upColor = getPixelColorOfImageRGB(image,*upPixel);

		float distanceWithCurrent = getDistanceBetweenColor(*currentColor,*upColor);
		float distanceWithAverage = getDistanceBetweenColor(*averageColor,*upColor);

		if( (distanceWithCurrent <= localRate) && (distanceWithAverage <= globalRate) && (!isPixelInColorRegionTable(colorRegions,regionNumber,*upPixel)) )
		{
			addColorAndPixelToRegion(colorRegions[regionNumber-1],*upColor,*upPixel);
			growRegionFromPixel(colorRegions,regionNumber,image,*upPixel,localRate,globalRate);
		}

		freePixel(&upPixel);
		freeColorRGB(&upColor);
	}

	if( ( ((int) pixel.ordinate) +1 ) < image.height)
	{
		Pixel* downPixel = newPixel(pixel.abscissa,pixel.ordinate+1);

		ColorRGB* downColor = getPixelColorOfImageRGB(image,*downPixel);

		float distanceWithCurrent = getDistanceBetweenColor(*currentColor,*downColor);
		float distanceWithAverage = getDistanceBetweenColor(*averageColor,*downColor);

		if( (distanceWithCurrent <= localRate) && (distanceWithAverage <= globalRate) && (!isPixelInColorRegionTable(colorRegions,regionNumber,*downPixel)) )
		{
			addColorAndPixelToRegion(colorRegions[regionNumber-1],*downColor,*downPixel);
			growRegionFromPixel(colorRegions,regionNumber,image,*downPixel,localRate,globalRate);
		}

		freePixel(&downPixel);
		freeColorRGB(&downColor);
	}

	freeColorRGB(&currentColor);
	freeColorRGB(&averageColor);
}

ColorRegion** growRegionFromImage(ImageRGB image, int* regionNumber, float localRate, float globalRate)
{

	Pixel* origin = newPixel(0,0);
	ColorRGB* colorOrigin = getPixelColorOfImageRGB(image,*origin);

	ColorRegion** colorRegions = (ColorRegion**) malloc(sizeof(ColorRegion*));
	colorRegions[0] = newColorRegion(*origin,*colorOrigin);
	*regionNumber = 1;

	growRegionFromPixel(colorRegions,*regionNumber,image,*origin,localRate,globalRate);

	freePixel(&origin);
	freeColorRGB(&colorOrigin);

	int columnIndex, rowIndex;

	for(columnIndex = 0 ; columnIndex < image.width ; columnIndex+=1)
	{
		for(rowIndex = 0 ; rowIndex < image.height ; rowIndex+=1)
		{
			Pixel* currentPixel = newPixel((unsigned int) columnIndex, (unsigned int) rowIndex);

			ColorRGB* currentColor = getPixelColorOfImageRGB(image,*currentPixel);

			if(!isPixelInColorRegionTable(colorRegions,*regionNumber,*currentPixel))
			{
				//printf("Dans le if pour [%d][%d]\n",columnIndex,rowIndex);

				(*regionNumber)+=1;

				int index = *regionNumber;

				colorRegions = (ColorRegion**) realloc(colorRegions,index * sizeof(ColorRegion*));

				//colorRegions[index-1] = NULL;

				colorRegions[index-1] = newColorRegion(*currentPixel,*currentColor);

				growRegionFromPixel(colorRegions,*regionNumber,image,*currentPixel,localRate,globalRate);

			}

			freePixel(&currentPixel);
			freeColorRGB(&currentColor);
		}
	}

	return colorRegions;
}
