/*
 * @title ColorRegion.h
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef COLORREGION_H_
#define COLORREGION_H_

#include "../image/ImageRGB.h"

/**
 * @chap The 'ColorRegion' data structure and its associated functions
 */

/**
 * @sec The 'ColorRegion' data structure
 */

/**
 * @struct ColorRegion
 * @desc Represent a color region of an RGB Image
 * @field size (int) : the size of the region
 * @field colors (ColorRGB**) : the list of color of the region
 * @field pixels (Pixel**) : the list of the pixel of the region
 */
typedef struct ColorRegion
{
	int size;
	ColorRGB** colors;
	Pixel** pixels;
}ColorRegion;

/**
 * @sec The memory handling functions
 */

/**
 * @fn newColorRegion
 * @desc Create a new color region from a seed
 * @param pixel (Pixel) : the seed of the region
 * @param colorRGB (ColorRGB) : the color of the seed
 * @return (ColorRegion*) : the allocated region
 * @bcode
 */
ColorRegion* newColorRegion(Pixel pixel, ColorRGB colorRGB);
/**
 * @ecode
 */

/**
 * @fn freeColorRegion
 * @desc Free the memory used by a 'ColorRegion' structure
 * @param colorRegion (ColorRegion**) : the address of the pointer of the region to free
 * @bcode
 */
void freeColorRegion(ColorRegion** colorRegion);
/**
 * @ecode
 */

/**
 * @sec The getter and setter
 */

/**
 * @fn addColorAndPixelToRegion
 * @desc Add a color and a pixel to the region
 * @param colorRegion (ColorRegion*) : the region to modify
 * @param colorRGB (ColorRGB) : the color to add
 * @param pixel (Pixel) : the pixel to add
 * @bcode
 */
void addColorAndPixelToRegion(ColorRegion* colorRegion, ColorRGB colorRGB, Pixel pixel);
/**
 * @ecode
 */

/**
 * @sec The boolean returning functions
 */

/**
 * @fn isPixelInColorRegion
 * @desc Check if a given pixel is inside a region
 * @param colorRegion (ColorRegion) : the region where the presence of the pixel will be checked
 * @param pixel (Pixel) : the pixel to check
 * @return (bool) : true, the pixel is in the region, false, the pixel is not in the region
 * @bcode
 */
bool isPixelInColorRegion(ColorRegion colorRegion, Pixel pixel);
/**
 * @ecode
 */

/**
 * @fn isPixelInColorRegionTable
 * @desc Check if a pixel is in a table of colorRegion
 * @param colorRegions (ColorRegion*) : the list of region
 * @param regionNumber (int) : the number of region in the list
 * @param pixel (Pixel) : the pixel to check
 * @return (bool) : true, the pixel is in one of the regions, false, the pixel is not in one of the regions
 * @bcode
 */
bool isPixelInColorRegionTable(ColorRegion** colorRegions, int regionNumber, Pixel pixel);
/**
 * @ecode
 */

/**
 * @sec The processing functions
 */

/**
 * @fn getColorRegionAverage
 * @desc Give the average color of all the colors contained in the region
 * @param colorRegion (ColorRegion) : the region whose average color we want
 * @return (ColorRGB*) : the average color of the region
 * @bcode
 */
ColorRGB* getColorRegionAverage(ColorRegion colorRegion);
/**
 * @ecode
 */

/**
 * @fn getColorRegionBarycentre
 * @desc Give the barycentre of all the pixels contained in the given region
 * @param colorRegion (ColorRegion) : the region whose barycentre we want
 * @return (Pixel*) : the barycentre of the region
 * @bcode
 */
Pixel* getColorRegionBarycentre(ColorRegion colorRegion);
/**
 * @ecode
 */

Pixel* getColorRegionTopPixel(ColorRegion colorRegion);

/**
 * @fn getLargestRegionIndex
 * @desc Give the index of the largest region contained in a region table
 * @param colorRegion (ColorRegion*) : the region table
 * @param regionNumber (int) : the number of region in the region table
 * @return (int) the index of the largest region
 * @bcode
 */
int getLargestRegionIndex(ColorRegion** colorRegions, int regionNumber);
/**
 * @ecode
 */

/**
 * @fn writeRegionInImage
 * @desc Write the average color of the region in the pixels of an image
 * @param colorRegion (ColorRegion) : the region to write
 * @param imageRGB (ImageRGB*) : the image to modify
 * @bcode
 */
void writeRegionInImage(ColorRegion colorRegion, ImageRGB* imageRGB);
/**
 * @ecode
 */

/**
 * @fn getImageFromRegion
 * @desc Get an image with the given region written in it
 * @param colorRegion (ColorRegion) : the region to write in the image
 * @param height (int) : the height of the image to create
 * @param width (int) : the width of the image to create
 * @return (ImageRGB*) : the image created
 * @bcode
 */
ImageRGB* getImageFromRegion(ColorRegion colorRegion, int height, int width);
/**
 * @ecode
 */

/**
 * @fn getImageFromRegions
 * @desc
 * @desc Get an image with the given regions written in it
 * @param colorRegion (ColorRegion*) : the region table to write in the image
 * @param regionNumber (int) : the number of region in the region table
 * @param height (int) : the height of the image to create
 * @param width (int) : the width of the image to create
 * @return (ImageRGB*) : the image created
 * @bcode
 */
ImageRGB* getImageFromRegions(ColorRegion** colorRegions, int regionNumber, int height, int width);
/**
 * @ecode
 */

/**
 * @fn growRegionFromPixel
 * @desc Grow a color region from a pixel in a image
 * @param colorRegions (ColorRegion**) : the list of the regions found
 * @param regionNumber (int) : the number of regions in the list
 * @param image (ImageRGB) : the image where the region is from
 * @param pixel (Pixel) : the seed of the region
 * @param localRate (float) :
 * @param globalRate (float) :
 * @bcode
 */
void growRegionFromPixel(ColorRegion** colorRegions, int regionNumber, ImageRGB image, Pixel pixel, float localRate, float globalRate);
/**
 * @ecode
 */

/**
 * @fn growRegionFromImage
 * @desc Grow several color regions from a given image
 * @param image (ImageRGB) : the image from which the regions are grown
 * @param regionNumber (int*) : the number of region created at the end of the process
 * @param localRate (float) :
 * @param globalRate (float) :
 * @bcode
 */
ColorRegion** growRegionFromImage(ImageRGB image, int* regionNumber, float localRate, float globalRate);
/**
 * @ecode
 */

#endif /* COLORREGION_H_ */

#if 0

/**
 * @chap Use example
 */

/**
 * @sec Growing a region from a pixel and storing it in a image
 */

/**
 * @bcode
 */
//Opening the image from which you want the region
ImageRGB* image = readBMPFile("image.bmp");
//Allocating the seed of your region
Pixel* seed = newPixel(0,0);
//Getting the seed's color
ColorRGB* seedColor = getPixelColorOfImageRGB(*image,*seed);
//Allocating the 'ColorRegion' table
ColorRegion** regions = (ColorRegion**) malloc(sizeof(ColorRegion*));
//Allocating the region from the seed
regions[0] = newColorRegion(*seed,*seedColor);
//Setting the local and global rates
float localRate = 50;
float globalRate = 50;
//Growing the region from the seed
growRegionFromPixel(regions,1,*image,*seed,localRate,globalRate);
//Getting the image containing the region
ImageRGB regionImage = getImageFromRegions(*regions,1,image->height,image->width);
/**
 * @ecode
 */
#endif
