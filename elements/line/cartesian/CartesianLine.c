/*
 * @title CartesianLine.c
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "CartesianLine.h"

CartesianLine* newCartesianLineFromPixels(Pixel firstPixel, Pixel secondPixel)
{
	CartesianLine* cartesianLine = (CartesianLine*) malloc(sizeof(CartesianLine));

	cartesianLine->leftEnd = (Pixel*) malloc(sizeof(Pixel));
	cartesianLine->rightEnd = (Pixel*) malloc(sizeof(Pixel));

	if(!isPixelEqualTo(firstPixel,secondPixel))
	{
		if(firstPixel.abscissa == secondPixel.abscissa)
		{
			cartesianLine->isVertical = true;
			cartesianLine->isHorizontal = false;
			cartesianLine->slope = 1;
			cartesianLine->OriginIntercept = (float) firstPixel.abscissa;

			if(firstPixel.ordinate < secondPixel.ordinate)
			{
				setLeftEndPixel(cartesianLine,firstPixel);
				setRightEndPixel(cartesianLine,secondPixel);
			}
			else
			{
				setLeftEndPixel(cartesianLine,secondPixel);
				setRightEndPixel(cartesianLine,firstPixel);
			}
		}
		else
		{
			cartesianLine->isVertical = false;

			if(firstPixel.abscissa < secondPixel.abscissa)
			{
				setLeftEndPixel(cartesianLine,firstPixel);
				setRightEndPixel(cartesianLine,secondPixel);
			}
			else
			{
				setLeftEndPixel(cartesianLine,secondPixel);
				setRightEndPixel(cartesianLine,firstPixel);
			}


			if(firstPixel.ordinate == secondPixel.ordinate)
			{
				cartesianLine->isHorizontal = true;
				cartesianLine->slope = 0;
				cartesianLine->OriginIntercept = (float) firstPixel.ordinate;
			}
			else
			{
				cartesianLine->isHorizontal = false;

				float firstAbscissa = (float) firstPixel.abscissa;
				float firstOrdinate = (float) firstPixel.ordinate;
				float secondAbscissa = (float) secondPixel.abscissa;
				float secondOrdinate = (float) secondPixel.ordinate;

				cartesianLine->slope = (secondOrdinate - firstOrdinate) / (secondAbscissa - firstAbscissa);
				cartesianLine->OriginIntercept = firstOrdinate - (cartesianLine->slope * firstAbscissa);
			}
		}
	}

	return cartesianLine;
}

void freeCartesianLine(CartesianLine** cartesianLine)
{
	if(cartesianLine != NULL)
	{
		if(*cartesianLine != NULL)
		{
			CartesianLine* interimCartesianLine = *cartesianLine;

			freePixel(&interimCartesianLine->leftEnd);
			freePixel(&interimCartesianLine->rightEnd);

			free(interimCartesianLine);

			*cartesianLine = NULL;
		}
	}
}

void setLeftEndPixel(CartesianLine* cartesianLine, Pixel pixel)
{
	cartesianLine->leftEnd->abscissa = pixel.abscissa;
	cartesianLine->leftEnd->ordinate = pixel.ordinate;
}

void setRightEndPixel(CartesianLine* cartesianLine, Pixel pixel)
{
	cartesianLine->rightEnd->abscissa = pixel.abscissa;
	cartesianLine->rightEnd->ordinate = pixel.ordinate;

}

bool isCartesianLineEqualTo(CartesianLine cartesianLine, CartesianLine cartesianLineToCompare, float accuracy)
{
	bool postulate = false;

	if((cartesianLine.isVertical == cartesianLineToCompare.isVertical)&&(cartesianLine.isHorizontal == cartesianLineToCompare.isHorizontal))
	{
		float slopeDifferential = cartesianLine.slope - cartesianLineToCompare.slope;

		if((slopeDifferential > (-accuracy))&&(slopeDifferential < accuracy))
		{
			float interceptDifferential = cartesianLine.OriginIntercept - cartesianLineToCompare.OriginIntercept;

			if((interceptDifferential > (-accuracy))&&(interceptDifferential < accuracy))
			{
				postulate = true;
			}
		}
	}

	return postulate;
}

bool isCartesianLineParallelTo(CartesianLine cartesianLine, CartesianLine cartesianLineToCompare, float accuracy)
{
	bool postulate = false;

	if((cartesianLine.isVertical)&&(cartesianLineToCompare.isVertical))
	{
		postulate = true;
	}
	else
	{
		if((cartesianLine.isHorizontal)&&(cartesianLine.isHorizontal))
		{
			postulate = true;
		}
		else
		{
			float slopeDifferential = cartesianLine.slope - cartesianLineToCompare.slope;

			if((slopeDifferential > (-accuracy))&&(slopeDifferential < accuracy))
			{
				postulate = true;
			}
		}
	}

	return postulate;
}

bool isPixelInCartesianLine(CartesianLine cartesianLine, Pixel pixel, float accuracy)
{
	bool postulate = false;

	if(cartesianLine.isVertical)
	{
		if(pixel.abscissa == cartesianLine.leftEnd->abscissa)
		{
			postulate = true;
		}
	}
	else
	{
		if(cartesianLine.isHorizontal)
		{
			if(pixel.ordinate == cartesianLine.leftEnd->ordinate)
			{
				postulate = true;
			}
		}
		else
		{
			float linearEquation = (((float) pixel.abscissa) * cartesianLine.slope) + cartesianLine.OriginIntercept;
			float equationDifferential = ((float) pixel.ordinate) - linearEquation;

			if((equationDifferential > (-accuracy))&&(equationDifferential < accuracy))
			{
				postulate = true;
			}

		}
	}

	return postulate;
}

void printCartesianLine(CartesianLine cartesianLine, bool printEnds)
{
	if(printEnds)
	{
		//printPixel(*cartesianLine.leftEnd);
		//printf(" - ");
		//printPixel(*cartesianLine.rightEnd);
		//printf(" : ");
	}

	if(cartesianLine.isVertical)
	{
		printf("x = %f\n",cartesianLine.OriginIntercept);
	}
	else
	{
		if(cartesianLine.isHorizontal)
		{
			printf("y = %f\n",cartesianLine.OriginIntercept);
		}
		else
		{
			printf("y = %f . x + %f\n",cartesianLine.slope,cartesianLine.OriginIntercept);
		}
	}
}

void drawCartesianLineInMatrix(CartesianLine cartesianLine, Matrix* matrix, short int greyLevel)
{
	if(cartesianLine.isVertical)
	{
		int rowIndex;
		int columnIndex = (int) cartesianLine.OriginIntercept;

		for(rowIndex = cartesianLine.leftEnd->ordinate ; rowIndex < cartesianLine.rightEnd->ordinate ; rowIndex+=1 )
		{
			addValueToMatrixAt(matrix,greyLevel,columnIndex,rowIndex);
		}
	}
	else
	{
		if(cartesianLine.isHorizontal)
		{
			int columnIndex;
			int rowIndex = (int) cartesianLine.OriginIntercept;

			for(columnIndex = cartesianLine.leftEnd->abscissa ; columnIndex < cartesianLine.rightEnd->abscissa; columnIndex+=1)
			{
				addValueToMatrixAt(matrix,greyLevel,columnIndex,rowIndex);
			}
		}
		else
		{
			int columnIndex, rowIndex;
			float startRow = cartesianLine.leftEnd->ordinate;
			float endRow = startRow+1;

			for(columnIndex = cartesianLine.leftEnd->abscissa ; columnIndex < cartesianLine.rightEnd->abscissa ; columnIndex+=1)
			{
				for(rowIndex = startRow ; rowIndex < endRow ; rowIndex+=1)
				{
					addValueToMatrixAt(matrix,greyLevel,columnIndex,(int) rowIndex);
				}

				startRow+=cartesianLine.slope;
				endRow+=cartesianLine.slope;
			}
		}
	}
}

void drawCartesianLineInImage(CartesianLine cartesianLine, ImageRGB* imageRGB, ColorRGB colorRGB)
{
	drawCartesianLineInMatrix(cartesianLine,imageRGB->redMatrix,colorRGB.red);
	drawCartesianLineInMatrix(cartesianLine,imageRGB->greenMatrix,colorRGB.green);
	drawCartesianLineInMatrix(cartesianLine,imageRGB->blueMatrix,colorRGB.blue);
}

Pixel* getIntersectionPixel(CartesianLine firstLine, CartesianLine secondLine)
{
	Pixel* intersectionPixel = NULL;

	if(!isCartesianLineParallelTo(firstLine,secondLine,0.1))
	{
		if(firstLine.isVertical)
		{
			if(secondLine.isHorizontal)
			{
				intersectionPixel = newPixel(firstLine.OriginIntercept,secondLine.OriginIntercept);
			}
			else
			{
				unsigned int abscissa = firstLine.OriginIntercept;
				int ordinate = (int) (secondLine.slope * abscissa + secondLine.OriginIntercept);

				if(ordinate >= 0)
				{
					intersectionPixel = newPixel(abscissa,ordinate);
				}
			}
		}
		else
		{
			if(firstLine.isHorizontal)
			{
				if(secondLine.isVertical)
				{
					intersectionPixel = newPixel(secondLine.OriginIntercept,firstLine.OriginIntercept);
				}
				else
				{
					unsigned int ordinate = firstLine.OriginIntercept;
					int abscissa = (int) ((ordinate - secondLine.OriginIntercept)/secondLine.slope);

					if(ordinate >= 0)
					{
						intersectionPixel = newPixel(abscissa,ordinate);
					}
				}
			}
			else
			{
				if(secondLine.isVertical)
				{
					unsigned int abscissa = secondLine.OriginIntercept;
					int ordinate = (int) (abscissa * firstLine.slope + firstLine.OriginIntercept);

					if(ordinate >= 0)
					{
						intersectionPixel = newPixel(abscissa,ordinate);
					}
				}
				else
				{
					if(secondLine.isHorizontal)
					{
						unsigned int ordinate = secondLine.OriginIntercept;
						int abscissa = (int) ((ordinate - firstLine.OriginIntercept)/firstLine.slope);

						if(ordinate >= 0)
						{
							intersectionPixel = newPixel(abscissa,ordinate);
						}
					}
					else
					{

						int abscissa = (int) ( (secondLine.OriginIntercept - firstLine.OriginIntercept)/(firstLine.slope - secondLine.slope) );
						int ordinate = (int) ( abscissa * firstLine.slope + firstLine.OriginIntercept );

						if((abscissa >= 0)&&(ordinate >= 0))
						{
							intersectionPixel = newPixel(abscissa,ordinate);
						}
					}
				}

			}
		}
	}

	return intersectionPixel;
}

CartesianLine* getOrthogonalCartesianLine(CartesianLine cartesianLine, Pixel pixel)
{
	CartesianLine* orthogonalLine = (CartesianLine*) malloc(sizeof(CartesianLine));

	orthogonalLine->leftEnd = (Pixel*) malloc(sizeof(Pixel));
	orthogonalLine->rightEnd = (Pixel*) malloc(sizeof(Pixel));

	if(cartesianLine.isVertical)
	{
		orthogonalLine->isVertical = false;
		orthogonalLine->isHorizontal = true;
		orthogonalLine->slope = 0;
		orthogonalLine->OriginIntercept = pixel.ordinate;
	}
	else
	{
		orthogonalLine->isHorizontal = false;

		if(cartesianLine.isHorizontal)
		{
			orthogonalLine->isVertical = true;
			orthogonalLine->slope = 1;
			orthogonalLine->OriginIntercept = pixel.abscissa;
		}
		else
		{
			orthogonalLine->isVertical = false;
			orthogonalLine->slope = - (1 / cartesianLine.slope);
			orthogonalLine->OriginIntercept = ((float) pixel.ordinate) - (orthogonalLine->slope * ((float) pixel.abscissa));
		}
	}

	setLeftEndPixel(orthogonalLine,pixel);
	setRightEndPixel(orthogonalLine,pixel);

	return orthogonalLine;
}


void addPixelToCartesianLine(CartesianLine* cartesianLine, Pixel pixel)
{
	if(cartesianLine->isVertical)
	{
		if(pixel.ordinate < cartesianLine->leftEnd->ordinate)
		{
			setLeftEndPixel(cartesianLine,pixel);
		}
		else
		{
			if(pixel.ordinate > cartesianLine->rightEnd->ordinate)
			{
				setRightEndPixel(cartesianLine,pixel);
			}
		}
	}
	else
	{
		if(pixel.abscissa < cartesianLine->leftEnd->abscissa)
		{
			setLeftEndPixel(cartesianLine,pixel);
		}
		else
		{
			if(pixel.abscissa > cartesianLine->rightEnd->abscissa)
			{
				setRightEndPixel(cartesianLine,pixel);
			}
		}
	}
}
