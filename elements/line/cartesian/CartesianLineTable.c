/*
 * @title CartesianLineTable.c
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "CartesianLineTable.h"

CartesianLineTable* newCartesianLineTable(CartesianLine cartesianLine)
{
	CartesianLineTable* cartesianLineTable = (CartesianLineTable*) malloc(sizeof(CartesianLineTable));

	cartesianLineTable->size = 1;

	cartesianLineTable->lines = (CartesianLine**) malloc(sizeof(CartesianLine*));

	cartesianLineTable->lines[0] = (CartesianLine*) malloc(sizeof(CartesianLine));

	cartesianLineTable->lines[0]->isVertical = cartesianLine.isVertical;
	cartesianLineTable->lines[0]->isHorizontal = cartesianLine.isHorizontal;
	cartesianLineTable->lines[0]->slope = cartesianLine.slope;
	cartesianLineTable->lines[0]->OriginIntercept = cartesianLine.OriginIntercept;
	cartesianLineTable->lines[0]->leftEnd = newPixel(cartesianLine.leftEnd->abscissa,cartesianLine.leftEnd->ordinate);
	cartesianLineTable->lines[0]->rightEnd = newPixel(cartesianLine.rightEnd->abscissa,cartesianLine.rightEnd->ordinate);

	cartesianLineTable->occurency = (int*) malloc(sizeof(int));

	cartesianLineTable->occurency[0] = 1;

	return cartesianLineTable;
}

void freeCartesianLineTable(CartesianLineTable** cartesianLineTable)
{
	if(cartesianLineTable != NULL)
	{
		if(*cartesianLineTable != NULL)
		{
			CartesianLineTable* interimCartesianLineTable = *cartesianLineTable;

			int lineIndex;

			for(lineIndex = 0 ; lineIndex < interimCartesianLineTable->size ; lineIndex+=1)
			{
				freeCartesianLine(&interimCartesianLineTable->lines[lineIndex]);
			}

			free(interimCartesianLineTable->occurency);

			free(interimCartesianLineTable->lines);

			free(interimCartesianLineTable);

			*cartesianLineTable = NULL;
		}
	}
}

void addCartesianLineToTable(CartesianLineTable* cartesianLineTable, CartesianLine cartesianLine)
{
	cartesianLineTable->size+=1;
	cartesianLineTable->lines = (CartesianLine**) realloc(cartesianLineTable->lines,cartesianLineTable->size * sizeof(CartesianLine*));
	cartesianLineTable->lines[cartesianLineTable->size-1] = (CartesianLine*) malloc(sizeof(CartesianLine));

	cartesianLineTable->lines[cartesianLineTable->size-1]->isVertical = cartesianLine.isVertical;
	cartesianLineTable->lines[cartesianLineTable->size-1]->isHorizontal = cartesianLine.isHorizontal;
	cartesianLineTable->lines[cartesianLineTable->size-1]->slope = cartesianLine.slope;
	cartesianLineTable->lines[cartesianLineTable->size-1]->OriginIntercept = cartesianLine.OriginIntercept;
	cartesianLineTable->lines[cartesianLineTable->size-1]->leftEnd = newPixel(cartesianLine.leftEnd->abscissa,cartesianLine.leftEnd->ordinate);
	cartesianLineTable->lines[cartesianLineTable->size-1]->rightEnd = newPixel(cartesianLine.rightEnd->abscissa,cartesianLine.rightEnd->ordinate);

	cartesianLineTable->occurency = (int*) realloc(cartesianLineTable->occurency,cartesianLineTable->size * sizeof(int));

	cartesianLineTable->occurency[cartesianLineTable->size-1] = 1;
}

int isCartesianLineInTable(CartesianLineTable cartesianLineTable, CartesianLine cartesianLine)
{
	int indexFound = -1;

	int lineIndex;

	for(lineIndex = 0 ; lineIndex < cartesianLineTable.size ; lineIndex+=1)
	{
		if(isCartesianLineEqualTo(cartesianLine,*cartesianLineTable.lines[lineIndex],1))
		{
			indexFound = lineIndex;
			break;
		}
	}

	return indexFound;
}

void drawCartesianLineTableInImage(CartesianLineTable cartesianLineTable, ImageRGB* imageRGB, ColorRGB colorRGB)
{
	int lineIndex;

	for(lineIndex = 0 ; lineIndex < cartesianLineTable.size ; lineIndex+=1)
	{
		drawCartesianLineInImage(*cartesianLineTable.lines[lineIndex],imageRGB,colorRGB);
	}
}

int getMaximumOcurencyIndex(CartesianLineTable cartesianLineTable, int ocurencyMin)
{
	int indexFound = 0;

	bool isIndexFound = false;

	int lineIndex;

	for(lineIndex = 0 ; lineIndex < cartesianLineTable.size ; lineIndex+=1)
	{
		if((cartesianLineTable.occurency[lineIndex] >= cartesianLineTable.occurency[indexFound])&&(cartesianLineTable.occurency[lineIndex] > ocurencyMin))
		{
			indexFound = lineIndex;
			isIndexFound = true;
		}
	}

	if(!isIndexFound)
	{
		indexFound = -1;
	}

	return indexFound;
}

CartesianLineTable* getSignificantLines(CartesianLineTable cartesianTable, int ocurencyMin)
{

	CartesianLineTable* significantLines = NULL;

	int indexFound = getMaximumOcurencyIndex(cartesianTable,ocurencyMin);

	//printf("First Index found : %d with %d ocurency\n",indexFound,cartesianTable.occurency[indexFound]);

	while(indexFound != -1)
	{
		if(significantLines != NULL)
		{
			addCartesianLineToTable(significantLines,*cartesianTable.lines[indexFound]);

			int lineIndex;

			for(lineIndex = 0 ; lineIndex < cartesianTable.size ; lineIndex+=1)
			{
				if(isCartesianLineParallelTo(*cartesianTable.lines[indexFound],*cartesianTable.lines[lineIndex],0.1))
				{
					cartesianTable.occurency[lineIndex] = 0;
				}
			}

		}
		else
		{
			significantLines = newCartesianLineTable(*cartesianTable.lines[indexFound]);
		}

		cartesianTable.occurency[indexFound] = 0;

		indexFound = getMaximumOcurencyIndex(cartesianTable,ocurencyMin);

		//printf("Index found : %d with %d ocurency\n",indexFound,cartesianTable.occurency[indexFound]);
	}


	return significantLines;
}
