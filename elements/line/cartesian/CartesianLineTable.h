/*
 * @title CartesianLineTable.h
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef CARTESIANLINETABLE_H_
#define CARTESIANLINETABLE_H_

#include "CartesianLine.h"

/**
 * @chap The 'CartesianLineTable' data structure and its associated functions
 */

/**
 * @sec The 'CartesianLineTable' data structure
 */

/**
 * @struct
 * @desc
 * @field
 * @field
 * @field
 */
typedef struct CartesianLineTable
{
	int size;
	int* occurency;
	CartesianLine** lines;
}CartesianLineTable;

/**
 * @sec The memory handling functions
 */

/**
 * @fn newCartesianLineTable
 * @desc
 * @param
 * @return
 * @bcode
 */
CartesianLineTable* newCartesianLineTable(CartesianLine cartesianLine);
/**
 * @ecode
 */

void freeCartesianLineTable(CartesianLineTable** cartesianLineTable);

/**
 * @sec The getter and setter
 */

void addCartesianLineToTable(CartesianLineTable* cartesianLineTable, CartesianLine cartesianLine);

/**
 * @sec The boolean returning functions
 */

int isCartesianLineInTable(CartesianLineTable cartesianLineTable, CartesianLine cartesianLine);

/**
 * @sec The processing functions
 */

void drawCartesianLineTableInImage(CartesianLineTable cartesianLineTable, ImageRGB* imageRGB, ColorRGB colorRGB);

int getMaximumOcurencyIndex(CartesianLineTable cartesianLineTable, int ocurencyMin);

CartesianLineTable* getSignificantLines(CartesianLineTable cartesianTable, int ocurencyMin);

#endif /* CARTESIANLINETABLE_H_ */
