/*
 * @title CartesianLineTest.c
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */

#include "CartesianLine.h"
#include "../../pixel/PixelCloud.h"
#include <stdio.h>
#include <stdlib.h>

PixelCloud* initPixels(void);
void createLineTest(PixelCloud pixelCloud);
void findOrthogonalTest(PixelCloud pixelCloud);

int main(void)
{
	PixelCloud* pixelCloud = initPixels();
	createLineTest(*pixelCloud);
	findOrthogonalTest(*pixelCloud);
	freePixelCloud(&pixelCloud);
	return 0;
}


PixelCloud* initPixels(void)
{
	Pixel* pixelZero = newPixel(100,100);

	PixelCloud* pixelCloud = newPixelCloud(*pixelZero);

	freePixel(&pixelZero);

	Pixel* pixelOne = newPixel(200,100);
	addPixelInPixelCloud(pixelCloud,*pixelOne);
	freePixel(&pixelOne);

	Pixel* pixelTwo = newPixel(300,100);
	addPixelInPixelCloud(pixelCloud,*pixelTwo);
	freePixel(&pixelTwo);

	Pixel* pixelThree = newPixel(400,100);
	addPixelInPixelCloud(pixelCloud,*pixelThree);
	freePixel(&pixelThree);

	Pixel* pixelFour = newPixel(100,200);
	addPixelInPixelCloud(pixelCloud,*pixelFour);
	freePixel(&pixelFour);

	Pixel* pixelFive = newPixel(200,200);
	addPixelInPixelCloud(pixelCloud,*pixelFive);
	freePixel(&pixelFive);

	Pixel* pixelSix = newPixel(300,200);
	addPixelInPixelCloud(pixelCloud,*pixelSix);
	freePixel(&pixelSix);

	Pixel* pixelSeven = newPixel(100,300);
	addPixelInPixelCloud(pixelCloud,*pixelSeven);
	freePixel(&pixelSeven);

	Pixel* pixelEight = newPixel(200,300);
	addPixelInPixelCloud(pixelCloud,*pixelEight);
	freePixel(&pixelEight);

	Pixel* pixelNine = newPixel(300,300);
	addPixelInPixelCloud(pixelCloud,*pixelNine);
	freePixel(&pixelNine);

	Pixel* pixelTen = newPixel(100,400);
	addPixelInPixelCloud(pixelCloud,*pixelTen);
	freePixel(&pixelTen);

	Pixel* pixelEleven = newPixel(400,400);
	addPixelInPixelCloud(pixelCloud,*pixelEleven);
	freePixel(&pixelEleven);

	return pixelCloud;
}

void createLineTest(PixelCloud pixelCloud)
{
	printf("\n==CreateLineTest==\n\n");

	Matrix* smallMatrix = newMatrix(500,500);
	Matrix* longMatrix = newMatrix(500,500);

	CartesianLine* lineOne = newCartesianLineFromPixels(*pixelCloud.pixels[1],*pixelCloud.pixels[2]);
	printCartesianLine(*lineOne,true);
	drawCartesianLineInMatrix(*lineOne,smallMatrix,255);
	if(isPixelInCartesianLine(*lineOne,*pixelCloud.pixels[0],1))
	{
		addPixelToCartesianLine(lineOne,*pixelCloud.pixels[0]);
	}
	if(isPixelInCartesianLine(*lineOne,*pixelCloud.pixels[3],1))
	{
		addPixelToCartesianLine(lineOne,*pixelCloud.pixels[3]);
	}
	printCartesianLine(*lineOne,true);
	drawCartesianLineInMatrix(*lineOne,longMatrix,255);

	CartesianLine* lineTwo = newCartesianLineFromPixels(*pixelCloud.pixels[4],*pixelCloud.pixels[7]);
	printCartesianLine(*lineTwo,true);
	drawCartesianLineInMatrix(*lineTwo,smallMatrix,255);
	if(isPixelInCartesianLine(*lineTwo,*pixelCloud.pixels[0],1))
	{
		addPixelToCartesianLine(lineTwo,*pixelCloud.pixels[0]);
	}
	if(isPixelInCartesianLine(*lineTwo,*pixelCloud.pixels[10],1))
	{
		addPixelToCartesianLine(lineTwo,*pixelCloud.pixels[10]);
	}
	printCartesianLine(*lineTwo,true);
	drawCartesianLineInMatrix(*lineTwo,longMatrix,255);

	CartesianLine* lineThree = newCartesianLineFromPixels(*pixelCloud.pixels[5],*pixelCloud.pixels[9]);
	printCartesianLine(*lineThree,true);
	drawCartesianLineInMatrix(*lineThree,smallMatrix,255);
	if(isPixelInCartesianLine(*lineThree,*pixelCloud.pixels[0],1))
	{
		addPixelToCartesianLine(lineThree,*pixelCloud.pixels[0]);
	}
	if(isPixelInCartesianLine(*lineThree,*pixelCloud.pixels[11],1))
	{
		addPixelToCartesianLine(lineThree,*pixelCloud.pixels[11]);
	}
	printCartesianLine(*lineThree,true);
	drawCartesianLineInMatrix(*lineThree,longMatrix,255);

	CartesianLine* lineFour = newCartesianLineFromPixels(*pixelCloud.pixels[8],*pixelCloud.pixels[6]);
	printCartesianLine(*lineFour,true);
	drawCartesianLineInMatrix(*lineFour,smallMatrix,255);
	if(isPixelInCartesianLine(*lineFour,*pixelCloud.pixels[10],1))
	{
		addPixelToCartesianLine(lineFour,*pixelCloud.pixels[10]);
	}
	if(isPixelInCartesianLine(*lineFour,*pixelCloud.pixels[3],1))
	{
		addPixelToCartesianLine(lineFour,*pixelCloud.pixels[3]);
	}
	printCartesianLine(*lineFour,true);
	drawCartesianLineInMatrix(*lineFour,longMatrix,255);

	CartesianLine* othoOne = getOrthogonalCartesianLine(*lineOne,*pixelCloud.pixels[6]);
	printCartesianLine(*othoOne,true);
	if(isPixelInCartesianLine(*othoOne,*pixelCloud.pixels[9],1))
	{
		addPixelToCartesianLine(othoOne,*pixelCloud.pixels[9]);
	}
	printCartesianLine(*othoOne,true);
	drawCartesianLineInMatrix(*othoOne,longMatrix,255);

	newGreyImageFromMatrix(*smallMatrix,"small.bmp");
	newGreyImageFromMatrix(*longMatrix,"long.bmp");
}


void findOrthogonalTest(PixelCloud pixelCloud)
{

}
