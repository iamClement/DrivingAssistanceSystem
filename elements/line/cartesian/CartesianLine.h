/*
 * @title CartesianLine.h
 * @date 21 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef CARTESIANLINE_H_
#define CARTESIANLINE_H_

#include "../../image/ImageRGB.h"

/**
 * @chap The 'CartesianLine' data structure and its associated functions
 */

/**
 * @sec The 'CartesianLine' data structure
 */

typedef struct CartesianLine
{
	bool isVertical;
	bool isHorizontal;
	float slope;
	float OriginIntercept;
	Pixel* leftEnd;
	Pixel* rightEnd;
}CartesianLine;

/**
 * @sec The memory handling functions
 */

/**
 * @fn newCartesianLineFromPixels
 * @desc Create a straight line from two pixels given
 * @param firstPixel (Pixel) : the first pixel defining the line
 * @param secondPixel (Pixel) : the second pixel defining the line
 * @return (CartesianLine*) the line containing the two pixels given
 * @bcode
 */
CartesianLine* newCartesianLineFromPixels(Pixel firstPixel, Pixel secondPixel);
/**
 * @ecode
 */

/**
 * @fn freeCartesianLine
 * @desc Free the memory allocated to a CartesianLine structure
 * @param cartesianLine (CartesianLine**) : the line to be freed
 * @bcode
 */
void freeCartesianLine(CartesianLine** cartesianLine);
/**
 * @ecode
 */

/**
 * @sec The getter and setter
 */

/**
 * @fn setLeftEndPixel
 * @desc Set the value of the left end pixel with the given pixel
 * @param cartesianLine (CartesianLine*) : the line to modify
 * @param pixel (Pixel) the pixel to set as the left end pixel
 * @bcode
 */
void setLeftEndPixel(CartesianLine* cartesianLine, Pixel pixel);
/**
 * @ecode
 */

/**
 * @fn setRightEndPixel
 * @desc Set the value of the right end pixel with the given pixel
 * @param cartesianLine (CartesianLine*) : the line to modify
 * @param pixel (Pixel) the pixel to set as the right end pixel
 * @bcode
 */
void setRightEndPixel(CartesianLine* cartesianLine, Pixel pixel);
/**
 * @ecode
 */

/**
 * @sec The boolean returning functions
 */

/**
 * @fn isCartesianLineEqualTo
 * @desc Check if two lines are confused thanks to a given accuracy
 * @param cartesianLine (CartesianLine) : the reference line
 * @param cartesianLineToCompare (CartesianLine) : the line to compare to the first one
 * @param accuracy (float) : the accuracy of the comparison
 * @return (bool) : true, the lines are confused, false, the lines are not confused
 * @bcode
 */
bool isCartesianLineEqualTo(CartesianLine cartesianLine, CartesianLine cartesianLineToCompare, float accuracy);
/**
 * @ecode
 */

/**
 * @fn isCartesianLineParallelTo
 * @desc Check if two lines are parallel
 * @param cartesianLine (CartesianLine) : the first line to check
 * @param cartesianLineToCompare (CartesianLine) : the second line to check
 * @param accuracy (float) : the accuracy of the verification
 * @return (bool) : true, the lines are parallel, false, the lines are not parallel
 * @bcode
 */
bool isCartesianLineParallelTo(CartesianLine cartesianLine, CartesianLine cartesianLineToCompare, float accuracy);
/**
 * @ecode
 */

/**
 * @fn isPixelInCartesianLine
 * @desc Check if a given pixel belongs to a given line
 * @param cartesianLine (CartesianLine) : the reference line
 * @param pixel (Pixel) : the pixel to check
 * @param accuracy (float) : the accuracy of the check-up
 * @return (bool) : true, the pixel belongs to the line, false, the pixel does not belong to the line
 * @bcode
 */
bool isPixelInCartesianLine(CartesianLine cartesianLine, Pixel pixel, float accuracy);
/**
 * @ecode
 */

/**
 * @sec The printing functions
 */

/**
 * @fn printCartesianLine
 * @desc Print the specifications of the line in the command prompt
 * @param cartesianLine (CartesianLine) : the line to print
 * @param printEnds (bool) : to print, or not, the coordinates of the ends of the line
 * @bcode
 */
void printCartesianLine(CartesianLine cartesianLine, bool printEnds);
/**
 * @ecode
 */

/**
 * @fn drawLineInMatrix
 * @desc Draw the line in a Matrix structure
 * @param cartesianLine (CartesianLine) : the line to draw
 * @param matrix (Matrix*) : the matrix in which the line will be drawn
 * @param greyLevel (short int) : the grey level of the line
 * @bcode
 */
void drawCartesianLineInMatrix(CartesianLine cartesianLine, Matrix* matrix, short int greyLevel);
/**
 * @ecode
 */

/**
 * @fn drawCartesianLineInImage
 * @desc Draw the given line in a RGB image
 * @param cartesianLine (CartesianLine) : the line to draw
 * @param imageRGB (ImageRGB*) : the image in which the line will be drawn
 * @param colorRGB (ColorRGB) : the color of the line
 * @bcode
 */
void drawCartesianLineInImage(CartesianLine cartesianLine, ImageRGB* imageRGB, ColorRGB colorRGB);
/**
 * @ecode
 */

/**
 * @sec The processing functions
 */

/**
 * @fn getIntersectionPixel
 * @desc Give the intersection pixel between two lines
 * @param
 * @param
 * @return (Pixel*) : the intersection pixel
 * @bcode
 */
Pixel* getIntersectionPixel(CartesianLine firstLine, CartesianLine secondLine);
/**
 * @ecode
 */

/**
 * @fn getOrthogonalCartesianLine
 * @desc Return the orthogonal line from a given line containing a given pixel.
 * @param cartesianLine (CartesianLine) : the reference line
 * @param pixel (Pixel) : the pixel through which the line passes
 * @return (CartesianLine*) : the orthogonal line
 * @bcode
 */
CartesianLine* getOrthogonalCartesianLine(CartesianLine cartesianLine, Pixel pixel);
/**
 * @ecode
 */

/**
 * @fn addPixelToLine
 * @desc Add a given pixel to a given line. if the pixel is not between the two ending pixel, it will replace one of them.
 * @param cartesianLine (CartesianLine*) : the line to which the pixel will be added
 * @param pixel (Pixel) : the pixel to add
 * @bcode
 */
void addPixelToCartesianLine(CartesianLine* cartesianLine, Pixel pixel);
/**
 * @ecode
 */


#endif /* CARTESIANLINE_H_ */
