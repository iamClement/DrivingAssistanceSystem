/*
 * @title ImageRGB.c
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "ImageRGB.h"
#include "../../libs/libisentlib/BmpLib.h"

ImageRGB* newRGBImage(unsigned int height, unsigned int width)
{
	//Alocation of the memory for the structure
	ImageRGB* imageRGB = (ImageRGB*) malloc(sizeof(ImageRGB));

	//Filling the field of the structure
	imageRGB->height = height;
	imageRGB->width = width;

	//Allocation of the matrices of the image
	imageRGB->redMatrix = newMatrix(imageRGB->width,imageRGB->height);
	imageRGB->greenMatrix = newMatrix(imageRGB->width,imageRGB->height);
	imageRGB->blueMatrix = newMatrix(imageRGB->width,imageRGB->height);

	//Filling the matrices of the image with black
	fillMatrix(imageRGB->redMatrix);
	fillMatrix(imageRGB->greenMatrix);
	fillMatrix(imageRGB->blueMatrix);

	return imageRGB;
}

ImageRGB* readBMPFile(char* fileName)
{
	DonneesImageRGB* donneesImageRGB = lisBMPRGB(fileName);

	//Allocation of the ImageRGB
	ImageRGB* imageRGB = newRGBImage((unsigned int) donneesImageRGB->hauteurImage, (unsigned int) donneesImageRGB->largeurImage);

	int heighIndex, widthIndex, DIRGB = 0;

	for(heighIndex = 0 ; heighIndex < donneesImageRGB->hauteurImage; heighIndex+=1)
	{
		for(widthIndex = 0 ; widthIndex < donneesImageRGB->largeurImage ; widthIndex+=1)
		{
			imageRGB->blueMatrix->data[heighIndex][widthIndex] = donneesImageRGB->donneesRGB[DIRGB];
			imageRGB->greenMatrix->data[heighIndex][widthIndex] = donneesImageRGB->donneesRGB[DIRGB+1];
			imageRGB->redMatrix->data[heighIndex][widthIndex] = donneesImageRGB->donneesRGB[DIRGB+2];
			DIRGB+=3;
		}
	}

	libereDonneesImageRGB(&donneesImageRGB);

	return imageRGB;
}

void freeRGBImage(ImageRGB** imageRGB)
{
	if(imageRGB != NULL)
	{
		if(*imageRGB != NULL)
		{
			ImageRGB* interimImageRBG = * imageRGB;

			//Liberation of the matrices of the image
			freeMatrix(&interimImageRBG->redMatrix);
			freeMatrix(&interimImageRBG->greenMatrix);
			freeMatrix(&interimImageRBG->blueMatrix);

			//Liberation of the imageRBG structure
			free(interimImageRBG);

			//The pointer address is set to NULL
			*imageRGB = NULL;
		}
	}
}

void setPixelColorOfImageRGB(ImageRGB* imageRGB, ColorRGB colorRGB, Pixel pixel)
{
	if((pixel.abscissa < imageRGB->width)&&(pixel.ordinate < imageRGB->height))
	{
		imageRGB->redMatrix->data[pixel.ordinate][pixel.abscissa] = colorRGB.red;
		imageRGB->greenMatrix->data[pixel.ordinate][pixel.abscissa] = colorRGB.green;
		imageRGB->blueMatrix->data[pixel.ordinate][pixel.abscissa] = colorRGB.blue;
	}
}

ColorRGB* getPixelColorOfImageRGB(ImageRGB imageRGB, Pixel pixel)
{
	ColorRGB* colorRGB = NULL;

	if((pixel.abscissa < imageRGB.width)&&(pixel.ordinate < imageRGB.height))
	{
		colorRGB = newColorRGB(imageRGB.redMatrix->data[pixel.ordinate][pixel.abscissa],imageRGB.greenMatrix->data[pixel.ordinate][pixel.abscissa],imageRGB.blueMatrix->data[pixel.ordinate][pixel.abscissa]);
	}

	return colorRGB;
}

void writeBMPFile(ImageRGB imageRGB, char* fileName)
{
	//Allocation of the DonneesImageRBG
	DonneesImageRGB* donneesImageRGB = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));

	//Filling the field of the donneesImageRGB
	donneesImageRGB->hauteurImage = (int) imageRGB.height;
	donneesImageRGB->largeurImage = (int) imageRGB.width;
	donneesImageRGB->donneesRGB = (unsigned char *) malloc(donneesImageRGB->hauteurImage * donneesImageRGB->largeurImage * 3 * sizeof(unsigned char));

	//Filling the pixels table
	int widthIndex, heightIndex, DIRGBIndex = 0;

	for(heightIndex = 0 ; heightIndex < donneesImageRGB->hauteurImage ; heightIndex+=1)
	{
		for(widthIndex = 0 ; widthIndex < donneesImageRGB->largeurImage ; widthIndex+=1)
		{
			donneesImageRGB->donneesRGB[DIRGBIndex] = imageRGB.blueMatrix->data[heightIndex][widthIndex];
			donneesImageRGB->donneesRGB[DIRGBIndex+1] = imageRGB.greenMatrix->data[heightIndex][widthIndex];
			donneesImageRGB->donneesRGB[DIRGBIndex+2] = imageRGB.redMatrix->data[heightIndex][widthIndex];
			DIRGBIndex+=3;
		}
	}

	ecrisBMPRGB_Dans(donneesImageRGB,fileName);

	libereDonneesImageRGB(&donneesImageRGB);
}

ImageRGB* convertToGreyLevel(ImageRGB imageRGB)
{
	ImageRGB* greyImage = newRGBImage(imageRGB.height,imageRGB.width);

	int heightIndex, widthIndex;

	for(heightIndex = 0 ; heightIndex < imageRGB.height ; heightIndex+=1)
	{
		for(widthIndex = 0 ; widthIndex < imageRGB.width ; widthIndex+=1)
		{
			//Calcul of the value of the grey level
			short int greyValue = (short int) (0.2125 * getValueOfMatrixAt(*imageRGB.redMatrix,widthIndex,heightIndex) + 0.7154 * getValueOfMatrixAt(*imageRGB.greenMatrix,widthIndex,heightIndex) + 0.0721 * getValueOfMatrixAt(*imageRGB.blueMatrix,widthIndex,heightIndex));
			//The value is added to every matrix of the RGB image
			addValueToMatrixAt(greyImage->redMatrix,greyValue,widthIndex,heightIndex);
			addValueToMatrixAt(greyImage->greenMatrix,greyValue,widthIndex,heightIndex);
			addValueToMatrixAt(greyImage->blueMatrix,greyValue,widthIndex,heightIndex);
		}
	}

	return greyImage;
}
