#include "ImageHSV.h"
#include <stdio.h>
#include <stdlib.h>

ImageHSV* newHSVImage(unsigned int height, unsigned int width)
{
	//Alocation of the memory for the structure
	ImageHSV* imageHSV = (ImageHSV*) malloc(sizeof(ImageHSV));

	//Filling the field of the structure
	imageHSV->height = height;
	imageHSV->width = width;

	//Allocation of the matrices of the image
	imageHSV->hueMatrix = newMatrix(imageHSV->width,imageHSV->height);
	imageHSV->saturationMatrix = newMatrix(imageHSV->width,imageHSV->height);
	imageHSV->valueMatrix = newMatrix(imageHSV->width,imageHSV->height);

	return imageHSV;
}

void fillHSVImage(ImageHSV* imageHSV)
{
	fillMatrix(imageHSV->hueMatrix);
	fillMatrix(imageHSV->saturationMatrix);
	fillMatrix(imageHSV->valueMatrix);
}


void freeHSVImage(ImageHSV** imageHSV)
{
	if(imageHSV != NULL)
	{
		if(*imageHSV != NULL)
		{
			ImageHSV* interimImageHSV = * imageHSV;

			//Liberation of the matrices of the image
			freeMatrix(&interimImageHSV->hueMatrix);
			freeMatrix(&interimImageHSV->saturationMatrix);
			freeMatrix(&interimImageHSV->valueMatrix);

			//Liberation of the imageHSV structure
			free(interimImageHSV);

			//The pointer address is set to NULL
			*imageHSV = NULL;
		}
	}
}
