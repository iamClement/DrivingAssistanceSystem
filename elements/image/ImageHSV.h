/**
 * @title ImageHSV.h
 * @date 19 oct. 2018
 * @docauthor Kevin Reynier
 * @make
 */

#include "../../libs/matrixlib/Matrix.h"
#include "../../libs/libisentlib/BmpLib.h"

#ifndef IMAGEHSV_H_
#define IMAGEHSV_H_

/**
 * @struct ImageHSV
 * @desc A structure that represent an image composed by three matrices
 * @field height (unsigned int) : the height of the image in pixels
 * @field width (unsigned int) : the width of the image in pixels
 * @field hueMatrix (Matrix*) : the matrix containing the hue value of the pixels
 * @field saturationMatrix (Matrix*) : the matrix containing the saturation value of the pixels
 * @field valueMatrix (Matrix*) : the matrix containing the value of the pixels
 */
typedef struct ImageHSV
{
	unsigned int height;
	unsigned int width;
	Matrix* hueMatrix;
	Matrix* saturationMatrix;
	Matrix* valueMatrix;
}ImageHSV;

/**
 * @fn newHSVImage
 * @desc Allocate the memory for an ImageHSV structure
 * @param height (unsigned int) : the height of the image in pixels
 * @param width (unsigned int) : the width of the image in pixels
 * @return (ImageHSV*) : the ImageHSV structure allocated
 * @bcode
 */
ImageHSV* newHSVImage(unsigned int height, unsigned int width);
/**
 * @ecode
 */


/**
 * @fn fillHSVImage
 * @desc Fill the Matrices of the images with 0
 * @param imageHSV (ImageHSV*) : the image to fill
 * @bcode
 */
void fillHSVImage(ImageHSV* imageHSV);
/**
 * @ecode
 */

/**
 * @fn freeHSVImage
 * @desc Free the memory used by an ImageHSV
 * @param imageHSV (ImageHSV**) : the image to free
 * @bcode
 */
void freeHSVImage(ImageHSV** imageHSV);
/**
 * @ecode
 */

#endif /* IMAGEHSV_H_ */
