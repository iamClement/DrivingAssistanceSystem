/*
 * @title ColorRGB.c
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef COLORRGB_C_
#define COLORRGB_C_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "ColorRGB.h"

ColorRGB * newColorRGB(unsigned int red, unsigned int green, unsigned int blue)
{
	ColorRGB* colorRGB = (ColorRGB*) malloc(sizeof(ColorRGB));

	colorRGB->red = red;
	colorRGB->green = green;
	colorRGB->blue = blue;

	return colorRGB;
}

void freeColorRGB(ColorRGB** colorRGB)
{
	if(colorRGB != NULL)
	{
		if(*colorRGB != NULL)
		{
			//Liberation of the structure
			free(*colorRGB);

			//The pointer address is set to NULL
			*colorRGB = NULL;
		}
	}
}

float getDistanceBetweenColor(ColorRGB firstColor, ColorRGB secondColor)
{
	float distance = (float) pow( (double) (((int)(firstColor.red)) - ((int) secondColor.red)) , 2) + pow( (double) ( ((int) firstColor.green) - ((int) secondColor.green)), 2) + pow((double) (((int) firstColor.blue) - ((int) secondColor.blue)),2);
	return sqrtf(distance);
}

#endif /* COLORRGB_C_ */
