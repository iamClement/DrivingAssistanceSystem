/*
 * @title ColorRGB.h
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef COLORRGB_H_
#define COLORRGB_H_

/**
 * @chap The 'ColorRGB' data structure and its associated functions
 */

/**
 * @sec The 'ColorRGB' data structure
 */

/**
 * @struct ColorRGB
 * @desc A structure that represent a color in the RGB system
 * @field red (unsigned int) : the red value of the color
 * @field green (unsigned int) : the green value of the color
 * @field blue (unsigned int) :  the blue value of the color
 */
typedef struct ColorRGB
{
	unsigned int red;
	unsigned int green;
	unsigned int blue;
}ColorRGB;

/**
 * @sec The memory handling functions
 */

/**
 * @fn newColorRGB
 * @desc Allocate the memory for a ColorRGB structure
 * @param red (unsigned int) : the red value of the color to create
 * @param green (unsigned int) : the green value of the color to create
 * @param blue (unsigned int) :  the blue value of the color to create
 * @return (ColorRGB*) : the ColorRGB structure allocated and filled
 * @bcode
 */
ColorRGB* newColorRGB(unsigned int red, unsigned int green, unsigned int blue);
/**
 * @ecode
 */

/**
 * @fn freeColorRGB
 * @desc Free the memory used by a ColorRGB structure
 * @param colorRGB (ColorRGB**) : the color to free
 * @bcode
 */
void freeColorRGB(ColorRGB** colorRGB);
/**
 * @ecode
 */

/**
 * @sec The processing functions
 */

/**
 * @fn getDistanceBetweenColor
 * @desc Give the euclidian distance between two given colors
 * @param firstColor (ColorRGB) : the first color
 * @param secondColor (ColorRGB) : the second color
 * @return (float) : the distance between the two color
 * @bcode
 */
float getDistanceBetweenColor(ColorRGB firstColor, ColorRGB secondColor);
/**
 * @ecode
 */

#endif /* COLORRGB_H_ */
