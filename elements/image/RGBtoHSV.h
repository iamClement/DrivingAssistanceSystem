/**
 * @title RGBtoHSV.h
 * @date 19 oct. 2018
 * @docauthor Kevin Reynier
 * @make
 */
#include "../../libs/matrixlib/Matrix.h"
#include "ImageRGB.h"
#include "ImageHSV.h"

/**
 * @fn maximum
 * @desc Return the maximum value between 3 float a, b and c
 * @param a (float) : first value
 * @param b (float) : second value
 * @param c (float) : third value
 * @return (float) : the maximum value
 * @bcode
 */
float maximum(float a,float b,float c);
/**
 * @ecode
 */


 /**
  * @fn minimum
  * @desc Return the minimum value between 3 float a, b and c
  * @param a (float) : first value
  * @param b (float) : second value
  * @param c (float) : third value
  * @return (float) : the minimum value
  * @bcode
  */
float minimum(float a,float b,float c);
/**
 * @ecode
 */


/**
 * @fn pixelRGBtoHSV
 * @desc Change the values of a HSV pixel according to the RGB values
 * @param red (short int) : red value of the pixel
 * @param green (short int) : green value of the pixel
 * @param blue (short int) : blue value of the pixel
 * @param hue (short int) : hue value of the pixel
 * @param saturation (short int) : saturation value of the pixel
 * @param value (short int) : value value of the pixel
 * @bcode
 */
void pixelRGBtoHSV( short int red, short int green, short int blue, short int *hue, short int *saturation, short int *value );
/**
 * @ecode
 */


/**
 * @fn imageRGBtoHSV
 * @desc Generate the HSV data of an image from the RGB data
 * @param imageRGB (ImageRGB) : the image with the RGB data
 * @return (ImageHSV*) : the image with the HSV data
 * @bcode
 */
ImageHSV* imageRGBtoHSV(ImageRGB* imageRGB);
/**
 * @ecode
 */


/**
 * @fn displayImageHSV
 * @desc Display on the terminal each pixels of an HSV image represented by [hue - saturation - value]
 * @param image (ImageHSV) : the image to display
 * @bcode
 */
void displayImageHSV(ImageHSV * image);
/**
 * @ecode
 */
