#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "RGBtoHSV.h"


float maximum(float a,float b,float c)
{
    if(b < c)
       b = c;
    if(a < b)
       a = b;
    return a;
}


float minimum(float a,float b,float c)
{
	if(b > c)
       b = c;
    if(a > b)
       a = b;
    return a;
}


// red, green, blue values are from 0 to 255
// hue = [0,360], saturation = [0,100], value = [0,100]
// if saturation == 0, then hue = -1 (undefined)
void pixelRGBtoHSV(short int red, short int green, short int blue, short int *hue, short int *saturation, short int *value)

{
     float min, max, delta;
     float redPixel , greenPixel , bluePixel;
     redPixel = red/255.0;
     greenPixel = green/255.0;
     bluePixel = blue/255.0;
     min = minimum( redPixel, greenPixel, bluePixel );
     max = maximum( redPixel, greenPixel, bluePixel );
     *value = max*100;

     delta = max - min;
     if( max != 0 )
     *saturation = (delta / max)*100;

     else {
         // r = g = b = 0 // saturation = 0, value is undefined
         *saturation = 0;
         *hue = -1;
         return;
     }

     if( redPixel == max )
     *hue = (( greenPixel - bluePixel ) / delta)*60; // between yellow & magenta


     else if( greenPixel == max )
     *hue = (2 + ( bluePixel - redPixel) / delta)*60; // between cyan & yellow


     else
     *hue = (4 + ( redPixel - greenPixel ) / delta)*60; // between magenta & cyan


     if( *hue < 0 )
     *hue += 360;
}



ImageHSV * imageRGBtoHSV(ImageRGB* imageRGB)
{
    unsigned int width = imageRGB->width, height = imageRGB->height, column,row;
    short int red, blue, green;

    ImageHSV * imageHSV = newHSVImage(height,width);
    fillHSVImage(imageHSV);


	//convert each pixel from RGB to HSV
    for (column=0; column<width; column++) {
        for (row=0; row<height; row++) {
			red = imageRGB->redMatrix->data[row][column];
			green = imageRGB->greenMatrix->data[row][column];
			blue = imageRGB->blueMatrix->data[row][column];

			pixelRGBtoHSV(red, green, blue, &(imageHSV->hueMatrix->data[row][column]), &(imageHSV->saturationMatrix->data[row][column]), &(imageHSV->valueMatrix->data[row][column]));
        }
    }
    return imageHSV;
}


void displayImageHSV(ImageHSV * image){
	unsigned int width = image->width;
	unsigned int height = image->height;
	short int hue, saturation, value;
	int row, column;
	for (row = 0; row<height; row++){
		for (column = 0; column<width ;column++){
			hue = image->hueMatrix->data[row][column];
			saturation = image->saturationMatrix->data[row][column];
			value = image->valueMatrix->data[row][column];
			printf("[ %d - %d - %d ] ",hue, saturation, value);
		}
		printf("\n");
	}
}
