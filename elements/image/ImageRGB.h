/*
 * @title ImageRGB.h
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#ifndef IMAGERGB_H_
#define IMAGERGB_H_

#include "ColorRGB.h"
#include "../pixel/Pixel.h"
#include "../../libs/matrixlib/Matrix.h"

/**
 * @chap The 'ImageRGB' data structure and its associated functions
 */

/**
 * @sec The 'ImageRGB' data structure
 */

/**
 * @struct ImageRGB
 * @desc A structure that represent an image composed by three matrices
 * @field height (unsigned int) : the height of the image in pixels
 * @field width (unsigned int) : the width of the image in pixels
 * @field redMatrix (Matrix*) : the matrix containing the red value of the pixels
 * @field greenMatrix (Matrix*) : the matrix containing the green value of the pixels
 * @field blueMatrix (Matrix*) : the matrix containing the blue value of the pixels
 */
typedef struct ImageRGB
{
	unsigned int height;
	unsigned int width;
	Matrix* redMatrix;
	Matrix* greenMatrix;
	Matrix* blueMatrix;
}ImageRGB;

/**
 * @sec The memory handling functions
 */

/**
 * @fn newRGBImage
 * @desc Allocate the memory for an ImageRGB structure
 * @param height (unsigned int) : the height of the image in pixels
 * @param width (unsigned int) : the width of the image in pixels
 * @return (ImageRGB*) : the ImageRGB structure allocated
 * @bcode
 */
ImageRGB* newRGBImage(unsigned int height, unsigned int width);
/**
 * @ecode
 */

/**
 * @fn readBMPFile
 * @desc Read the data contained in a 24 bits BMP image file and store it in a 'ImageRGB' data structure
 * @param fileName (char*) : the name of the file to open
 * @return (ImageRGB*) : the image filled with the data of the file
 * @bcode
 */
ImageRGB* readBMPFile(char* fileName);
/**
 * @ecode
 */

/**
 * @fn freeRGBImage
 * @desc Free the memory used by an ImageRGB
 * @param imageRGB (ImageRGB**) : the image to free
 * @bcode
 */
void freeRGBImage(ImageRGB** imageRGB);
/**
 * @ecode
 */

/**
 * @sec Getter and Setter
 */

/**
 * @fn setPixelColorOfImageRGB
 * @desc Set a color to a given pixel of a RGB image
 * @param imageRGB (ImageRGB*) : The image to be modified
 * @param colorRBG (ColorRGB) : The color to set to the image given pixel
 * @param Pixel (Pixel) : the pixel where we will set the color
 * @bcode
 */
void setPixelColorOfImageRGB(ImageRGB* imageRGB, ColorRGB colorRGB, Pixel pixel);
/**
 * @ecode
 */

/**
 * @fn getPixelColorOfImageRGB
 * @desc Give the color of a given pixel of a RGB image
 * @param imageRGB (ImageRGB) : the image whose color we want
 * @param pixel (Pixel) : the pixel where we want the color
 * @return (ColorRGB*) : the color of the pixel
 * @bcode
 */
ColorRGB* getPixelColorOfImageRGB(ImageRGB imageRGB, Pixel pixel);
/**
 * @ecode
 */

/**
 * @sec Processing functions
 */

/**
 * @fn writeBMPFile
 * @desc Write the data of an ImageRGB structure into a 24 bits BMP image file
 * @param imageRGB (ImageRGB) : the image to write
 * @param fileName (char*) : the name of the file to create
 * @bcode
 */
void writeBMPFile(ImageRGB imageRGB, char* fileName);
/**
 * @ecode
 */

/**
 * @fn convertToGreyLevel
 * @desc Convert the RGB image into a grey level image
 * @param imageRGB (ImageRGB) : The image to be converted
 * @return (ImageRGB*) : the grey image
 * @bcode
 */
ImageRGB* convertToGreyLevel(ImageRGB imageRGB);
/**
 * @ecode
 */

#endif /* IMAGERGB_H_ */
