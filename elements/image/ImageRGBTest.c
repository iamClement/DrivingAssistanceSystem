/*
 * @title ImageRGBTest.c
 * @date 14 janv. 2019
 * @docauthor luca
 * @make
 */

#include <stdio.h>
#include <stdlib.h>

#include "ImageRGB.h"

int main(void)
{
	ColorRGB* firstColor = newColorRGB(255,0,0);

	ColorRGB* secondColor = newColorRGB(0,255,255);

	printf("distance between colors : %f\n",getDistanceBetweenColor(*firstColor,*secondColor));

	freeColorRGB(&firstColor);
	freeColorRGB(&secondColor);

	ImageRGB* imageRGB = readBMPFile("../../image-files/chat.bmp");

	ImageRGB* greyImage = convertToGreyLevel(*imageRGB);

	writeBMPFile(*greyImage,"../../image-files/chat-2.bmp");

	freeRGBImage(&greyImage);

	freeRGBImage(&imageRGB);

}
